<?php
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
*/
defined( 'TERRAGON' ) or die( 'Restricted access' );
if(!defined('DS')) define( 'DS', DIRECTORY_SEPARATOR );


function url_generator($url,$appender){
	$url_data = parse_url($url);
	$isarray = false;
	if(is_array($appender)){
		$isarray = true;
		$appender = http_build_query($appender);
	}
    if(isset($url_data['query']) && $url_data != '')
    	$url .= !empty($appender) ?  '&'. $appender : '';
    elseif($isarray == true)
        $url .= !empty($appender) ?  '?'. $appender : '';
    else
        $url .= !empty($appender) ?  '/'. $appender : '';
    return $url;
}


function  Tcurl($url, $access_header=''){
	$headers = [
		"content-type : application/json",
		"X-Host-Override :  {$access_header}"
	 ];
	$options = array(
		"url" 		=> trim($url),
		"data" 		=> "plain",
		"type" 		=> "GET",
		'header' 	=> 0,
		"return_transfer" => "1",
		'httpheader' => $headers
	);
	$obj =& Terragon::load_libraries('curlclass');
	$obj->set_options($options);
	$json_str = trim($obj->Execute());
	if($obj->get_httpCode() == 200){
		$json_str = json_decode($json_str,true);
		return $json_str;
	}
	return false;
}


function write_file($filename, $somecontent, $type = 'a'){
	if (!$handle = fopen($filename, $type)) {
        return false;
    }
    if (flock($handle, LOCK_EX | LOCK_NB))
	{
	    // Write $somecontent to our opened file.
	    if (fwrite($handle, $somecontent) === FALSE) {
	        return false;
	    }
	   
	    flock($handle, LOCK_UN);

		fclose($handle);
	}
    return true;
}

function get_time_key($m = '',$d = '',$y = ''){
	$d = ($d == '') ? date('d') : $d;
	$m = ($m == '') ? date('m') : $m;
	$y = ($y == '') ? date('Y') : $y;
	return mktime(0, 0, 0, $m, $d, $y);
}

function read_file($filename){
	if(!is_file($filename)) return false;
	$file = file_get_contents($filename);
	if (!$file) {
        return false;
    }
    return $file;
}

function msisdn_sanitizer($msisdn,$plus = true){
	$msisdn = trim($msisdn);
	$msisdn = str_replace('+', '', $msisdn);
	if(preg_match('/^234/i',$msisdn) == true){
		$msisdn = '0'.substr($msisdn, 3); 		
	}
	if(strlen($msisdn) == '11'){
		$msisdn = '+234'.substr($msisdn, 1); 
	}else{
		if(strpos($msisdn,'+') == false) 
			$msisdn = '+'.$msisdn;		
	}
	if($plus == false){
		$msisdn = str_replace('+', '', $msisdn);
	}
	return $msisdn;
}
function gerarkey($length = 40,$upper = false) 
{
	$length = $length - 10;
	$key = NULL;
	$pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTWXYZ-_';
	for( $i = 0; $i < $length; ++$i )
	{
		$key .= $pattern{rand(0,60)};
	}
	if(!$upper){
		return time().$key;
	}else{
		return time().strtoupper($key);
	}
}
if ( ! function_exists('prep_url'))
{
	function prep_url($str = '')
	{
		if ($str == 'http://' OR $str == '')
		{
			return '';
		}

		$url = parse_url($str);

		if ( ! $url OR ! isset($url['scheme']))
		{
			$str = 'http://'.$str;
		}

		return $str;
	}
}
// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
if ( ! function_exists('redirect'))
{
	function redirect($uri = '', $method = 'location', $http_response_code = 302)
	{
		if ( ! preg_match('#^https?://#i', $uri))
		{
			$uri = site_url($uri);
		}

		switch($method)
		{
			case 'refresh'	: header("Refresh:0;url=".$uri);
				break;
			default			: header("Location: ".$uri, TRUE, $http_response_code);
				break;
		}
		exit;
	}
}

function send_sms($msisdn, $message, $shortcode, $serviceid, $debug = false){
	// $message = rawurlencode(urldecode($message));		
	// // $sender_url = "http://46.38.169.102/sendsms/?msisdn=$msisdn&message=$message";
	// $sender_url = "http://46.38.169.102:13130/cgi-bin/sendsms?username=terragon123&password=terragon123&smsc=MTN_SMSC&from=30046&to=$msisdn&text=$message";
	// //curl($sender_url);
	$sdp  =& Terragon::getSDP($serviceid);
	$sms_content = rawurldecode($message);
	return $sdp->sendSms($msisdn,  $sms_content, $shortcode , $debug );
}

function curl($url,$requestData = ''){
	$options = array(
		"url" 			=> $url,
		"data" 			=> "plain",
		"httpheader" 	=> array('Content-type: application/json'),
		//"post" 			=> "1",
		//"post_fields" 	=> json_encode($requestData),
		"return_transfer" => "1"
	);		
	$obj =& Terragon::load_libraries('curlclass');
	$obj->set_options($options);
	$resp = $obj->Execute();	
	if($obj->get_httpCode() != 200){
		return "";
	}return $resp;
}



/**
 * parse a full url or pathname and return an array(protocol, host, path,
 * file + query + fragment)
 *
 * @param string $url
 * @return array
 */
function explode_url_keep($url) {
	$protocol = "";
	$host = "";
	$path = "";
	$file = "";

	$arr = parse_url($url);

	if ( isset($arr["scheme"]) && $arr["scheme"] != "file" && mb_strlen($arr["scheme"]) > 1 ) // Exclude windows drive letters...
	{
		$protocol = $arr["scheme"] . "://";
		if ( isset($arr["user"]) ) {
			$host .= $arr["user"];
			if ( isset($arr["pass"]) )
				$host .= "@" . $arr["pass"];
			$host .= ":";
		}
		if ( isset($arr["host"]) )
			$host .= $arr["host"];

		if ( isset($arr["port"]) )
			$host .= ":" . $arr["port"];

		if ( isset($arr["path"]) && $arr["path"] !== "" ) {
			// Do we have a trailing slash?
			if ( $arr["path"]{ mb_strlen($arr["path"]) - 1 } == "/" ) {
				$path = $arr["path"];
				$file = "";
			} else {
				$path = dirname($arr["path"]) . "/";
				$file = basename($arr["path"]);
			}
		}
		if ( isset($arr["query"]) )
			$file .= "?" . $arr["query"];

		if ( isset($arr["fragment"]) )
			$file .= "#" . $arr["fragment"];

	} else {
		$i = mb_strpos($url, "file://");
		if ( $i !== false)
			$url = mb_substr($url, $i + 7);

		$protocol = ""; // "file://"; ? why doesn't this work... It's because of
                    // network filenames like //COMPU/SHARENAME
		$host = ""; // localhost, really
		$file = basename($url);
		$path = dirname($url);

		// Check that the path exists
		if ( $path !== false ) {
			$path .= '/';
		} else {
			// generate a url to access the file if no real path found.
			$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
			$host = isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : php_uname("n");
			if ( substr($arr["path"], 0, 1) == '/' ) {
				$path = dirname($arr["path"]);
			} else {
				$path = '/' . rtrim(dirname($_SERVER["SCRIPT_NAME"]), '/') . '/' . $arr["path"];
			}
		}
	}
	$ret = array($protocol, $host, $path, $file,
		"protocol" => $protocol,
		"host" => $host,
		"path" => $path,
		"file" => $file
	);
	return $ret;
}



function isValidTimezoneId2($tzid)
{
	if(empty($tzid)) return false;
	$valid = array();
	$tza = timezone_abbreviations_list();
	foreach ($tza as $zone){
		foreach ($zone as $item){
			$valid[$item['timezone_id']] = true;
		}
	}
	unset($valid['']);
	return !!$valid[$tzid];
}
function encode($string ,$coded = false, $key = 'password to (en/de)crypt')
{
	if($coded == false) return base64_encode($string);
	else return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
}
function decode($string, $coded = false, $key = 'password to (en/de)crypt')
{
	if($coded == false) return base64_decode($string);
	else return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
}
function isJson($string) {
  return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
}


function createRandomcode($len = 10,$upper = false,$onlynumber = false,$pass = '')
{
	if($onlynumber == true){
		$pattern = "1234567890";
		$max = 9;
	}else{
		$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTWXYZ";
		$max = 58;
	}
	for( $i = 0; $i < $len; ++$i )
	{
		$pass .= $pattern{rand(0,$max)};
	}	
	if(strlen($pass) < $len){
		return createRandomcode($len,$upper,$onlynumber,$pass);
	}elseif( strlen($pass) > $len) {
		$pass = substr($pass, 0, $len);
	}	
	if(!$upper){
		return $pass;
	}else{
		if($upper == 1)	return strtoupper($pass);
		else  return ucfirst($pass);
	}	
}

function timestamp_convertdate ($stamp)
{
	//list($date, $time) = explode(" ", $stamp);
	list($month, $day, $year) = explode("/", $stamp);
	list($hour, $minite) = 		explode(":", $time);
	//if(!is_int($day) || empty($month) || !is_int($month) || !is_int($year) )	return;	
	if (strlen($month) > 2)		return;
	elseif(strlen($day) > 2)	return;
	elseif(strlen($year) > 4)	return;	
	
	return mktime(0, 0, 0, $month, $day, $year);
}

function convertsize ($bytes,$type = 'auto', $addunit = true)
{
	switch(strtolower($type)){			
		default : 
		case "gb":
			$bytes = number_format($bytes / 1073741824, 2);
			if($addunit == true) $bytes .= ' GB';
		break;
		case "mb":
			$bytes = number_format($bytes / 1048576, 2);
			if($addunit == true) $bytes .= ' MB';
		break;
		case "kb":
			$bytes = number_format($bytes / 1024, 2);
			if($addunit == true) $bytes .= ' KB';
		break;
		case "auto" :
			if ($bytes >= 1073741824){
				$bytes = number_format($bytes / 1073741824, 2);
				if($addunit == true) $bytes .= ' GB';
			}elseif ($bytes >= 1048576){
				$bytes = number_format($bytes / 1048576, 2);
				if($addunit == true) $bytes .= ' MB';
			}elseif ($bytes >= 1024){
				$bytes = number_format($bytes / 1024, 2);
				if($addunit == true) $bytes .= ' KB';
			}elseif ($bytes > 1){
				$bytes = $bytes;
				if($addunit == true) $bytes .= ' bytes';
			}elseif ($bytes == 1){
				$bytes = $bytes;
				if($addunit == true) $bytes .= ' byte';
			}else{
				$bytes = '0';
				if($addunit == true) $bytes .= ' bytes';
			}					
		break;			
	}
	return $bytes;
}

function tohtml($strValue)
{
  return htmlspecialchars($strValue , ENT_QUOTES);
}
function tourl($strValue,$enquote = '')
{
	if($enquote == '') return urlencode($strValue);
	else{
		return  str_replace(" ", $enquote, $strValue);
	}
}
function fromurl($strValue,$enquote = '')
{
	if($enquote == '') return urldecode($strValue);
	else{
		$strValue = str_replace($enquote, " ", $strValue);
		return urldecode($strValue);
	}
}
function is_number($string_value)
{
  if(is_numeric($string_value) || !strlen($string_value))
    return true;
  else 
    return false;
}
function set_param($name, $value = null, $hash = 'method', $overwrite = true)
{
	// If overwrite is true, makes sure the variable hasn't been set yet
	if (!$overwrite && array_key_exists($name, $_REQUEST))
	{
		return $_REQUEST[$name];
	}
	// Get the request hash value
	$hash = strtoupper($hash);
	if ($hash === 'METHOD')
	{
		$hash = strtoupper($_SERVER['REQUEST_METHOD']);
	}

	$previous = array_key_exists($name, $_REQUEST) ? $_REQUEST[$name] : null;

	switch ($hash)
	{
		case 'GET':
			$_GET[$name] = $value;
			$_REQUEST[$name] = $value;
			break;
		case 'POST':
			$_POST[$name] = $value;
			$_REQUEST[$name] = $value;
			break;
		case 'COOKIE':
			$_COOKIE[$name] = $value;
			$_REQUEST[$name] = $value;
			break;
		case 'FILES':
			$_FILES[$name] = $value;
			break;
		case 'ENV':
			$_ENV['name'] = $value;
			break;
		case 'SERVER':
			$_SERVER['name'] = $value;
			break;
	}
	return $previous;
}
function get_param($name, $hash = 'default', $type = 'string',$clean = true)
{
	$hash = strtoupper( $hash );
	if ($hash === 'METHOD') {
		$hash = strtoupper( $_SERVER['REQUEST_METHOD'] );
	}
	// Get the input hash
	switch ($hash)
	{
		case 'GET' :
			$input = &$_GET;
			break;
		case 'POST' :
			$input = &$_POST;
			break;
		case 'FILES' :
			$input = &$_FILES;
			break;
		case 'COOKIE' :
			$input = &$_COOKIE;
			break;
		case 'ENV'    :
			$input = &$_ENV;
			break;
		case 'SERVER'    :
			$input = &$_SERVER;
			break;
		default:
			$input = &$_REQUEST;
			$hash = 'REQUEST';
			break;
		}	
	$var = (isset($input[$name]) && $input[$name] !== null) ? $input[$name] : '';
	if($var == '')	return $var;
	if($type == 'int'){
		 if(is_number($var) === false)
		 return '';
	}
	if($clean == true &&  $type == 'string'){
		import('libraries.filterinput');
		$filler = new JFilterInput();
		$var = trim($filler->clean($var,$type));
		$var = stripcslashes($var);
	}
	return $var;  
}


function redirect_to($where)
{
	if (headers_sent()) {
			echo "<script>document.location.href='{$where}';</script>\n";
	} else {
		header("Location: {$where}");
		exit;
	}	
}

function userErrorHandler($errno, $errmsg, $filename, $linenum, $vars) 
{
	error_reporting(0);
	// timestamp for the error entry
	$dt = time();
	if($errno == 2048) return;
	// define an assoc array of error string
	 $errortype = array (
                "1"		=> 'Error',				// E_ERROR 
                "2"     => 'Warning',			// E_WARNING 
                "4"     => 'Parsing Error', 	// E_PARSE 
                "8"     => 'Notice',			// E_NOTICE
                "16"    => 'Core Error',		// E_CORE_ERROR
                "32"	=> 'Core Warning',		// E_CORE_WARNING
                "64"    => 'Compile Error',		// E_COMPILE_ERROR
                "128"   => 'Compile Warning',	// E_COMPILE_WARNING
                "256"	=> 'User Error',		// E_USER_ERROR
                "512"	=> 'User Warning',		// E_USER_WARNING
                "1024"	=> 'User Notice',		// E_USER_NOTICE 
                "2048"	=> 'Runtime Notice',	// E_STRICT 
                "4096"	=> 'Catchable Fatal Error', // E_RECOVERABLE_ERROR
				"202"	=> 'Template Error', // My Error
				"203"	=> 'Api Error', // My Error
				"204"	=> 'Db query track', // My Error
                );
	 	if (array_key_exists($errno,$errortype)) {
			$en=  $errortype[$errno];
		}else{
			$en = "";
		}
		$e = "<p>".$en." : ".$errmsg." line No :".$linenum."</p>";
		$e .="<p>".$filename."</p>";		
		$config = Terragon::get_config();	
		
		$s_config =& get_config();
		if($s_config['site_error'] == 'database')
		{
			if ( array_key_exists($errno, $errortype) ) {			
				$db =& Terragon::dbconnect();

				$url = Terragon::get_url_com();
				$query = "insert into #__error_handler (id,error_no,error_type,error_msg,scriptname,scriptline,url,date) values (" .
				$db->tosql(null,"Text") . "," .
				$db->tosql($errno,"Text") . "," .
				$db->tosql($errortype[$errno],"Text") . "," .
				$db->tosql($errmsg,"Text") . "," .
				$db->tosql($filename,"Text") . "," .
				$db->tosql($linenum,"Text") . "," .	
				$db->tosql($url,"Text") . "," .	
				$db->tosql($dt, "Text") . ")";				
				$db->query($query,'error');
			}			
		}elseif($s_config['site_error'] == 'raise'){		
			Terragon::raise_warning ('set', array('msg' =>$e,'type'=>'alert'));
		}else{			
			echo $e;			
			$back =  debug_backtrace();
			echo "<pre>";print_r($back); "</pre>";
			die;				
		}
		if($config->sand_box == true) {
			echo $e;
			return;
		}	
		// save to the error log, and e-mail me if there is a critical user error
    if ($errno == E_USER_ERROR)
	{		
       mail("oadesipe@terragonltd.com", "Critical User Error", $err);
    }
}

function Stext ($Text)
{
	return $Text;			
}
function Slang ($sText)
{	
	/*Find and replace all lang text to be interpreted*/
	preg_match_all('/Stext\((.*?)\)/i', $sText, $words, PREG_PATTERN_ORDER);
	for ($i = 0; $i < count($words[0]); $i++) {
		$sText = str_replace("Stext(".$words[1][$i].")" , Stext($words[1][$i]),  $sText );
	}
	return $sText;
}

function showerror()
{
	echo Terragon::raise_warning('get',array('show'=>0));
}

if ( ! function_exists('show_error'))
{
	function show_error($message)
	{
		Terragon::raise_warning ('set', array('msg' =>$message,'type'=>'alert'));
		echo  Terragon::raise_warning('get',array('show'=>0));
		exit;
	}
}
if ( ! function_exists('is_loaded'))
{
	function is_loaded($class = '')
	{
		static $_is_loaded = array();

		if ($class != '')
		{
			$_is_loaded[strtolower($class)] = $class;
		}
 		//print_r($_is_loaded);
		return $_is_loaded;
	}
}
if ( ! function_exists('get_config'))
{
	function &get_config($replace = array())
	{
		static $_config;

		if (isset($_config)){
			return $_config[0];
		}
		$file_path = TERRAGON_BASE.DS.'config.php';
		// Fetch the config file
		if ( ! file_exists($file_path))	{
			exit('The configuration file does not exist.');
		}
		require($file_path);
		// Does the $config array exist in the file?
		if ( ! isset($s_config) OR ! is_array($s_config)){
			exit('Your config file does not appear to be formatted correctly.');
		}// Are any values being dynamically replaced?
		if (count($replace) > 0)
		{
			foreach ($replace as $key => $val)
			{
				if (isset($s_config[$key]))
				{
					$s_config[$key] = $val;
				}
			}
		}
		$_config[0] =& $s_config;
		return $_config[0];
	}
}
function break_array($parsearray,$pergroup = 2)
{
	$redfunc = function ($partial, $elem) use ($pergroup) {
		$groupCount = count($partial);
		if ($groupCount == 0 || count(end($partial)) == $pergroup)
			$partial[] = array($elem);
		else
			$partial[$groupCount-1][] = $elem;
	
		return $partial;
	};
	return  array_reduce($parsearray, $redfunc, array());
}

if ( ! function_exists('is_really_writable'))
{
	function is_really_writable($file)
	{
		// If we're on a Unix server with safe_mode off we call is_writable
		if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE)
		{
			return is_writable($file);
		}

		// For windows servers and safe_mode "on" installations we'll actually
		// write a file then read it.  Bah...
		if (is_dir($file))
		{
			 $file = rtrim($file, '/').'/'.md5(mt_rand(1,100).mt_rand(1,100));
			if (($fp = @fopen($file, 'w')) === FALSE){
				return FALSE;
			}
			fclose($fp);
			@chmod($file, 0777);
			@unlink($file);
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
		{
			return FALSE;
		}

		fclose($fp);
		return TRUE;
	}
}

?>