
### MTN SDP



### Database Structure [In-Review]

## MYSQL

# Content Provider Table (sdp_cpdetails)
- id
- content_id
- product_id
- campaign_id
- service_id
- content_name
- content_description
- price
- keywords
- access_url
- media
- creation_time
- duration
- welcome_message
- renewal_message
- cp_id
- content_category
- thumbnail_url
- sample_image_url
- payment_update_url
# User Data (sdp_userdata)
- id
- msisdn
- user_id
- cds_campaign_id
- time_created
# Content Provider Update (sdp_cpupdate)
- id
- product_id
- msisdn
- status
- type
- processed
# Price Code (sdp_pricecodes)
- id
- code

## REDIS
# User Subscription Status
{service:[number]:[productid]}

#api urls
- http://192.168.1.139:81/mtnsdp/api/subscribe/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]&partner=[partner]
- http://192.168.1.139:81/mtnsdp/api/unsubscribe/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]
- http://192.168.1.139:81/mtnsdp/api/check_subscriber_status/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]
- http://192.168.1.139:81/mtnsdp/api/msisdn_fetcher/?ms=[ms]&ad_id=[ad_id]&redir_url=[redir_url]
- http://192.168.1.139:81/mtnsdp/api/direct_billing_api/?cds_campaign_id=[cds_campaign_id]&msisdn=[msisdn]&partner=[partner]
- http://192.168.1.139:81/mtnsdp/api/unsubscribe_user_sms_service/?msisdn=[msisdn]&productID=[productID]
- http://192.168.1.139:81/mtnsdp/api/messaging_api/?user_id=[user_id]&content=[content]&ad_id=[ad_id]
- http://192.168.1.139:81/mtnsdp/api/renewer_api/?user_id=[user_id]&price_code=[price_code]&cid=[cds_campaign_id]
- http://192.168.1.139:81/mtnsdp/sdp/subscribe/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
- http://192.168.1.139:81/mtnsdp/sdp/update/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
- http://192.168.1.139:81/mtnsdp/sdp/unsubscribe/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
- http://192.168.1.139:81/mtnsdp/api/cpudate/