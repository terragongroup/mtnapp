status=$(curl -s --head -w %{http_code} http://127.0.0.1:81/ -o /dev/null)
echo $status

 #status=500

if [ $status -ne 200 ]
then 
	echo "Server is down"
	echo "Restarting server"
	/etc/init.d/php-fpm restart
	sleep 2
	service nginx restart
	echo "Restarting server"
	date >> /var/www/html/api/restart.log
	echo "Server was restarted" >> /var/www/html/api/restart.log
else
	echo "Server is up"
fi


status=$(redis-cli -a 103REDISSERVER ping)
echo $status

 #status=500

if [ "$status" != "PONG" ]
then
        echo "REDIS Server is down"
        echo "starting redis server"
        redis-server /etc/redis.conf &
        date >> /var/www/html/api/redis_restart.log
        echo "redis Server was restarted" >> /var/www/html/api/restart.log
        echo "Restarting redis server"
else
        echo "Redis Server is up"
fi
