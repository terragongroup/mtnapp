<?php
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
error_reporting (0);

// require 'defines.php';
// require 'import.php';

// import('function');
// import('factory');
// import('Slim.Slim');

// Terragon::load_libraries('Options');

// \Slim\Slim::registerAutoloader();

// $app = new \Slim\Slim();

// $redis 	=& Terragon::redis();  
// $db 	=& Terragon::dbconnect(); 



// $ID 			= get_param('ID');
// $type 			= get_param('type');
// $spID 			= get_param('spID');
// $productID 		= get_param('productID');
// $serviceID 		= get_param('serviceID');
// $keyword 		= get_param('keyword');
// $updateType 	= get_param('updateType');
// $serviceList 	= get_param('serviceList');
// $starttime 		= get_param('starttime');
// $updateTime 	= get_param('updateTime');
// $updateDesc 	= get_param('updateDesc');
// $effectiveTime 	= get_param('effectiveTime');
// $expiryTime 	= get_param('expiryTime');
// $accessCode 	= get_param('accessCode');
// $chargeMode 	= get_param('chargeMode');
// $MDSPSUBEXPMODE = get_param('MDSPSUBEXPMODE');
// $objectType 	= get_param('objectType');
// $isFreePeriod 	= get_param('isFreePeriod');
// $operatorID 	= get_param('operatorID');
// $payType 		= get_param('payType');
// $transactionID 	= get_param('transactionID');
// $orderKey 		= get_param('orderKey');
// $fee 			= get_param('fee');
// $durationOfGracePeriod 		= get_param('durationOfGracePeriod');
// $serviceAvailability 		= get_param('serviceAvailability');
// $durationOfSuspendPeriod 	= get_param('durationOfSuspendPeriod');
// $servicePayType 			= get_param('servicePayType');

// $cycleEndTime				= get_param('cycleEndTime');
// $channelID					= get_param('channelID');
// $TraceUniqueID				= get_param('TraceUniqueID');
// $rentSuccess				= get_param('rentSuccess');
// $try						= get_param('try');
// $updateReason				= get_param('updateReason');

// $request[] = $_POST;
// $request[] = $_GET;
// $request[] = $_SERVER;
// $request[] = $_ENV;
// $request[] = $_REQUEST;



class syncOrderRelation {

    public $userID; // UserID
    public $spID; // string
    public $productID; // string
    public $serviceID; // string
    public $serviceList; // string
    public $updateType; // int
    public $updateTime; // string
    public $updateDesc; // string
    public $effectiveTime; // string
    public $expiryTime; // string
    public $extensionInfo; // NamedParameterList

    public function syncOrderRelation() {
    	
       	$this->logrequest($_SERVER);
      
        $msisdn     = trim($parameters->userID->ID);
        $productID  = trim($parameters->productID);
        $updateType = trim($parameters->updateType);

        $msisdn			= get_param('HTTP_ID','SERVER');
        $updateType		= get_param('HTTP_UPDATETYPE','SERVER');
		$productID		= get_param('HTTP_PRODUCTID','SERVER');

        $UPDATETIME		= get_param('HTTP_UPDATETIME','SERVER');
		$SERVICEID		= get_param('HTTP_SERVICEID','SERVER');
		$SPID			= get_param('HTTP_SPID','SERVER');
		$TYPE			= get_param('HTTP_TYPE','SERVER');

		$request = array(
		 	'UPDATETIME'	=> $UPDATETIME,
		 	'SERVICEID'		=> $SERVICEID,
		 	'MSISDN'		=> $msisdn,
		 	'SPID'			=> $SPID,
		 	'TYPE'			=> $UPDATETIME,
		 	'UPDATETYPE'	=> $updateType,
		 	'PRODUCTID'		=> $productID
		);

		$this->logproceesedrequest($request);
		

		if ($updateType == 0) {
			$response = array('result' =>1211, 'resultDescription' =>'Invalid update type');
			return $response;
		}
		$response = "";    
		switch ($updateType) {
			case 1:
				//$url = "http://127.0.0.1:81/sdp/subscribe/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp"; 
                $url = "http://127.0.0.1/sdpapi/sdp/subscribe/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp"; 
				$result = json_decode($this->curl_min($url),true);
			break;
			case 2:
				//$url = "http://127.0.0.1:81/sdp/unsubscribe/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp";
                $url = "http://127.0.0.1/sdpapi/sdp/unsubscribe/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp"; 
				$result = json_decode($this->curl_min($url),true);
			break;
			case 3:
				//$url = "http://127.0.0.1:81/sdp/renew/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp"; 
                $url = "http://127.0.0.1/sdpapi/sdp/renew/?content_id=".$productID."&msisdn=".$msisdn."&partner=sdp";
				$result = json_decode($this->curl_min($url),true);
			break;
			default:
				$result = array(
					'error' 		=> true,
					'error_code'	=> 1211,
					'response' 		=> "The field format is incorrect or the value is invalid"
				);
			break;
		}
		$response = array('result' =>$result['error_code'], 'resultDescription' => $result['response']);
		$response['url'] = $url;
		$this->logresponse($response);
		return $response;
    }
    function logrequest($parameters){
    	$defaultParam = json_encode($parameters)."\n";
		$file = fopen('../apilogs/Sync-log-'.date('n-Y').'.log', 'a+');
		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
    }
    function logproceesedrequest($parameters){
    	$defaultParam = json_encode($parameters)."\n";
		$file = fopen('../apilogs/request-log-'.date('n-Y').'.log', 'a+');
		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
    }
    function logresponse($parameters){
    	$defaultParam = json_encode($parameters)."\n";
		$file = fopen('../apilogs/response-log-'.date('n-Y').'.log', 'a+');
		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
    }
   
    private function curl_min($url) {
        $timeout = 50;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $result = curl_exec($ch);

        // if (curl_errno($ch) != 0) {
        //     $this->logerror("[" . $date . "]" . curl_errno($ch) . " | " . curl_error($ch));
        // }
        curl_close($ch);
        return $result;
    }
}

$server = new syncOrderRelation();



