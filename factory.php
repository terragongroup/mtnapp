<?php 
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
defined( 'TERRAGON' ) or die( 'Restricted access' );
use Elasticsearch\ClientBuilder;
class Terragon
{
	/*
	*********************************
	**
	**	LOAD LIBRARIES CONTROLLER
	**	USEAGE 
	**  $classobject =& Terragon::load_libraries([CLASSNAME]); 
	*********************************
	*/
	public static function &load_libraries($class, $param = "")	{
		static $_classes = array();
		if (isset($_classes[$class])){
			return $_classes[$class];
		}
		$name = FALSE;	
		$file_path = LIBRARIES.DS.$class.'.php';		
		if (file_exists($file_path)){
			$name = $class;
			if (class_exists($name) === FALSE)	{
				import('libraries.'.$class);	
			}	
		}
		// Did we find the class?
		if ($name === FALSE){
			Terragon::raise_warning ('set', array('msg' =>Stext("Unable to locate the specified  class").":".$file_path,'type'=>'alert'));
		}
		// Keep track of what we just loaded
		is_loaded('libraries'.$class);
		$_classes[$class] = new $name($param);
		return $_classes[$class];
	}

	/*
	*********************************
	**
	**	LOAD MODELS CONTROLLER
	**	USEAGE 
	**  $modelobject =& Terragon::load_model([MODELNAME]);
	*********************************
	*/

	public static function &load_model($class,$item)	{
	
		static $_classes = array();
		if (isset($_classes[$class.$item])){
			return $_classes[$class.$item];
		}
		$name = FALSE;	
		$file_path = MODELS.DS.$class.'.php';		
		if (file_exists($file_path)){
			$name = "model_".$class;
			if (class_exists($name) === FALSE)	{
				import('models.'.$class);	
			}	
		}
		// Did we find the class?
		if ($name === FALSE){
			Terragon::raise_warning ('set', array('msg' =>Stext("Unable to locate the specified  class").":".$file_path,'type'=>'alert'));
		}
		// Keep track of what we just loaded
		is_loaded('model'.$class);
		$_classes[$class.$item] = new $name($item);
		return $_classes[$class.$item];
	}

	/*
	*********************************
	**
	**	DATABASE CONNECTION INTERFACE
	**	USEAGE 
	**  $db =& Terragon::dbconnect(); 
	*********************************
	*/
	
	public static function &dbconnect($dbtype = "")
	{
		static $instances;
		global $db;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['db'.$dbtype])) {	
			$object =& Terragon::load_libraries('db_mysql');
			$db = $object;
			$s_config =& get_config();		
			if($dbtype == ""){
				$db->Database 		= $s_config['db_database'];
				$db->User     		= $s_config['db_user'];
				$db->Password 		= $s_config['dp_password'];
				$db->Host     		= $s_config['db_host'];
				$db->Prefix   		= $s_config['db_prefix'];
			}else{
				$db->Database 		= $s_config['db_database_2'];
				$db->User     		= $s_config['db_user_2'];
				$db->Password 		= $s_config['dp_password_2'];
				$db->Host     		= $s_config['db_host_2'];
				$db->Prefix   		= "";
			}
			$db->Halt_On_Error	= $s_config['db_error'];
			$instances['db'.$dbtype] = $object;
		}return $instances['db'.$dbtype];
	}

	/*
	*********************************
	**
	**	REDIS CONNECTION INTERFACE
	**	USEAGE 
	**  $redis =& Terragon::redis();
	*********************************
	*/

	public static function &redis($soft = false)
	{
		static $instances;
		global $redis;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['redis'])) {	
			$object =& Terragon::load_libraries('Predis');
			$s_config =& get_config();		
			$object->scheme 	= $s_config['redis_scheme'];
			$object->host     	= $s_config['redis_host'];
			$object->port 		= $s_config['redis_port'];
			$object->database   = $s_config['redis_database'];
			$object->soft    	= $soft;
			$object->connect();
			$redis = $object;
			$instances['redis'] = $object;
		}return $instances['redis'];
	}

	/*
	*********************************
	**
	**	RABBITMQ CONNECTION INTERFACE
	**	USEAGE
	**  $rabbitmq =& Terragon::rabbitmq();  
	*********************************
	*/

	public static function &rabbitmq($soft = false)
	{		
		static $instances;
		global $rabbitmq;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['rabbitmq'])) {	
			$object =& Terragon::load_libraries('RabbitMQ');
			$s_config =& get_config();		
			$object->host 		= $s_config['rabbit_host'];
			$object->port     	= $s_config['rabbit_port'];
			$object->username 	= $s_config['rabbit_username'];
			$object->passkey    = $s_config['rabbit_passkey'];
			$object->soft 		= $soft;
			$object->connect();
			$rabbitmq = $object;
			$instances['rabbitmq'] = $object;
		}return $instances['rabbitmq'];
	}

	public static function &elastic($soft = false)
	{
		static $instances;
		global $eS;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['elastic'])) {		
			$s_config =& get_config();      
			$hosts = [$s_config['es_host'].':'.$s_config['es_port']];
			$object = ClientBuilder::create() ->setHosts($hosts)->build();    
			$eS = $object;
			$instances['elastic'] = $object;
		}return $instances['elastic'];
	}

	public static function &elasticSearch($soft = false)
	{
		static $instances;
		global $elasticSearch;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['elasticSearch'])) {	
			$object =& Terragon::load_libraries('ElasticSearch');
			$s_config =& get_config();      
	        $object->server = $s_config['es_server'];
	        $object->index 	= $s_config['index'];
			$elasticSearch = $object;
			$instances['elasticSearch'] = $object;
		}return $instances['elasticSearch'];
	}

	/*
	*********************************
	**
	**	MONGO CONNECTION INTERFACE
	**	USEAGE 
	**  $mongo =& Terragon::mongo();
	*********************************
	*/

	public static function &mongo($soft = false)
	{
		static $instances;
		global $mongo;
		if (!isset ($instances)) 	$instances = array();
		if (empty($instances['mongo'])) {	
			$object =& Terragon::load_libraries('cimongo',$soft);
			$mongo = $object;
			$instances['mongo'] = $object;
		}return $instances['mongo'];
	}

	/*
	*********************************
	**
	**	REQUIRES CAMPAIGNID AND (EITHER USERID OR MSISDN)
	**	USEAGE 
	**  $msisdnobj =& Terragon::getUC([CAMPAGINID],[USERID]); 
	** 	OR
	**	$msisdnobj =& Terragon::getUC([CAMPAGINID],[MSISDN]); 
	*********************************
	*/

	public static function &getUC($campid,$data)
	{
		static $instances;
		if (!isset ($instances)) 	$instances = array ();
		if (empty($instances[$campid.':'.$data])) {	
			import('libraries.UserCampid');
			$instances[$campid.':'.$data] = new UserCampid($campid,$data);
		}
		return $instances[$campid.':'.$data];
	}

	/*
	*********************************
	**
	**	REQUIRES productid OR msisdn)
	**	USEAGE 
	**  $msisdnobj =& Terragon::getMP([productid],[msisdn]); 
	*********************************
	*/

	public static function &getMP($productid,$msisdn)
	{
		static $instances;
		if (!isset ($instances)) $instances = array ();
		if (empty($instances[$productid.':'.$msisdn])) {	
			import('libraries.MsisdnSubscriber');
			$instances[$productid.':'.$msisdn] = new MsisdnSubscriber($productid,$msisdn);
		}
		return $instances[$productid.':'.$msisdn];
	}

	/*
	*********************************
	**
	**	REQUIRES MSISDN AND PRODUCTID
	**	USEAGE 
	**	$ms_service =& Terragon::getMS([MSISDN],[PRODCUTID]);
	*********************************
	*/

	public static function &getMS($msisdn,$productid)
	{
		$productid = $productid;
		static $instances;
		if (!isset ($instances)) 	$instances = array ();
		if (empty($instances[$msisdn.':'.$productid])) {
			import('libraries.MsisdnService');	
			$instances[$msisdn.':'.$productid] = new MsisdnService($msisdn,$productid);
		}
		return $instances[$msisdn.':'.$productid];
	}

	

	/*
	*********************************
	**
	**	REQUIRES MSISDN AND PRODUCTID
	**	USEAGE 
	**	$ms_service =& Terragon::getMU([MSISDN],[PRODCUTID]);
	*********************************
	*/

	public static function &getMU($msisdn,$productid)
	{
		$productid = $productid;
		static $instances;
		if (!isset ($instances)) 	$instances = array ();
		if (empty($instances[$msisdn.':'.$productid])) {
			import('libraries.MsisdnUpdate');	
			$instances[$msisdn.':'.$productid] = new MsisdnUpdate($msisdn,$productid);
		}
		return $instances[$msisdn.':'.$productid];
	}
	

	/*
	*********************************
	**	USEAGE 
	**  $sdp =& Terragon::getSDP([SERVICEID]); 
	*********************************
	*/
	public static function &getSDP($serviceid)
	{
		static $instances;
		if (!isset ($instances)) 	$instances = array ();
		if (empty($instances[$serviceid])) {	
			import('libraries.SDP');
			$instances[$serviceid] = new SDP($serviceid);
		}
		return $instances[$serviceid];
	}

	/*
	*********************************
	**
	**	GET URL QUERY PARAMETER
	**	USEAGE 
	**	$URL = Terragon::get_url();
	*********************************
	*/

	public static function &get_url()
	{
		$url = $_SERVER['QUERY_STRING'];
		return $url;
	}

	/*
	*********************************
	**
	**	GET FULL URL STRING
	**	USEAGE 
	**	$URL = Terragon::get_full_url();
	*********************************
	*/

	public static function &get_full_url()
	{
		$url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		return $url;
	}	

	/*
	*********************************
	**
	**	GET site baseurl
	**	USEAGE 
	**	$baseurl = Terragon::get_baseurl();
	*********************************
	*/

	public static function &get_baseurl()
	{
		$s_config =& get_config();		
		$baseurll  =  $s_config['base_url'];
		return $baseurll;
	}	

	/*
	*********************************
	**
	**	SET AND GET WARNING RAISED IN APPLICATION
	**	USEAGE 
	**  SET WARNING  
	**	Terragon::raise_warning ('set', array('msg' =>'warning message','type'=>'alert'));
	**  GET WARNING
	**	$error = Terragon::raise_warning('get');
	*********************************
	*/

	public static function raise_warning($type,$params = array())
	{	
		static $DangerType, $Danger;
		if (!isset ($DangerType)) 	$DangerType = array ();
		if (!isset ($Danger)) 	$Danger = array ();
	
		$param = array(
			'show'=>1,
			'api'=>0,
			'msg'=>'',
			'notice_class'=>'notif success',
			'error_class'=>'notif warning',
			'alert_class'=>'notif error alert',
			'type'=>''
		);
		foreach($param as $c=>$v){
			if(isset($params[$c])) $param[$c] = $params[$c];
		}
	
		switch($type)
		{
			case "clear" :
				$DangerType = array ();
				$Danger = array ();
			break;
			case "set";	
				$DangerType[] = $param['type'];
				$Danger[] = $param['msg'];
				return $return_string = "<div class=\"notif warning\">". $param['msg']."</div>";
			break;
			default:
			case "get";
				if(count($Danger) == 0) 	return;		// Check if error exist, exist if no error exist		
				$return_string = "";	
				//print_r($this->Danger);
				if(count($Danger) != count($DangerType)) return; // handele this later			
				$Error = array_combine($Danger, $DangerType ); // Combine Error and Error type to determine erroor output
				
				foreach ($Error as $ErrorValue => $alertType)
				{
					if(empty($ErrorValue))		continue;
					switch($alertType)
					{ case "alert":
						{
							$return_string .= "<div class=\"{$param['alert_class']}\">". $ErrorValue;
							if($param['show'] == 1)	$return_string .=" <a href=\"#\" class=\"close\">x</a>";
							$return_string .="</div>";	
						}break;
						case "error":
						{
							$return_string .= "<div class=\"{$param['error_class']}\">". $ErrorValue;
							if($param['show'] == 1)	 $return_string .=" <a href=\"#\" class=\"close\">x</a>";
							$return_string .="</div>";	
						}
						break;
						case "notice":
							{
								$return_string .= "<div class=\"{$param['notice_class']}\">". $ErrorValue;
								if($param['show'] == 1)  $return_string .=" <a href=\"#\" class=\"close\">x</a>";
								$return_string .="</div>";	
							}
						break;
						default : 
							$return_string .= "<div >". $ErrorValue;
							if($param['show'] == 1)  $return_string .=" <a href=\"#\" class=\"close\">x</a>";
							$return_string .="</div>";	
						break;
					}
					
					if($param['api'] == 1){
						$return_string =  $ErrorValue;
					}
				}
				return $return_string;
			break;
		}
	}		
}
?>