<?php  
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
defined( 'TERRAGON' ) or die( 'Restricted access' );

$s_config['base_url']	= 'http://localhost/Slim';

$s_config['index_page'] = '?';

$s_config['language']	= 'english';

$s_config['charset'] = 'UTF-8';

$s_config['date_format'] = "jS F, Y";

$s_config['cookie_len'] = (60*60*24*7);

$s_config['site_error'] = 'database'; //database | raise | halt

$s_config['cookie_len_kill'] = (60*60*24);

// database setting
// dbconnection for shortcode 5033
$s_config['db_database'] = 'mtncp'; 

$s_config['db_user'] = 'mtncp';

$s_config['dp_password'] = 'mtncp'; 

$s_config['db_host'] = 'localhost'; 

$s_config['db_prefix'] = 'sdp_';

$s_config['db_error'] = 'no'; // report | no | yes

// dbconnection for shortcode 30046

$s_config['db_host_2'] 		= 'mtnvas-1457347914.eu-west-1.elb.amazonaws.com'; 

$s_config['db_database_2'] 	= 'mtn_v2'; 

$s_config['db_user_2'] 		= 'terravas';

$s_config['dp_password_2'] 	= 'terravasapp'; 

// redis settings

$s_config['redis_scheme'] 	= 'tcp'; 

$s_config['redis_host'] 	= '127.0.0.1'; 

$s_config['redis_port'] 	= '6379'; 

$s_config['redis_database'] = '1'; 


// mongo connection 

$s_config['mongo_host'] 	= ''; 

$s_config['mongo_port'] 	= ''; 

$s_config['mongo_user'] 	= ''; 

$s_config['mongo_pass'] 	= ''; 

$s_config['mongo_db'] 		= ''; 

$s_config['mongo_safety'] 	= ''; 

$s_config['mongo_flag'] 	= ''; 

// rabbitmq connection 

// $s_config['rabbit_host'] 	= '162.13.221.160'; 

// $s_config['rabbit_port'] 	= '5672'; 

// $s_config['rabbit_username'] 	= 'eti-administrator'; 

// $s_config['rabbit_passkey'] 	= '@eti_123'; 

//$s_config['rabbit_host'] 	= 'rmqtmoni-1924396886.eu-west-1.elb.amazonaws.com'; 
$s_config['rabbit_host'] 	= '52.210.131.160'; 

$s_config['rabbit_port'] 	= '5672'; 

$s_config['rabbit_username'] 	= 'appadmin'; 

$s_config['rabbit_passkey'] 	= 'teraappadmin1#'; 

//$s_config['es_server']	= 'http://52.51.46.189:9200';

$s_config['es_server']	= 'http://elastic-lb-752364616.eu-west-1.elb.amazonaws.com:8500';

$s_config['index']		= 'mtnsdp';

$s_config['es_host']	= 'elastic-lb-752364616.eu-west-1.elb.amazonaws.com';

$s_config['es_port']	= '8500';

?>
