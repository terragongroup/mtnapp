<?php 
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
*/
defined( 'TERRAGON' ) or die( 'Restricted access' );
class Timport
{
	function &file_import( $filePath, $base = null, $key = '' )
	{
		static $paths;
	
		if (!isset($paths)) {
			$paths = array();
		}
	
		$keyPath = $key ? $key . $filePath : $filePath;
	
		if (!isset($paths[$keyPath]))
		{
			if ( ! $base ) {
				$base =  TERRAGON_BASE;
			}
	
			$parts = explode( '.', $filePath );
			
			$path  = str_replace( '.', DS, $filePath );
			
			$rs   = require_once ($base.DS.$path.'.php');			
			$paths[$keyPath] = $rs;
			/*
			if(is_file($base.DS.$path.'.php')){
				$rs   = require_once ($base.DS.$path.'.php');			
				$paths[$keyPath] = $rs;
			}
			*/
		}
		return $paths[$keyPath];
	}
}

function import( $filePath, $base = null, $key = '' )
{
	$import = new Timport();
	$paths = $import->file_import($filePath, $base , $key);
	return $paths;
}
?>