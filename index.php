<?php
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
//error_reporting (0);

require 'defines.php';
require 'import.php';
require 'vendor/autoload.php';

import('function');
import('factory');
import('Slim.Slim');

Terragon::load_libraries('Options');

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$redis 	=& Terragon::redis();  
$db 	=& Terragon::dbconnect(); 


$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"/>
            <title>Slim Framework for PHP 5</title>
            <style>
                html,body,div,span,object,iframe,
                h1,h2,h3,h4,h5,h6,p,blockquote,pre,
                abbr,address,cite,code,
                del,dfn,em,img,ins,kbd,q,samp,
                small,strong,sub,sup,var,
                b,i,
                dl,dt,dd,ol,ul,li,
                fieldset,form,label,legend,
                table,caption,tbody,tfoot,thead,tr,th,td,
                article,aside,canvas,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section,summary,
                time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent;}
                body{line-height:1;}
                article,aside,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section{display:block;}
                nav ul{list-style:none;}
                blockquote,q{quotes:none;}
                blockquote:before,blockquote:after,
                q:before,q:after{content:'';content:none;}
                a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;}
                ins{background-color:#ff9;color:#000;text-decoration:none;}
                mark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold;}
                del{text-decoration:line-through;}
                abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help;}
                table{border-collapse:collapse;border-spacing:0;}
                hr{display:block;height:1px;border:0;border-top:1px solid #cccccc;margin:1em 0;padding:0;}
                input,select{vertical-align:middle;}
                html{ background: #EDEDED; height: 100%; }
                body{background:#FFF;margin:0 auto;min-height:100%;padding:0 30px;width:800px;color:#666;font:14px/23px Arial,Verdana,sans-serif;}
                h1,h2,h3,p,ul,ol,form,section{margin:0 0 20px 0;}
                h1{color:#333;font-size:20px;}
                h2,h3{color:#333;font-size:14px;}
                h3{margin:0;font-size:12px;font-weight:bold;}
                ul,ol{list-style-position:inside;color:#999;}
                ul{list-style-type:square;}
                code,kbd{background:#EEE;border:1px solid #DDD;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:0 4px;color:#666;font-size:12px;}
                pre{background:#EEE;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:5px 10px;color:#666;font-size:12px;}
                pre code{background:transparent;border:none;padding:0;}
                a{color:#70a23e;}
                header{padding: 30px 0;text-align:center;}
            </style>
        </head>
        <body>
	
            <section>
                 <h2>Mtn SDP</h2>
                <p>
                   	http://go.twinpinenetwork.com:81/api/subscribe/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]&partner=[partner]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/unsubscribe/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/check_subscriber_status/?cds_campaign_id=[cds_campaign_id]&user_id=[user_id]
				</p>
				
				<p>
					http://go.twinpinenetwork.com:81/api/direct_billing_api/?cds_campaign_id=[cds_campaign_id]&msisdn=[msisdn]&partner=[partner]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/unsubscribe_user_sms_service/?msisdn=[msisdn]&productID=[productID]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/messaging_api/?user_id=[user_id]&content=[content]&ad_id=[ad_id]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/renewal_api/?user_id=[user_id]&price_code=[price_code]&cid=[cds_campaign_id]
				</p>
				<!--
				<p>
					http://go.twinpinenetwork.com:81/api/msisdn_fetcher/?ms=[ms]&ad_id=[ad_id]&redir_url=[redir_url]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/sdp/subscribe/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/sdp/update/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/sdp/unsubscribe/?content_id=[product_id]&msisdn=[msisdn]&partner=[partner]
				</p>
				<p>
					http://go.twinpinenetwork.com:81/api/cpudate/
                </p>
				 -->
            </section>
           
           
        </body>
    </html>
EOT;
        echo $template;
    }
);

/*
*********************************
**
**	CP SUBSCRIBE URL
**
*********************************
*/

$app->get('/api/request_subscribtion/', function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');
	$tool 		=& Terragon::load_libraries('db');

	$camp_id 		= get_param('cds_campaign_id');
	$user_id 		= get_param('user_id');
	$ref_id 		= get_param('ref_id');
	$partner 		= get_param('partner');
	$HTTP_REFERER 	= get_param('HTTP_REFERER','SERVER');
	$refurl 		= get_param('referralUrl') ." | ". $HTTP_REFERER;


	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	$_msisdn = $msisdnobj->msisdn;
	$prefix = substr($_msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$sender_url = "http://localhost/tlratendate/subscribe/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}

	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($user_id == '')
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	}
	$content->get(array('campaign_id'=>$camp_id));
	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	if($msisdnobj->exist() == false){
		$log->log('error','No records of MSISDN FOUND',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse('No records of MSISDN FOUND',1);
	}

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Account not yet configured',2);
	}

	$msisdnobj->addRefId($ref_id);
	//echo $content->product_id;
	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);
	$mtnresponse = $msisdnobj->subscribe($content->product_id, $content->shortcode, $content->service_id); 
	switch ($mtnresponse) {
		case '22007233':
			$ms_service->PushService('pending',$partner);
			$log->log('success','Request Subscription sent to user',array('request_subscribtion'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->successResponse('Request Subscription sent to user');
		break;
		case '22007696':
			$log->log('error','Request Previously Sent to user',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			//$log->log('error','Request Previously Sent to user',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Request Previously Sent to user',4);
		break;
		case '22007330' :
			$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Billing Failed. This user has insufficient credit',1); 
		break;
		case '22007238':
		case '22007201':
			$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			//$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
		break;
		case '00000000':
		    //$content->cpupdate($msisdnobj->msisdn,'sub');
			$ms_service->PushService('active',$partner);
			$ms_service->subscribe($content->duration);
			$log->log('success','User Subscribed to service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->successResponse('User has been successfully billed');
		break;
		default:
			$log->log('error','Request Subscription failed',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Request Subscription failed',3);
		break;
	}
});


$app->get('/api/request_subscribtion_test/', function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');
	$tool 		=& Terragon::load_libraries('db');

	$camp_id 		= get_param('cds_campaign_id');
	$user_id 		= get_param('user_id');
	$ref_id 		= get_param('ref_id');
	$partner 		= get_param('partner');
	$HTTP_REFERER 	= get_param('HTTP_REFERER','SERVER');
	$refurl 		= get_param('referralUrl') ." | ". $HTTP_REFERER;

	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	$_msisdn = $msisdnobj->msisdn;
	$prefix = substr($_msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}


	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$sender_url = "http://localhost/tlratendate/subscribe/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}

	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($user_id == '')
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	}
	$content->get(array('campaign_id'=>$camp_id));
	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	
	//echo $msisdnobj->msisdn;

	die;
	if($msisdnobj->exist() == false){
		$log->log('error','No records of MSISDN FOUND',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse('No records of MSISDN FOUND',1);
	}

	// if(empty($content->product_id)){
	// 	$log->log('error','Account not yet configured',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
	// 	$response->errorResponse('Account not yet configured',2);
	// }

	// $msisdnobj->addRefId($ref_id);
	// $ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);
	// $mtnresponse = $msisdnobj->subscribe($content->product_id, $content->shortcode, $content->service_id); 
	// switch ($mtnresponse) {
	// 	case '22007233':
	// 		$ms_service->PushService('pending',$partner);
	// 		$log->log('success','Request Subscription sent to user',array('request_subscribtion'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->successResponse('Request Subscription sent to user');
	// 	break;
	// 	case '22007696':
	// 		$log->log('error','Request Previously Sent to user',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		//$log->log('error','Request Previously Sent to user',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->errorResponse('Request Previously Sent to user',4);
	// 	break;
	// 	case '22007330' :
	// 		$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->errorResponse('Billing Failed. This user has insufficient credit',1); 
	// 	break;
	// 	case '22007238':
	// 	case '22007201':
	// 		$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		//$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
	// 	break;
	// 	case '00000000':
	// 	    $content->cpupdate($msisdnobj->msisdn,'sub');
	// 		$ms_service->PushService('active',$partner);
	// 		$ms_service->subscribe($content->duration);
	// 		$log->log('success','User Subscribed to service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->successResponse('User has been successfully billed');
	// 	break;
	// 	default:
	// 		$log->log('error','Request Subscription failed',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
	// 		$response->errorResponse('Request Subscription failed',3);
	// 	break;
	// }
});

$app->get('/api/request_subscribtion_msisdn/', function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');
	$tool 		=& Terragon::load_libraries('db');

	$camp_id 		= get_param('cds_campaign_id');
	$msisdn 		= get_param('msisdn');
	$ref_id 		= get_param('ref_id');
	$partner 		= get_param('partner');
	$HTTP_REFERER 	= get_param('HTTP_REFERER','SERVER');
	$refurl 		= get_param('referralUrl') ." | ". $HTTP_REFERER;

	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "Unknown msisdn";

	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	$_msisdn = $msisdnobj->msisdn;
	$prefix = substr($_msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	}
	$content->get(array('campaign_id'=>$camp_id));
	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn); 
	if($msisdnobj->exist('msisdn') == false){
		$log->log('error','No records of MSISDN FOUND',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse('No records of MSISDN FOUND',1);
	}

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Account not yet configured',2);
	}

	$msisdnobj->addRefId($ref_id);
	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);
	$mtnresponse = $msisdnobj->subscribe($content->product_id, $content->shortcode, $content->service_id); 
	switch ($mtnresponse) {
		case '22007233':
			$ms_service->PushService('pending',$partner);
			$log->log('success','Request Subscription sent to user',array('request_subscribtion'=>'subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
			$response->successResponse('Request Subscription sent to user');
		break;
		case '22007696':
			$msisdnobj->active();
			$log->log('error','Request Previously Sent to user',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		//	$log->log('error','Request Previously Sent to user',array('cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
			$response->errorResponse('Request Previously Sent to user',4);
		break;
		case '22007330' :
			$msisdnobj->active();
			$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Billing Failed. This user has insufficient credit',1); 
		break;
		case '22007238':
		case '22007201':
			$msisdnobj->active();
			$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			//$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
		break;
		case '00000000':
		    $content->cpupdate($msisdnobj->msisdn,'sub');
			$ms_service->PushService('active',$partner);
			$ms_service->subscribe($content->duration);
			$log->log('success','User Subscribed to service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->successResponse('User has been successfully billed');
		break;
		default:
			$log->log('error','Request Subscription failed',array('apicall'=>'request_subscribtion','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
			$response->errorResponse('Request Subscription failed',3);
		break;
	}
});

$app->get('/api/subscribe/', function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');
	$tool 		=& Terragon::load_libraries('db');

	$camp_id 		= get_param('cds_campaign_id');
	$user_id 		= get_param('user_id');
	$partner 		= get_param('partner');
	$ref_id 		= get_param('ref_id');
	$HTTP_REFERER 	= get_param('HTTP_REFERER','SERVER');
	$refurl 		= get_param('referralUrl') ." | ". $HTTP_REFERER;

	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	$_msisdn = $msisdnobj->msisdn;
	$prefix = substr($_msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}


	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$sender_url = "http://localhost/tlratendate/subscribe/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}

	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($user_id == '')
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));
	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}
	
	$msisdnobj =& Terragon::getUC($camp_id,$user_id); 
	if($msisdnobj->exist() == false){
		$log->log('error','No records of MSISDN FOUND',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
		$response->errorResponse('No records of MSISDN FOUND',1);
	}
	$msisdnobj->addRefId($ref_id);
	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Account not yet configured',2);
	}


	$mtnresponse = $msisdnobj->subscribe($content->product_id, $content->shortcode, $content->service_id); 
	
	// 22007628
	// 22007696 = "request previously sent"
	// 22007233 => "request sent to user"
	switch ($mtnresponse) {
		case '00000000':
		    //$content->cpupdate($msisdnobj->msisdn,'sub');
			$ms_service->PushService('active',$partner);
			$ms_service->subscribe($content->duration);
			$log->log('success','User Subscribed to service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->successResponse('User has been successfully billed');
		break;
		case '22007233':
			$msisdnobj->active();
			$ms_service->PushService('pending',$partner);
			$log->log('success','Request Subscription sent to user',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->successResponse('Request Subscription sent to user');
		break;
		case '22007696':
			$msisdnobj->active();
			$log->log('error','Request Previously Sent to user',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			//$log->log('error','Request Previously Sent to user',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Request Previously Sent to user',4);
		break;
		case '22007330' :
			$msisdnobj->active();
			$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			
			$response->errorResponse('Billing Failed. This user has insufficient credit',1); 
		break;
		case '22007238':
		case '22007201':
			$msisdnobj->active();
			$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			//$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			
			$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
		break;
		default:
			$log->log('error','Billing Failed. This might be because of an issue with the Network Operator',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));

			$response->errorResponse('Billing Failed. This might be because of an issue with the Network Operator',3);
		break;
	}

});
/*
*********************************
**
**	CP UNSUBSCRIBE URL
**
*********************************
*/
$app->get('/api/unsubscribe/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');
	$tool 		=& Terragon::load_libraries('db');

	$camp_id 	= get_param('cds_campaign_id');
	$user_id 	= get_param('user_id');



	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$sender_url = "http://localhost/tlratendate/unsubscribe/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}

	// echo "hi";

	// echo $user_id;
	// echo $camp_id;

	// die;

	
	$error = array();
	if($camp_id == '')
		$error[] = "Ad Identity is Missing"; 
	if($user_id == '')
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));
	if($content->exist() == false){
		$log->log('error','Invalid Ad Id',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);
	if($msisdnobj->exist() == false){
		$log->log('error','No records of MSISDN',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('No records of MSISDN',1);
	}

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Account not yet configured',2);
	}

	$mtnresponse = $msisdnobj->unsubscribe($content->product_id ,  $content->shortcode, $content->service_id); 

	

	switch ($mtnresponse) {
		case '00000000':

		    $content->cpupdate($msisdnobj->msisdn,'unsub');

			$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

			$ms_service->setStatus('inactive');

			$log->log('success','User has been successfully unsubscribed',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->successResponse('User has been successfully unsubscribed');
		break;
		case '22007219' :
			$log->log('error','User not subscribed to service',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->errorResponse('User not subscribed to service',4); 
		break;
		default:
			$log->log('error','Unsubscription failed',array('apicall'=>'unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->errorResponse('Unsubscription failed',3);
		break;
	}
});
/*
*********************************
**
**	CP CHECK SUBSCRIBER STATUS
**
*********************************
*/
$app->get('/api/check_subscriber_status/',  function () use ($app) {
	$log 		=& Terragon::load_libraries('LogRequest');
	$content 	=& Terragon::load_libraries('Campaign');
	$response 	=& Terragon::load_libraries('ApiResponse');

	$camp_id 		= get_param('cds_campaign_id');
	$user_id 		= get_param('user_id');

	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$sender_url = "http://localhost/tlratendate/check_subscriber_status/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}
	
	$error = array();
	if ($camp_id == '') 
		$error[] = "Ad Identity is Missing";

	if ($user_id == '') 
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('success',implode(' and ',$error),array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse(implode(' and ',$error));
	} 

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('success','Invalid Ad Id',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);

	if($msisdnobj->exist() == false)
	{
		$response->errorResponse('No records of MSISDN',1);
	}

	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

	if($ms_service->exist() == true){ // check redis first
		$status = $ms_service->getStatus();
	}else{ // check mysql if not found on redis
		$status = $msisdnobj->getStatus();
	}

	switch ($status) {
		case 'active':
			$log->log('success','The user is subscribed to this service',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
       		$response->successResponse('The user is subscribed to this service');
		break;
		default:
			$log->log('success','The user is unsubscribe form this service',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->successResponse('The user is unsubscribe form this service');
		break;
	}
});
/*
*********************************
**
**	CP CHECK SUBSCRIBER STATUS
**
*********************************
*/
$app->get('/api/check-subscriber-status/',  function () use ($app) {
	$log 		=& Terragon::load_libraries('LogRequest');
	$content 	=& Terragon::load_libraries('Campaign');
	$response 	=& Terragon::load_libraries('ApiResponse');

	$camp_id 		= get_param('cds_campaign_id');
	$user_id 		= get_param('user_id');

	if($camp_id == 'tl-c-21012014262101201443'){
		$log->log('error','RATENDATE',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$sender_url = "http://localhost/tlratendate/check_subscriber_status/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id);
 		$output = curl($sender_url);
 		header('Content-Type: application/json');
		echo $output;
		exit();
	}
	
	$error = array();
	if ($camp_id == '') 
		$error[] = "Ad Identity is Missing";

	if ($user_id == '') 
		$error[] = "Unknown user_id";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse(implode(' and ',$error));
	} 

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('error','Invalid Ad Id',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);

	if($msisdnobj->exist() == false)
	{
		$response->errorResponse('No records of MSISDN',1);
	}

	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

	if($ms_service->exist() == true){ // check redis first
		$status = $ms_service->getStatus();
	}else{ // check mysql if not found on redis
		$status = $msisdnobj->getStatus();
	}

	switch ($status) {
		case 'active':
			$log->log('success','The user is subscribed to this service',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
       		$response->successResponse('The user is subscribed to this service');
		break;
		default:
			$log->log('success','The user is unsubscribe form this service',array('apicall'=>'subscriber_status','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->successResponse('The user is unsubscribe form this service');
		break;
	}
});

/*
*********************************
**
**	CP MSISDN FETCHER
**
*********************************
*/
$app->get('/api/msisdn_fetcher/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$msisdn 	= get_param('ms');
	$camp_id 	= get_param('ad_id');
	$redir_url 	= get_param('redir_url');

	$error = array();
	if($camp_id == ''){
		$statusCode = array(
			'code'=>2,
			'message'=>'Ad Identity is Missing'
		);
		$log->log('error','Ad Identity is Missing' ,array('apicall'=>'msisdn_fetcher','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'redir_url'=>$redir_url,'ip'=> $_SERVER['REMOTE_ADDR'] ));
		redirect(prep_url($redir_url.'&responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message']));
		//$error[] = "Ad Identity is Missing";
	}

	if($msisdn == ''){
		$statusCode = array(
			'code'=>1,
			'message'=>'Cannot get user msisdn'
		);
		$log->log('error','Cannot get user msisdn' ,array('apicall'=>'msisdn_fetcher','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'redir_url'=>$redir_url,'ip'=> $_SERVER['REMOTE_ADDR'] ));
		redirect(prep_url($redir_url.'&responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message']));
	}


	if($redir_url == ''){
		$statusCode = array(
			'code'=>5,
			'message'=>'Redirect Url not set'
		);
		$log->log('error','Redirect Url not set' ,array('apicall'=>'msisdn_fetcher','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'redir_url'=>$redir_url,'ip'=> $_SERVER['REMOTE_ADDR'] ));
		redirect(prep_url($redir_url.'&responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message']));
		//$error[] = "Redirect Url not found";
	}

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$statusCode = array(
			'code'=>2,
			'message'=>'Invalid Ad Id'
		);
		$log->log('error','Invalid Ad Id',array('apicall'=>'msisdn_fetcher','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'redir_url'=>$redir_url,'ip'=> $_SERVER['REMOTE_ADDR'] ));
		redirect(prep_url($redir_url.'&responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message']));
		//$response->errorResponse('Invalid Ad Id',2);
	}

	

	$statusCode = array(
		'code'=>0,
		'message'=>'OK'
	);

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$msisdnobj->add();
	}

	$log->log('success','msisdn found ',array('apicall'=>'msisdn_fetcher','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'redir_url'=>$redir_url,'ip'=> $_SERVER['REMOTE_ADDR'] ));

	$url_pos = strpos($redir_url, "?");
	if($url_pos >= 1){
		redirect(prep_url($redir_url.'&responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message'].'&userId='.$msisdnobj->user_id));
	}
	else{
		redirect(prep_url($redir_url.'?responseCode='.$statusCode['code'].'&responseMessage='.$statusCode['message'].'&userId='.$msisdnobj->user_id));
	}
});
/*
*********************************
**
**	CP MSISDN FETCHER FOR RATE AND DATE
**
*********************************
*/
$app->get('/api/msisdn_fetcher_tl/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	
	$msisdn 	= get_param('ms');
	$camp_id 	= get_param('ad_id');
	$redir_url 	= get_param('redir_url');
	$ref_id 	= get_param('ref_id');

	$error = array();
	if($camp_id == '') 
		$error[] = "Ad Identity is Missing";

	if($redir_url == '')
		$error[] = "Redirect Url not found";

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}
	$url = "http://46.38.169.103/tlratendate/msisdn_fetcher/";
	redirect($url.'?msisdn='.$msisdn.'&ad_id='.$camp_id.'&redir_url='.$redir_url);
});
/*
*********************************
**
**	CP MSISDN FETCHER
**
*********************************
*/
$app->get('/api/generate_user_id/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$msisdn 	= get_param('msisdn');
	$camp_id 	= get_param('cid');
	
	$error = array();
	if($camp_id == '') 
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";

	
	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'generate_user_id','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('error','Invalid Ad Id',array('apicall'=>'generate_user_id','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$msisdnobj->add();
	}
	$log->log('success','User id found',array('apicall'=>'generate_user_id','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));

	$response->successResponse($msisdnobj->user_id);

	
});

/*
*********************************
**
**	TMONI DCB UNSUBCRIPTION
**
*********************************
*/

$app->get('/api/tmoni_unsubscribe/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$msisdn 	= get_param('msisdn');
	$camp_id 	= get_param('cds_campaign_id');
	
	$error = array();
	if($camp_id == '') 
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";

	
	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('error','Invalid Ad Id',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Invalid Ad Id',202);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$log->log('error','No records of MSISDN',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'user_id'=>$msisdn));
		$response->errorResponse('No records of MSISDN',100);
	}

	

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Account not yet configured',300);
	}

	$user_id = $msisdnobj->user_id;

	if(empty($user_id)){
		$log->log('error','Cannot identify userid',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Cannot identify userid',304);
	}

	// $log->log('success','User has been successfully unsubscribed',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
	// $response->successResponse(array('status'=>true,'error_code'=>'0','content'=>'User has been successfully unsubscribed' ,'userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id) );

	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

	$mtnresponse = $msisdnobj->unsubscribe($content->product_id ,  $content->shortcode, $content->service_id); 

	switch ($mtnresponse) {
		case '00000000':
		    $content->cpupdate($msisdnobj->msisdn,'unsub');
			$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);
			$ms_service->setStatus('inactive');
			$log->log('success','User has been successfully unsubscribed',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->successResponse(array('status'=>true,'error_code'=>'0','content'=>'User has been successfully unsubscribed' ,'userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id) );
		break;
		case '22007219' :
			$log->log('error','User not subscribed to service',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->errorResponse('User not subscribed to service',500);
		break;
		default:
			$log->log('error','Unsubscription failed',array('apicall'=>'tmoni-unsubscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->errorResponse('Unsubscription failed',600);
		break;
	}
});

/*
*********************************
**
**	TMONI DCB SUBCRIPTION
**
*********************************
*/

$app->get('/api/tmoni_subscribe/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$msisdn 	= get_param('msisdn');
	$camp_id 	= get_param('cds_campaign_id');
	$partner = "sdp";
	
	$error = array();
	if($camp_id == '') 
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";


	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	
	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('error','Invalid Ad Id',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Invalid Ad Id',600);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$msisdnobj->add();
	}

	$user_id = $msisdnobj->user_id;

	if(empty($user_id)){
		$log->log('error','Cannot identify userid',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Cannot identify userid',700);
	}
	$ms_service =& Terragon::getMS($msisdnobj->msisdn,$content->product_id);

	if(empty($content->product_id)){
		$log->log('error','Account not yet configured',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Account not yet configured',800);
	}

	// $log->log('success','Request Subscription sent to user',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
	// $response->successResponse(array('status'=>true,'error_code'=>'1','content'=>'Request Subscription sent to user','userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id));

	$mtnresponse = $msisdnobj->subscribe($content->product_id, $content->shortcode, $content->service_id); 

	switch ($mtnresponse) {
		case '00000000':
		    $content->cpupdate($msisdnobj->msisdn,'sub');
			$ms_service->PushService('active',$partner);
			$ms_service->subscribe($content->duration);
			$log->log('success','User Subscribed to service',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->successResponse(array('status'=>true,'error_code'=>'0','content'=>'User has been successfully billed and Subscribed to service','userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id));
		break;
		case '22007233':
			$log->log('success','Request Subscription sent to user',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			// $response->successResponse(array('status'=>true,'error_code'=>'201','content'=>'Request Subscription sent to user','userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id));
			$response->errorResponse('Request Subscription sent to user',201);
		break;
		case '22007696':
			$log->log('error','Request Previously Sent to user',array('apicall'=>'subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			//$log->log('error','Request Previously Sent to user',array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id,'partner'=>$partner));
			$response->errorResponse('Request Previously Sent to user',202);
		break;
		case '22007330' :
			$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->errorResponse('Billing Failed. This user has insufficient credit',300);
		break;
		case '22007238':
		case '22007201':
			$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
			$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',304);
		break;
		default:
			$log->log('error','Billing Failed. This might be because of an issue with the Network Operator',array('apicall'=>'tmoni_subscribe','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->errorResponse('Billing Failed. This might be because of an issue with the Network Operator '.$mtnresponse,500);
		break;
	}
});

/*
*********************************
**
**	CP Tmoni DIRECT BILLING URL
**
*********************************
*/
$app->get('/api/tmoni_direct_billing_api/',  function () use ($app) {
	
	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$googleurlapi 	=& Terragon::load_libraries('googleurlapi');
	$log 			=& Terragon::load_libraries('LogRequest');

	$camp_id 		= get_param('cds_campaign_id');
	$msisdn 		= get_param('msisdn');
	$price 			= get_param('price');
	
	$error = array();
	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($price == '')
		$error[] = "Price not sent";

	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	
	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'tmoni_direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse(implode(' and ',$error));
	} 

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$msisdnobj->add();
	}

	$user_id = $msisdnobj->user_id;

	if(empty($user_id)){
		$log->log('error','Cannot identify userid',array('apicall'=>'tmoni_direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
		$response->errorResponse('Cannot identify userid',2);
	}

	$resp = $msisdnobj->tmonibill($price);

	switch ($resp)
	{
		case '0':
			//$url = Options::$_verify_url.'?cds='.$camp_id.'&code='.$msisdnobj->gerarkeyuserid(5);
			//$short_url = $googleurlapi->shorten($url);
			//$sms_content = 'You have successfully purchased this product. Please follow this link: '.$short_url;
			$sms_content = 'You have successfully purchased this product';
			//now send sms to customer okay...
			send_sms($msisdnobj->msisdn,$sms_content,$content->shortcode,$content->service_id);
			$log->log('success','User has successfully purchased this product',array('apicall'=>'tmoni_direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$response->successResponse( array('content'=>'User has successfully purchased this product','userid'=>$user_id,'msisdn'=>$msisdnobj->msisdn,'cds_campaign_id'=>$camp_id) );
			
		break;
		default:
			$log->log('error','Insufficient credit',array('apicall'=>'tmoni_direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn));
			$final_content = "You don't have sufficient credit to purchase this product. Please browse through our store for more exciting products. Thanks.";
			send_sms($msisdnobj->msisdn,$final_content,$content->shortcode,$content->service_id);			
			$response->errorResponse('Insufficient credit',4);
		break;
	}

	
});
/*
*********************************
**
**	CP MSISDN FETCHER
**
*********************************
*/
$app->get('/api/fetch_user_id/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');

	$msisdn 	= get_param('msisdn');
	$camp_id 	= get_param('cid');
	
	$error = array();
	if($camp_id == '') 
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";

	
	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$response->errorResponse('No records of MSISDN',2);
	}

	$response->successResponse($msisdnobj->user_id);

	
});

/*
*********************************
**
**	CP DIRECT BILLING URL
**
*********************************
*/
$app->get('/api/direct_billing_api/',  function () use ($app) {
	
	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$googleurlapi 	=& Terragon::load_libraries('googleurlapi');
	$log 			=& Terragon::load_libraries('LogRequest');

	$camp_id 		= get_param('cds_campaign_id');
	$msisdn 		= get_param('msisdn');
	$partner 		= get_param('partner');
	
	$error = array();
	if($camp_id == '')
		$error[] = "Ad Identity is Missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($partner == '')
		$error[] = "Who is our Partner?";


	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	if(!empty($error)){
		$log->log('error',implode(' and ',$error),array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	} 

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false)
	{
		$log->log('error','No records of MSISDN',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse('No records of MSISDN',1);
	}

	// echo "<pre>";print_r($content);echo "</pre>";
	// die;

	switch($content->content_category)
	{
		case 1: //entertainment content...	
			$resp = $msisdnobj->bill($content->price);
			switch ($resp)
			{
				case '0':
					$url = Options::$_verify_url.'?cds='.$camp_id.'&code='.$msisdnobj->gerarkeyuserid(5);
					$short_url = $googleurlapi->shorten($url);
					$sms_content = 'You have successfully purchased this product. Please follow this link: '.$short_url;
					//now send sms to customer okay...
					send_sms($msisdnobj->msisdn,$sms_content,$content->shortcode,$content->service_id);
					$log->log('success','User has successfully purchased this product',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					$response->successResponse('User has successfully purchased this product');
					
				break;
				default:
					$log->log('error','Insufficient credit, you will be redirected to error page...',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					$final_content = "You don't have sufficient credit to purchase this product. Please browse through our store for more exciting products. Thanks.";
					send_sms($msisdnobj->msisdn,$final_content,$content->shortcode,$content->service_id);			
					$response->errorResponse('Insufficient credit, you will be redirected to error page...',4);
				break;
			}
		break;
		case 2: //informative content
			$resp = $msisdnobj->subscribe($content->product_id); 
			switch ($resp)
			{
				case '00000000':

					$content->cpupdate($msisdnobj->msisdn,'sub');

					$response->successResponse('User has been successfully billed');

					$log->log('success','User has been successfully billed',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					
				break;
				case '22007330' :
					$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					$response->errorResponse('Billing Failed. This user has insufficient credit',1); 

				break;
				case '22007238':
				case '22007201':
					$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
				break;
				default:
					$log->log('error','Billing Failed. This might be because of an issue with the Network Operator',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
					$response->errorResponse('Billing Failed. This might be because of an issue with the Network Operator',3);
				break;
			}
		break;
		default:
			$log->log('error','You are not enabled for direct billing',array('apicall'=>'direct_billing_api','cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
			$response->errorResponse('You are not enabled for direct billing',3);
		break;
	}
});
/*
*********************************
**
**	CP UNSUBSCRIBE USER SMS SERVICE
**
*********************************
*/
$app->get('/api/unsubscribe_user_sms_service/',  function () use ($app) {
	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$tool 			=& Terragon::load_libraries('db');

	$msisdn 		= get_param('msisdn');
	$product_id 	= get_param('productID');
	
	$error = array();
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($product_id == '')
		$error[] = "No Product ID stated";

	if(!empty($error))
	{
		$response->errorResponse(implode(' and ',$error));
	}
	
	$ms_service =& Terragon::getMS($msisdn,$product_id);

	$content->get(array('product_id'=>$product_id));

	if($content->exist() == false)
	{
		$response->errorResponse('The service does not exist.',2032);
	}
	
	if ($ms_service->exist() == false)
	{
		$response->errorResponse('Cannot identify this user against this service', 2031);
	}

	if ($ms_service->getServices() == 'active')
	{
		$ms_service->unsubscribe();
	}

	$content->cpupdate($msisdn,'unsub');

	$response->successResponse('User unsudscribed', 2031);

});
/*
*********************************
**
**	MESSAGE API URL
**
*********************************
*/
$app->get('/api/messaging_api/',  function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');

	$user_id 	    = get_param('user_id');
	$sms_content 	= get_param('content');
	$camp_id 		= get_param('ad_id');

	if($camp_id == 'tl-c-21012014262101201443')
	{
		$sender_url = "http://localhost/tlratendate/messaging/?userid=".urlencode($user_id)."&ad_id=".urlencode($camp_id)."&messaging=".rawurlencode($sms_content);
		$output = curl($sender_url);
		header('Content-Type: application/json');
		echo $output;
		exit();
	}

	$error = array();
	if($user_id == '')
		$error[] = "Ad Identity is Missing";
	if($sms_content == '')
		$error[] = "SMS Content is Missing";
	if($user_id == '')
		$error[] = "User Identity not found";

	if(!empty($error)){

		$response->errorResponse(implode(' and ',$error));
	}

	if(strlen($sms_content) > 160)
	{
		$response->errorResponse('Message too long',3);
	}

	$content->get(array('campaign_id'=>$camp_id), 'content_id');

	if($content->exist() == false)
	{
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);
	
	if($msisdnobj->exist() == false)
	{
		$response->errorResponse('User ID does not exist',1);
	}

	$sms_content = rawurldecode($sms_content);

	//echo $content->service_id;

	$data = send_sms($msisdnobj->msisdn,$sms_content,$content->shortcode,$content->service_id);

	if($data == false)
	{
		$response->errorResponse('Message sending failed' ,1);
	}
	// var_dump($data);
	// exit();
	$response->successResponse('Message Sent');
	
});


$app->get('/api/messaging_api_test/',  function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');

	$user_id 	    = get_param('user_id');
	$sms_content 	= get_param('content');
	$camp_id 		= get_param('ad_id');

	$error = array();
	if($user_id == '')
		$error[] = "Ad Identity is Missing";
	if($sms_content == '')
		$error[] = "SMS Content is Missing";
	if($user_id == '')
		$error[] = "User Identity not found";

	if(!empty($error)){

		$response->errorResponse(implode(' and ',$error));
	}

	if(strlen($sms_content) > 160)
	{
		$response->errorResponse('Message too long',3);
	}

	$content->get(array('campaign_id'=>$camp_id), 'content_id');

	if($content->exist() == false)
	{
		$response->errorResponse('Invalid Ad Id',2);
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);
	
	if($msisdnobj->exist() == false)
	{
		$response->errorResponse('User ID does not exist',1);
	}
	
	
	$sms_content = rawurldecode($sms_content);

	$msisdnobj->sendSms($sms_content,$content->service_id);

	$response->successResponse('Message Sent');
	
});
/*
*********************************
**
**	CP RENEWAL API URL
**
*********************************
*/
$app->get('/api/renewal_api/',  function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$log 			=& Terragon::load_libraries('LogRequest');

	$user_id 	    = get_param('user_id');
	$pid 			= get_param('price_code');
	$camp_id 		= get_param('cid');

	$error = array();
	
	if($user_id == '')
		$error[] = "User Identity is Missing";
	if($pid == '')
		$error[] = "Price Code is Missing";
	if($camp_id == '')
		$error[] = "Campaign Identity is Missing";

	if(!empty($error)){
		$log->log('success',implode(' and ',$error),array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse(implode(' and ',$error));
	}

	$content->get(array('campaign_id'=>$camp_id));

	if($content->exist() == false){
		$log->log('success','We cannot locate the campaign you are trying to bill for' ,array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('We cannot locate the campaign you are trying to bill for','Err03');
	}

	$price 	=& Terragon::load_model('price',$pid);

	if($price->exist() == false){
		$log->log('success','the price code you supply is invalid' ,array('cds_campaign_id'=>$camp_id,'user_id'=>$user_id));
		$response->errorResponse('the price code you supply is invalid','Err02');
	}

	$msisdnobj  =& Terragon::getUC($camp_id,$user_id);

	if($msisdnobj->exist() == false)
	{
		$log->log('success','No records of userid',array('cds_campaign_id'=>$camp_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse('No records of userid',1);
	}

	$resp = $msisdnobj->bill($price->codes);

	switch ($resp)
	{
		case '0':
			$response->successResponse('Successful Billing');
		break;
		default:	
			$response->errorResponse('Billing Not successful','Err00');
		break;
	}

});
/*
*********************************
**
**	GET SUBSCRIBER API
**
*********************************
*/
$app->get('/api/get_subscribers/',  function () use ($app) {
	$camp_id 	= get_param('camp_id');
	$status 	= get_param('status');
	$start 	    = get_param('start');
	$limit 		= get_param('limit');
	$status 	= empty($status) ? '': $status;
	$response 	=& Terragon::load_libraries('ApiResponse');
	$content 	=& Terragon::load_libraries('Campaign');
	$data = $content->get_subscribers($camp_id, $status , $start , $limit);
	$response->successResponse($data);
});
/*
*********************************
**
**	MTN SDP SUBSCRIBE URL
**
*********************************
*/
$app->get('/sdp/subscribe/',  function () use ($app) {

	$response 	=& Terragon::load_libraries('ApiResponse');
	$tool 		=& Terragon::load_libraries('db');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$product_id  = get_param('content_id');
	$msisdn     = get_param('msisdn');
	$partner    = get_param('partner');

	

	$error = array();
	if($product_id == '') 
		$error[] = "Content_id missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";

	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	if(!empty($error)){
		$log->log('spdupdate',implode(' and ',$error),array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));
	}else{
		$log->log('spdupdate','Subscription update ',array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
	}

	$content->get(array('product_id'=>$product_id));

	if($content->exist() == false){
		$response->errorResponse('Invalid Content id',2);
	}

	$ms_service =& Terragon::getMS($msisdn,$product_id);

	if ($ms_service->getStatus() != 'active')
	{
	  	$ms_service->PushService('active',$partner);
		$ms_service->subscribe($content->duration);   
		$content->cpupdate($msisdn,'sub',true);
	}

	$response->successResponse('User has been successfully subscribed');

});
/*
*********************************
**
**	MTN SDP RENEW API
**
*********************************
*/
$app->get('/sdp/renew/',  function () use ($app) {
	global $db;
	$response 	=& Terragon::load_libraries('ApiResponse');
	$tool 		=& Terragon::load_libraries('db');
	$content 	=& Terragon::load_libraries('Campaign');
	$log 		=& Terragon::load_libraries('LogRequest');

	$product_id  = get_param('content_id');
	$msisdn     = get_param('msisdn');
	$partner    = get_param('partner');

	
	$time 		= get_time_key();

	$error = array();
	if($product_id == '') 
		$error[] = "Content_id missing";
	if($msisdn == '')
		$error[] = "MSISDN is Missing";

	if(!empty($error)){
		$log->log('renewupdate',implode(' and ',$error),array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));	
	}else{
		$log->log('renewupdate','Renewal update ',array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
	}

	
	$content->get(array('product_id'=>$product_id));

	if($content->exist() == false){
		$response->errorResponse('Invalid Content ID',2);
	}

	$ms_service =& Terragon::getMS($msisdn,$product_id);

	if ($ms_service->getStatus() == 'inactive')
	{
      	$ms_service->PushService('active',$partner);
		$ms_service->subscribe($content->duration);
		$content->cpupdate($msisdn,'renew');
	}else{
		//$count = $tool->count("#__".Options::$_cpupdate,'*'," where type ='renew' and timestamp =  ".$db->tosql($time,'Text')." and product_id = ".$db->tosql($productid,'Text')." and msisdn = ".$db->tosql($msisdn,'Text'));
		$ltime = $ms_service->getLasttimeupdated();
		if($ltime != $time){
	      	$ms_service->PushService('active',$partner);
			$ms_service->subscribe($content->duration);
			$content->cpupdate($msisdn,'renew',true);
		}	
	}
	$response->successResponse('Renewal status has been successfully updated.');
});
/*
*********************************
**
**	MTN SDP UNSUBSCRIBE URL
**
*********************************
*/
$app->get('/sdp/unsubscribe/',  function () use ($app) {
	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$tool 			=& Terragon::load_libraries('db');
	$log 			=& Terragon::load_libraries('LogRequest');

	$msisdn 		= get_param('msisdn');
	$product_id 	= get_param('content_id');
	$partner 		= get_param('partner');
	
	
	$error = array();
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($product_id == '')
		$error[] = "No Product ID stated";

	

	if(!empty($error)){
		$log->log('unsubupdate',implode(' and ',$error),array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
		$response->errorResponse(implode(' and ',$error));	
	}else{
		$log->log('unsubupdate','UnSubscription update ',array('apicall'=>'sdp_subscribe','content_id'=>$product_id,'msisdn'=>$msisdn,'partner'=>$partner));
	}
	
	$content->get(array('product_id'=>$product_id));
	if($content->exist() == false){
		$response->errorResponse('The service does not exist.');
	}
	$ms_service =& Terragon::getMS($msisdn,$product_id);
	if ($ms_service->exist() == false){
		$content->cpupdate($msisdn,'unsub');
		$response->errorResponse('Cannot identify this user against this service');
	}
	if ($ms_service->getStatus() == 'active'){
	    $ms_service->unsubscribe();  
	}
	$content->cpupdate($msisdn,'unsub',true);
	$response->successResponse('User unsudscribed');

});
/*
*********************************
**
**	MTN SDP CP CRON UPDATE
**
*********************************
*/
$app->get('/api/cpupdate/',  function () use ($app) {
	$tool 			=& Terragon::load_libraries('db');
	$content 		=& Terragon::load_libraries('Campaign');
	$log 			=& Terragon::load_libraries('LogRequest');
	$DATA = $tool->load("#__".Options::$_cpupdate," where processed = 0 limit 5000 ");
	//echo "<pre>";print_r($DATA);echo "</pre>";

	foreach ($DATA as $cpupdate) {

		$type 		= $cpupdate['type'];

		$msisdn 	= $cpupdate['msisdn'];

		$product_id = $cpupdate['product_id'];

		$time 		= get_time_key();

		$data = array(
	        'product_id'	=> $product_id,
	        'msisdn'		=> $msisdn, 
	        'type'			=> $type,
	        'timestamp'		=> $time
	    );

	    $rmq =& Terragon::load_libraries('RabbitMQWorker');
	
		if($rmq->connected == true){
			$rmq->PushToQueue('MTN_CP_UPDATE',$data);

			$DATA = $tool->delete("#__".Options::$_cpupdate," where id = ".$tool->db->tosql($cpupdate['id'],'Text'));
		}
	}
});
/*
*********************************
**
**	MTN SDP CP RABBITMQ UPDATE
**
*********************************
*/
$app->get('/sdp/consumer/',  function () use ($app) {


	$rmq =& Terragon::load_libraries('RabbitMQWorker');

	$strings	= array('MTN_CP_UPDATE','MTN_CP_UPDATE_NEW');
	$key		= array_rand($strings);
	$queue_name =  $strings[$key];

	//echo $queue_name = "MTN_CP_UPDATE";

	//die;

	if($rmq->connected == true){

		list($queue_name,$totalqueued,$consumerCount) = $rmq->channel->queue_declare($queue_name,false,true,false,false);

		if($consumerCount >= 50)
			exit;

		$callback = function($msg){
		
			
			$content 		=& Terragon::load_libraries('Campaign');
			$log 			=& Terragon::load_libraries('LogRequest');

			$datasent = $msg->body;
			
			$cpupdate = json_decode($datasent,true);

			$type 		= $cpupdate['type'];

			$msisdn 	= $cpupdate['msisdn'];

			$product_id = $cpupdate['product_id'];

			$timestamp = $cpupdate['timestamp'];

			$add_to_attempt_log = array_key_exists('add_to_attempt_log',$cpupdate) ? $cpupdate['add_to_attempt_log'] : '';

			echo $url= "http://go.twinpinenetwork.com:81/sdp/sendCpUpdate/?type={$type}&msisdn={$msisdn}&product_id={$product_id}&time={$timestamp}&add_to_attempt_log={$add_to_attempt_log}"; 
			echo "<br>";
			curl($url);

	
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});

$app->get('/sdp/sendCpUpdate/',  function () use ($app) {


	$response 		=& Terragon::load_libraries('ApiResponse');
	$content 		=& Terragon::load_libraries('Campaign');
	$log 			=& Terragon::load_libraries('LogRequest');


	$type 		= get_param('type');
	$msisdn 	= get_param('msisdn');
	$product_id = get_param('product_id');
	$timestamp 	= get_param('time');
	$add_to_attempt_log = get_param('add_to_attempt_log');

	$time 		= empty($timestamp) ? get_time_key() :  $timestamp;

	$error = array();
	if(empty($type))
		$error[] = "MSISDN update type";
	
	if(empty($product_id))
		$error[] = "No Product ID stated";

	if(empty($msisdn))
		$error[] = "No msisdn";


	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));	
	}

	$content->get(array('product_id'=>$product_id));

	$msisdnobj  =& Terragon::getUC($content->campaign_id,$msisdn);

	//$elastic  =& Terragon::elastic();

	if($msisdnobj->exist('msisdn') == false){
		$status = ($type == 'sub' || $type == 'renew') ? 'active' : 'inactive';
		$msisdnobj->add($status);
	}

	$currentstatus =  $msisdnobj->getStatus();
	
	switch ($type)
	{
		case 'renew':
		case 'sub':
			if($currentstatus != 'active') $msisdnobj->active();
		break;
		case 'unsub':
			if($currentstatus != 'inactive')$msisdnobj->inactive();
		break;
	}
	$append = '';
	$refid = "";
	if($type == 'sub'){
		$refid = $msisdnobj->getRefId();
		if(!empty($refid)){
			$append = '&refid='.$refid;
		}
	}

	$data = array(
        'product_id'	=> $product_id,
        'msisdn'		=> $msisdn, 
        'type'			=> $type,
        'timestamp'		=> $time,
        "processed"		=> 1
    );

    $log->cpupdatelog($data);

    if($add_to_attempt_log == 'yes'){
    	$data['campaign_id'] = $content->campaign_id;
    	$data['user_id'] = $msisdnobj->user_id;
    	$data['nowtime'] = time();
    	$data['revenue'] = ( $type == 'sub' || $type == 'renew' ) ? 1 : 0;
    	$log->cpeslog($data);
    }

	if (!empty($content->access_url)) {

		if($content->tmoni == 0){
			echo $url= $content->access_url."?user_id=".$msisdnobj->user_id."&update_type=".$type.$append."&startdatetime=".time()."&logtime=".$time; 
			echo "<br>";
			curl($url);
		}else{
			$url = url_generator( $content->access_url ,  [ 
				"msisdn" => $msisdn,
				"mtn_id"=> $content->campaign_id,
				//"user_id" => $msisdnobj->user_id ,
				"update_type" => $type,
				"startdatetime" => time(),
				"refid" => $refid,
			]);
			$response = Tcurl($url, 'mtn-subs-update.tmoni-service.ng' );

			//var_dump($response);
		}
	
	}

		

});


$app->get('/tmonitest/',  function () use ($app) {


	// echo $url= "http://c.froggie-mm.com/ng_tw/mtn_notif.axd/?user_id=useridtest&update_type=sub"; 
	// echo "<br>";
	// curl($url);

	$content =& Terragon::load_libraries('Campaign');

	$content->get(array('product_id'=>'23401220000012468'));

	$type = get_param('type');

	//$msisdnobj  =& Terragon::getUC($content->campaign_id,$msisdn);

	var_dump($content->access_url);

	var_dump($content->campaign_id);

	$url = url_generator( $content->access_url ,  [ 
		"msisdn" => '08133751266',
		"mtn_id"=> $content->campaign_id,
		"update_type" =>$type,
		"startdatetime" => time(),

	]);
	$response = Tcurl($url, 'mtn-subs-update.tmoni-service.ng' );

	 var_dump($response);
	
	
	
});

$app->get('/sdptest1/',  function () use ($app) {
	
	$sdp  =& Terragon::getSDP( '234012000008970');	
	
	$sdp->authouriseService('08037268261' , '30046' );
	
});

$app->get('/sdptest2/',  function () use ($app) {
	
	$sdp  =& Terragon::getSDP( '234012000008970');	

	$sdp->authouriseCharge('08037268261' , '30046' );

});

$app->get('/sdptest3/',  function () use ($app) {
	
	$sdp  =& Terragon::getSDP( '234012000008970');	

	$sdp->chargeService('08037268261' , '30046' );

});

$app->get('/wapshop/',  function () use ($app) {
	
	$sdp  =& Terragon::getSDP( '234012000008970');	

	$sdp->startSmsNotification('08037268261' , '30046', 'http://176.34.130.167/wapshop/callback/mtnresponse' );

});

/*
*********************************
**
**	MESSAGE SEND SMS
**
*********************************
*/
$app->get('/sendsms/',  function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	
	$msisdn 	    = get_param('msisdn');
	$sms_content 	= get_param('content');
	$serviceid 		= get_param('serviceid');
	$shortcode 		= get_param('shortcode');


	$error = array();
	if($serviceid == '')
		$error[] = "SERVICE ID is Missing";
	if($sms_content == '')
		$error[] = "SMS Content is Missing";
	if($msisdn == '')
		$error[] = "MSISDN MISSING";

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}

	if(strlen($sms_content) > 160)
	{
		$response->errorResponse('Message too long',3);
	}

	// $sdp  =& Terragon::getSDP($serviceid);
	// $sms_content = rawurldecode($sms_content);
	// $sdp->sendSms($msisdn,  $sms_content, $shortcode );
	
	$send = send_sms($msisdn,$sms_content,$shortcode,$serviceid);

	if($send == false)
	{
		$response->errorResponse('Message sending failed',1);
	}

	$response->successResponse('Message Sent');
});


/*
*********************************
**
**	MESSAGE SEND SMS
**
*********************************
*/
$app->get('/sendsms_que/',  function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	
	$msisdn 	    = get_param('msisdn');
	$sms_content 	= get_param('content');
	$serviceid 		= get_param('serviceid');
	$shortcode 		= get_param('shortcode');

	$rmq =& Terragon::load_libraries('RabbitMQWorker');

	$data = array(
        'to'		=> $msisdn,
        'content'	=> $msisdn, 
        'service_id'	=> $serviceid,
        'from'		=> $shortcode
    );

	$queue_name = "MTNVASCONTENT";

	if($rmq->connected == true){
		$rmq->PushToQueue($queue_name,$data);
	}else{
		$response->errorResponse('Message Que failed');
	}
	$response->successResponse('Message Queued');
	
});

/*
*********************************
**
**	MTN SMS QUE PROCESS
**
*********************************
*/
$app->get('/sdp/smsque/',  function () use ($app) {
	$rmq =& Terragon::load_libraries('RabbitMQWorker');

	$queue_name = "MTNVASCONTENT";

	if($rmq->connected == true){

		list($queue_name,$totalqueued,$consumerCount) = $rmq->channel->queue_declare($queue_name,false,true,false,false);

		if($consumerCount >= 50)
			exit;

		$callback = function($msg){

			$rmq =& Terragon::load_libraries('RabbitMQWorker');
			$log 			=& Terragon::load_libraries('LogRequest');
		
			$datasent = $msg->body;
			
			$DATA = json_decode($datasent,true);


			foreach ($DATA as $data) {
				$msisdn 		= $data['to'];
				$sms_content 	= $data['content'];
				$serviceid 		= $data['service_id'];
				$shortcode 		= $data['from'];

				$source 		= $data['source'];


				$log->smslog($data);

				$send = send_sms($msisdn,$sms_content,$shortcode,$serviceid);
				//var_dump($send);
				//die;
				if($send == false){
					if($source == 'mtn_tl_vas'){
						$rmq->PushToQueue('MTN_TL_FAILED',$data);
					}
				}	
			}
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});

use Elasticsearch\ClientBuilder;

$app->get('/pushsync/',  function () use ($app) {
	global $db;
	$dbtool =& Terragon::load_libraries('db');
	$rmq =& Terragon::load_libraries('RabbitMQWorker');
	//$total = $dbtool->count("#__userdata_new2",'*'," where cds_campaign_id =".$db->tosql("cc-c-21012014262101201443",'Text'));

	//$hosts = ['52.51.46.189'.':'.'9200'];
	$hosts = ['elastic-lb-752364616.eu-west-1.elb.amazonaws.com'.':'.'8500'];
	$elastic = ClientBuilder::create() ->setHosts($hosts)->build();

	$params = array(
		'index' => 'mtnsdp',
		'type' 	=> 'userdata'
	);	
	$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" =>  "cc-c-21012014262101201443" , "fields" => ["cds_campaign_id"], 'operator' => 'and' , "type"=> "cross_fields"];
	$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" =>  "active" , "fields" => ["subscription_status"], 'operator' => 'and' , "type"=> "cross_fields"];
	
	//echo "<pre>";print_r($params['body']);echo "</pre>";
	$DATA = $elastic->count($params);
	 // echo "<pre>";print_r($DATA);echo "</pre>";
	 // die;
	$total = $DATA['count'];

	if(!empty($total)){
		$batch = 1000;
		$stage = ceil(($total/$batch));
		for ($i=0; $i < $stage; $i++) { 
			$start = $i * $batch;
			$limit = $batch;
			$data = array('start'=>$start,'limit'=>$limit,'cds_campaign_id'=>"cc-c-21012014262101201443");
			$queue_name =  "SYNCDATA1";
			if($rmq->connected == true){
				$rmq->PushToQueue($queue_name,$data);
			}
		}
	}
});



$app->get('/processsync/',  function () use ($app) {
	$rmq =& Terragon::load_libraries('RabbitMQWorker');
	$queue_name = "SYNCDATA1";
	if($rmq->connected == true){
		list($queue_name,$totalqueued,$consumerCount) = $rmq->channel->queue_declare($queue_name,false,true,false,false);
		if($consumerCount >= 50)
			exit;
		$callback = function($msg){	
			$datasent = $msg->body;
			$data = json_decode($datasent,true);

			$db 	=& Terragon::dbconnect(); 
			$dbtool =& Terragon::load_libraries('db');
			$rmq 	=& Terragon::load_libraries('RabbitMQWorker');

			$start 		= $data['start'];
			$limit 		= $data['limit'];
			$cds_campaign_id 		= $data['cds_campaign_id'];
			
			//$DATA = $dbtool->load("#__userdata_new2", " where cds_campaign_id =".$db->tosql($cds_campaign_id,'Text'),$start,$limit);


			// $hosts = ['52.51.46.189'.':'.'9200'];
			// $elastic = ClientBuilder::create() ->setHosts($hosts)->build();

			$hosts = ['elastic-lb-752364616.eu-west-1.elb.amazonaws.com'.':'.'8500'];
			$elastic = ClientBuilder::create() ->setHosts($hosts)->build();

			$params = array(
				'index' => 'mtnsdp',
				'type' 	=> 'userdata'
			);	
			$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" => $cds_campaign_id , "fields" => ["cds_campaign_id"], 'operator' => 'and' , "type"=> "cross_fields"];
			$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" =>  "active" , "fields" => ["subscription_status"], 'operator' => 'and' , "type"=> "cross_fields"];
			$params['body']['from'] = $start;
			$params['body']['size'] = $limit;
			//echo "<pre>";print_r($params['body']);echo "</pre>";
			$DATA = $elastic->search($params);

			$DATA = $DATA['hits']['hits'];

			$QUE 		= array();
			$quebatch 	= 1;
			$queue 		= "SYNCDATA";
			$i = 0;
			foreach($DATA as $mydata) {
				$db_data = $mydata['_source'];
				$i++;
				$msisdn = $db_data['msisdn'];
				$user_id = $db_data['user_id'];

				$queuedata = array(
		            'msisdn'  		=> $msisdn,
		            'camp_id'   	=> $cds_campaign_id,
		            'serviceid'   	=> '234012000004149',
		            'shortcode'   	=> '5033',
		        );
				$QUE[] = $queuedata;
				if( ( $i % $quebatch) == 0) {
					if(count($QUE) > 0){
						echo "<pre>";print_r($QUE);echo "</pre>";
				   		$rmq->PushToQueue($queue,$QUE);	
						$QUE = array();
					}
		        }	
			}
			if( ( $i % $quebatch) != 0){
				if(count($QUE) > 0){
					echo "<pre>";print_r($QUE);echo "</pre>";
			   		$rmq->PushToQueue($queue, $QUE );
					$QUE = array();
				}
		    }
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});

$app->get('/change_status/',  function () use ($app) {

	$status 		= get_param('status');
	$msisdn 		= get_param('msisdn');
	$camp_id 		= get_param('cid');
			
	$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

	if($msisdnobj->exist('msisdn') == false){
		$msisdnobj->add();
	}

	if($status == 1){
		$msisdnobj->active();
	}else{
		$msisdnobj->inactive();
	}	
});

$app->get('/dosync/',  function () use ($app) {
	
	$rmq =& Terragon::load_libraries('RabbitMQWorker');
	$queue_name = "SYNCDATA";
	if($rmq->connected == true){
		list($queue_name,$totalqueued,$consumerCount) = $rmq->channel->queue_declare($queue_name,false,true,false,false);
		if($consumerCount >= 200)
			exit;
		$callback = function($msg){	
			$datasent = $msg->body;
			$DATA = json_decode($datasent,true);
			
			$data = $DATA[0];

		
			
				$msisdn 		= $data['msisdn'];
				$camp_id 		= $data['camp_id'];
				$serviceid 		= $data['serviceid'];
				$shortcode 		= $data['shortcode'];

				$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

				if($msisdnobj->exist('msisdn') == false){
					$msisdnobj->add();
				}
				//$user_id = $msisdnobj->user_id;

				$send = send_sms($msisdn,'',$shortcode,$serviceid);

				if($send == true){
					$msisdnobj->active();
				}else{
					$msisdnobj->inactive();
				}	
			
		  
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});

$app->get('/dosync1/',  function () use ($app) {
	$rmq =& Terragon::load_libraries('RabbitMQWorker');
	$queue_name = "SYNCDATA4";
	if($rmq->connected == true){
		list($queue_name,$totalqueued,$consumerCount) = $rmq->channel->queue_declare($queue_name,false,true,false,false);
		if($consumerCount >= 200)
			exit;
		$callback = function($msg){	
			$datasent = $msg->body;
			$DATA = json_decode($datasent,true);

			foreach ($DATA as $data) {
				$msisdn 		= $data['msisdn'];
				$camp_id 		= $data['camp_id'];
				$serviceid 		= $data['serviceid'];
				$shortcode 		= $data['shortcode'];
				$msisdnobj =& Terragon::getUC($camp_id,$msisdn);

				if($msisdnobj->exist('msisdn') == false){
					$msisdnobj->add();
				}
				//$user_id = $msisdnobj->user_id;

				$send = send_sms($msisdn,'',$shortcode,$serviceid);

				if($send == true){
					$msisdnobj->active();
				}else{
					$msisdnobj->inactive();
				}	
			}
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});


/*
*********************************
**
**	MTN TL SUBSCRIBE URL
**
*********************************
*/

$app->get('/sdp/subscribe_tl/',  function () use ($app) {
	$response 		=& Terragon::load_libraries('ApiResponse');
	$campaignTl 	=& Terragon::load_libraries('CampaignTl');
	$tool 			=& Terragon::load_libraries('db');

	$msisdn 		= get_param('msisdn');
	$product_id 	= get_param('product_id');
	$effectiveTime 	= get_param('effectiveTime');
	$expiryTime 	= get_param('expiryTime');
	$updateDesc 	= get_param('updateDesc');
	$updateType 	= get_param('updateType');
	$rentStatus 	= get_param('rentStatus');
	
	$error = array();
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($product_id == '')
		$error[] = "No Product ID stated";


	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}
	$service = $campaignTl->getServcie(array('product_id'=>$product_id));
	if($service == false){
		$response->errorResponse('The service does not exist.');
	}
	$ms_service =& Terragon::getMU($msisdn,$product_id);
	$msisdn = msisdn_sanitizer($msisdn,false);
	$param = array(
        'msisdn'      	=> $msisdn, 
        'effectiveTime' => $effectiveTime,
        'expiryTime'   	=> $expiryTime,
        'updateDesc'   	=> $updateDesc,
        'updateType'   	=> $updateType,
        'product_id'   	=> $product_id,
        'rentStatus'   	=> $rentStatus
    );
  	$ms_service->subscribe($param);
	$campaignTl->pushupdate($param);
	$response->successResponse('User has been successfully subscribed');
});

/*
*********************************
**
**	MTN TL UNSUBSCRIBE URL
**
*********************************
*/

$app->get('/sdp/unsubscribe_tl/',  function () use ($app) {
	$response 		=& Terragon::load_libraries('ApiResponse');
	$campaignTl 	=& Terragon::load_libraries('CampaignTl');
	$tool 			=& Terragon::load_libraries('db');

	$msisdn 		= get_param('msisdn');
	$product_id 	= get_param('product_id');
	$effectiveTime 	= get_param('effectiveTime');
	$expiryTime 	= get_param('expiryTime');
	$updateDesc 	= get_param('updateDesc');
	$updateType 	= get_param('updateType');
	$rentStatus 	= get_param('rentStatus');

	$error = array();
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($product_id == '')
		$error[] = "No Product ID stated";

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}
	$service = $campaignTl->getServcie(array('product_id'=>$product_id));
	if($service == false){
		$response->errorResponse('The service does not exist.');
	}
	$ms_service =& Terragon::getMU($msisdn,$product_id);
	$msisdn = msisdn_sanitizer($msisdn,false);
	$param = array(
        'msisdn'      	=> $msisdn, 
        'effectiveTime' => $effectiveTime,
        'expiryTime'   	=> $expiryTime,
        'updateDesc'   	=> $updateDesc,
        'updateType'   	=> $updateType,
        'product_id'   => $product_id,
        'rentStatus'   	=> $rentStatus
    );
    if ($ms_service->exist() == false){	
	//	$response->errorResponse('Cannot identify this user against this service');
	}
	$campaignTl->pushupdate($param);
	//if ($ms_service->getStatus() == 'active'){
	   $ms_service->unsubscribe();
	//}
	$response->successResponse('User unsudscribed');
});
/*
*********************************
**
**	MTN TL RENEWAL URL
**
*********************************
*/
$app->get('/sdp/renew_tl/',  function () use ($app) {
	$response 		=& Terragon::load_libraries('ApiResponse');
	$campaignTl 	=& Terragon::load_libraries('CampaignTl');
	$tool 			=& Terragon::load_libraries('db');

	$msisdn 		= get_param('msisdn');
	$product_id 	= get_param('product_id');
	$effectiveTime 	= get_param('effectiveTime');
	$expiryTime 	= get_param('expiryTime');
	$updateDesc 	= get_param('updateDesc');
	$updateType 	= get_param('updateType');
	$rentStatus 	= get_param('rentStatus');
	
	
	$error = array();
	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($product_id == '')
		$error[] = "No Product ID stated";

	if(!empty($error)){
		$response->errorResponse(implode(' and ',$error));
	}
	$campaignTl->getServcie(array('product_id'=>$product_id));
	if($campaignTl->exist() == false){
		$response->errorResponse('The service does not exist.');
	}
	$ms_service =& Terragon::getMU($msisdn,$product_id);
	$msisdn = msisdn_sanitizer($msisdn,false);
	$param = array(
        'msisdn'      	=> $msisdn, 
        'effectiveTime' => $effectiveTime,
        'expiryTime'   	=> $expiryTime,
        'updateDesc'   	=> $updateDesc,
        'updateType'   	=> $updateType,
        'product_id'   	=> $product_id,
        'rentStatus'   	=> $rentStatus
    );

	if ($ms_service->getStatus() == 'inactive'){
		$ms_service->subscribe($param);
		$campaignTl->pushupdate($param);
	}else{
		//$count = $tool->count("#__".Options::$_cpupdate,'*'," where type ='renew' and timestamp =  ".$db->tosql($time,'Text')." and product_id = ".$db->tosql($productid,'Text')." and msisdn = ".$db->tosql($msisdn,'Text'));
		$time 		= get_time_key();
		$ltime = $ms_service->getLasttimeupdated();
		if($ltime != $time){
	      	$ms_service->subscribe($param);
			$campaignTl->pushupdate($param);
		}	
	}
	$response->successResponse('Renewal status has been successfully updated.');
});


/*
*********************************
**
**	MTN SDP TL RABBITMQ UPDATE
**
*********************************
*/
$app->get('/sdp/tlconsumer/',  function () use ($app) {
	
	$rmq =& Terragon::load_libraries('RabbitMQWorker');

	$strings	= array('MTN_TL_UPDATE','MTN_TL_UPDATE_NEW');
	$key		= array_rand($strings);
	$queue_name =  $strings[$key];

	//$queue_name = 'MTN_TL_UPDATE';

	if($rmq->connected == true){

		list($queue_name,$totalqueued,$consumerCount)  = $rmq->channel->queue_declare($queue_name,false,true,false,false);

		if($consumerCount >= 50)
			exit;

		$callback = function($msg){

			
		
			$campaignTl 	=& Terragon::load_libraries('CampaignTl');
			$log 			=& Terragon::load_libraries('LogRequest');

			$datasent = $msg->body;
			
			$cpupdate = json_decode($datasent,true);	



			$effectiveTime 	= $cpupdate['effectiveTime'];
			$expiryTime 	= $cpupdate['expiryTime'];
			$updateDesc 	= $cpupdate['updateDesc'];
			echo $updateType 	= $cpupdate['updateType'];
			$timestamp 		= $cpupdate['timestamp'];
			$msisdn 		= $cpupdate['msisdn'];
			$rentStatus 	= @$cpupdate['rentStatus'];
			
			$product_id 	= $cpupdate['product_id'];

			$msisdnobj  =& Terragon::getMP($product_id,$msisdn);

			$today_content = $campaignTl->getContent($msisdnobj->service_code);

			$PARAM = array(
				'msisdn' 						=> $msisdn,
				'Apply_Time' 					=> $effectiveTime,
				'Expire_Time' 					=> $expiryTime,
				'Rent_status' 					=> $rentStatus,
			);

			
			switch ($updateType)
			{
				case 1:
					$PARAM['delete_status'] = 0;
					if($msisdnobj->exist() == false){
						$msisdnobj->add($PARAM);
						$campaignTl->addBillInfo($expiryTime,"Subscription",$msisdnobj->msisdn,$msisdnobj->service_code);
					}else{
						$msisdnobj->renew($PARAM);
						$campaignTl->addBillInfo($expiryTime,"Renewer",$msisdnobj->msisdn,$msisdnobj->service_code);
					}
					$today_content = $campaignTl->getContent($msisdnobj->service_code);
					if($today_content != false){
						send_sms($msisdnobj->msisdn,$today_content,'30046',$msisdnobj->service_id);
					}
				break;
				case 2:
					$PARAM['delete_status'] = 1;
					$msisdnobj->unsub($PARAM);
				break;
				case 3: 
	                $PARAM['delete_status'] = 0;
					if($msisdnobj->exist() == false){
						$msisdnobj->add($PARAM);
					}else{
						$msisdnobj->renew($PARAM);
					}
					$campaignTl->addBillInfo($expiryTime,"Renewer",$msisdnobj->msisdn,$msisdnobj->service_code);
					$today_content = $campaignTl->getContent($msisdnobj->service_code);
					if($today_content != false){
						$send = send_sms($msisdnobj->msisdn,$today_content,'30046',$msisdnobj->service_id);
					}
				break;
			}
			//die;
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		};
		$rmq->channel->basic_qos(null, 1, null);
		$rmq->channel->basic_consume($queue_name, '', false, false, false, false, $callback);
		if($totalqueued < 1)
	 			exit;
		while(count($rmq->channel->callbacks)){
			 $rmq->channel->wait();
		}
	}
});



$app->get('/ext/subscribe/', function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	$log 			=& Terragon::load_libraries('LogRequest');
	
	$msisdn 		= get_param('msisdn');
	$serviceid 		= get_param('serviceid');
	$productid 		= get_param('productid');
	$shortcode 		= get_param('shortcode');

	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	
	if($serviceid == '')
		$error[] = "Service Id is Missing";

	if($productid == '')
		$error[] = "Product Id is Missing";

	if($shortcode == '')
		$error[] = "Shortcode is Missing";


	$prefix = substr($msisdn,0,8);
	if($prefix  == "23480300")
	{
	    echo "I am an MTN Staff";
	    return;
	}


	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
		$response->errorResponse(implode(' and ',$error));
	}

	$sdp  =& Terragon::getSDP($serviceid);
	$mtnresponse = $sdp->subscribe($msisdn, $productid , $shortcode);

	switch ($mtnresponse) {
		case '00000000':
			$log->log('success','User Subscribed to service',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->successResponse('User has been successfully billed');
		break;
		case '22007233':
			$log->log('success','Request Subscription sent to user',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->successResponse('Request Subscription sent to user',4);
		break;
		case '22007696':
			$log->log('error','Request Previously Sent to user',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('Request Previously Sent to user',5);
		break;
		case '22007330' :
			$log->log('error','Billing Failed. This user has insufficient credit',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('Billing Failed. This user has insufficient credit',1); 
		break;
		case '22007238':
		case '22007201':
			$log->log('error','Billing Failed. This is because the user is already subscribed to the service',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('Billing Failed. This is because the user is already subscribed to the service',4);
		break;
		case '22007203':
			$log->log('error','This product does not exist',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('This product does not exist',4);
		break;
		default:
			$log->log('error','Billing Failed. This might be because of an issue with the Network Operator',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('Billing Failed. This might be because of an issue with the Network Operator',3);
		break;
	}
});

$app->get('/ext/unsubscribe/', function () use ($app) {

	$response 		=& Terragon::load_libraries('ApiResponse');
	$log 			=& Terragon::load_libraries('LogRequest');
	
	$msisdn 		= get_param('msisdn');
	$serviceid 		= get_param('serviceid');
	$productid 		= get_param('productid');
	$shortcode 		= get_param('shortcode');

	if($msisdn == '')
		$error[] = "MSISDN is Missing";
	if($serviceid == '')
		$error[] = "Service Id is Missing";

	if($productid == '')
		$error[] = "Product Id is Missing";

	if($shortcode == '')
		$error[] = "Shortcode is Missing";

	if(!empty($error)){
		$log->log('error',implode(' and ',$error) ,array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
		$response->errorResponse(implode(' and ',$error));
	}

	$sdp  =& Terragon::getSDP($serviceid);
	$mtnresponse = $sdp->unsubscribe($msisdn, $productid , $shortcode);

	switch ($mtnresponse) {
		case '00000000':
			$log->log('success','User has been successfully unsubscribed',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->successResponse('User has been successfully unsubscribed');
		break;
		case '22007219' :
			$log->log('error','User not subscribed to service',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('User not subscribed to service',4); 
		break;
		default:
			$log->log('error','Unsubscription failed',array('apicall'=>'extsubscribe','msisdn'=>$msisdn,'serviceid'=>$serviceid,'productid'=>$productid,'shortcode'=>$shortcode));
			$response->errorResponse('Unsubscription failed',3);
		break;
	}
});

$app->get('/test/',  function () use ($app) {
	
	// //$send = send_sms('08037268261','test','5033','234012000008089',true);	
	// $content =& Terragon::load_libraries('Campaign');

	// $msisdn = '08032005744';
	// $content->get(array('product_id'=>'23401220000011978'));

	// $msisdnobj  =& Terragon::getUC($content->campaign_id,$msisdn);


	// if($msisdnobj->exist('msisdn') == false){
	// 	echo "hi";
	// 	// $status = ($type == 'sub' || $type == 'renew') ? 'active' : 'inactive';
	// 	// $msisdnobj->add($status);
	// }
	// var_dump($msisdnobj->user_id);

	$elastic  =& Terragon::elastic();

	// how successResponse('User has been successfully billed');

	// $time 		= get_time_key();

	// $type = get_param('type');

	// $type = empty($type) ? 'sub' : $type;

	// echo $url= "http://ng.takeabreaktv.com/feedback/apimtn.php?user_id=cc-u-577100a95d6d4&update_type=".$type."&startdatetime=".time()."&logtime=".$time; 
	// echo "<br>";
	// curl($url);
});


$app->run();
