<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class LogRequest{
	function __construct(){
		
	}

	function log ($type, $message, $param = array()){

		$URL = Terragon::get_full_url();
		$defaultParam = array(
			'time' 	=> date('Y-m-d H:i:s'),
			'type' 	=> $type,
			'param' => $param,
			'url' 	=> $URL
		);
		$defaultParam = json_encode($defaultParam)."\n";
		//$filename = '../apilogs/log-'.date('j-n-Y').'.log';

		$filename = '../apilogs/log-'.date('n-Y').'.log';
		$file = fopen($filename, 'a+');

		@chmod($filename, 0777);

		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
		fclose($file);
	}

	function smslog ($defaultParam){
		$defaultParam['time'] = date('Y-m-d H:i:s');
		$defaultParam['timestamp'] = time();
		$defaultParam = json_encode($defaultParam)."\n";
		//$filename = '../apilogs/log-'.date('j-n-Y').'.log';
		$filename = '../apilogs/smslog-'.date('n-Y').'.log';
		$file = fopen($filename, 'a+');
		@chmod($filename, 0777);
		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
		fclose($file);
	}

	


	function cpupdatelog ($data){

		$tool 		=& Terragon::load_libraries('db');

		$tool->add("#__".Options::$_cplogs,$data);

		return;
		/*
		$defaultParam = json_encode($data)."\n";
		$time 		= get_time_key();
		//$filepath = 'logs/log-'.date('j-n-Y',$time).'.log';
		$filepath = '../sdplogs/log-cpupdate.log';
		$file = fopen($filepath, 'a+');
		if(!$file){
			return;
		}
		if( fwrite($file, $defaultParam) == false){
			return;
		}
		fclose($file);
		*/
	}
	function cpeslog ($data){
		$elastic  =& Terragon::elastic();
		$s_config =& get_config();		
		$Update =  array(
			'index' => $s_config['index'],
			'type' 	=> 'updates'
		);
		try {	
			$Update['body'] 	=  $data;
			return $response = $elastic->index($Update);
		}catch (Exception $e) {
			return false;
		}
		
	}
}
?>
