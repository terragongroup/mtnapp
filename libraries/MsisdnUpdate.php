<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class MsisdnUpdate{
	
	public $client;

	private $push 			= '';
	private $DATA			= array();

	private $msisdn			= "";
	private $productid		= "";


	var $redisactive = false;

	function __construct($msisdn = "",$productid = ""){
		$this->redis_connection();
		$this->init($msisdn,$productid);
	}

	protected function redis_connection(){
		global $redis;
       	$this->client = $redis->client;      
       	if(!is_object($this->client)){
       		echo "Couldn't connected to on msisdn layer<br>";
       	}
       $this->redisactive = true;
	}
	private function init($msisdn,$productid){
		$this->msisdn 		= msisdn_sanitizer($msisdn);
		$this->productid 	= $productid;
		$this->push = "serviceupdate30036:".$this->msisdn.':'.$this->productid;
	}

	function PushService( $param = array() ){
		$time 		= get_time_key();
		if($this->exist() == true){
			$data = $this->DATA;
			foreach ($param as $key => $value) {
				$data[$key] = $value;
			}
			$data['time'] 		= $time;
		}else{
			$data = $param;
			$data['time'] 		= $time;
		}
		try {
      		$this->client->hmset($this->push,$data);
			$this->DATA = $this->client->hgetall($this->push); 
	    }catch (Exception $e) {
	    	$this->DATA = $data;
	    }
	}

	function subscribe($param){
		$param['status'] = 'active';
		$this->PushService($param);
	}

	function unsubscribe(){
		if($this->exist() == true){
			$this->setStatus('inactive');
		}
	}

	function setStatus($status){
		if($this->exist() == true){
			try {
				$time 		= get_time_key();
		      	$data = $this->DATA;
				$data['status'] = $status;
				$data['time'] = $time;
				$this->client->hmset($this->push,$data);
				$this->DATA = $this->client->hgetall($this->push); 
		    }catch (Exception $e) {
		    	$this->DATA['status'] = $status;
		    }
		}
	}

	function getStatus(){
		if($this->exist() == true){
			if(array_key_exists('status', $this->DATA)){
				return $this->DATA['status'];
			}
		}
		return "";
	}

	function getLasttimeupdated(){
		if($this->exist() == true){
			if(array_key_exists('time', $this->DATA)){
				return $this->DATA['time'];
			}
			return "";
		}
		return "";
	}

	function getServices(){
		try {
	      	if(count($this->DATA) > 0){
				return $this->DATA;
			}
			$this->DATA = $this->client->hgetall($this->push);
			return $this->DATA;
	    }catch (Exception $e) {
	     	return $this->DATA;
	    }
	}

	function exist(){
		$this->getServices();
		return (count($this->DATA) > 0) ? true : false;
	}

}
