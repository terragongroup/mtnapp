<?php
defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
require_once __DIR__ . '/RabbitMQ/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
class RabbitMQ {
	public $channel;
	public $connection;
	var $soft 		= false;
	var $connected 	= false; 
	var $host;	
	var $port;		
	var $username; 			
	var $passkey;
	function __construct(){
	}
	public function connect(){
		try{
			$this->connection = new AMQPConnection(
				$this->host,
				$this->port,
				$this->username,
				$this->passkey
			);
			$this->channel = $this->connection->channel();
			$this->connected = true;
		}catch(Exception $ex){
			if($this->soft == false){
				show_error("Unable to connect to RabbitMQ: {$e->getMessage()}", 500);
			}
		}
	}
}
?>
