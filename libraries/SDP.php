<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class SDP{
	private $serviceid	= "";

    private $_ns          = 'http://www.huawei.com.cn/schema/common/v2_1';
  
    private $_ws          = 'wsdl/osg_subscribe_service_1_0.wsdl';
    private $_vLocation   = "http://41.206.4.162:8310/SubscribeManageService/services/SubscribeManage";
    private $_uri         = "http://41.206.4.162:8310/SubscribeManageService/services/SubscribeManage";
    
    private $_smsws       = 'wsdl/SMSParlayX/SMS/parlayx_sms_send_service_2_2.wsdl';
    private $_smsLoc      = "http://41.206.4.162:8310/SendSmsService/services/SendSms";
    private $_smsuri      = "http://41.206.4.162:8310/SendSmsService/services/SendSms";
    
    private $_aws         = 'wsdl/authorization_service_1_0.wsdl';
    private $_avLocation  = "http://41.206.4.162:8310/authorizationService/services/authorization";

    private $_cws         = 'wsdl/common/parlayx_payment_amount_charging_service_3_1.wsdl';
    private $_cvLocation  = "http://41.206.4.162:8310/AmountChargingService/services/AmountCharging";
	
	private $_startSMSNotWS          = 'wsdl/SMSParlayX/SMS/parlayx_sms_notification_manager_service_2_3.wsdl';
    private $_startSMSNotvLocation   = "http://41.206.4.162:8310/SmsNotificationManagerService/services/SmsNotificationManager";
    private $_startSMSNoturi         = "http://41.206.4.162:8310/SmsNotificationManagerService/services/SmsNotificationManager";
	
	

    private $_shortcode = array(
        "5033"  => array("spId"=>'2340110000930', "spPwd"=>'bmeB500' ),
        "30046" => array("spId"=>'2340110000417', "spPwd"=>'ABiTera7'),
        "53447" => array("spId"=>'2340110000417', "spPwd"=>'ABiTera7')
    );

	function __construct($serviceid){
		$this->init($serviceid);
	}
	
	private function init($serviceid){
		$this->serviceid 	= $serviceid;
	}
	
	public function subscribe($msisdn, $productID , $shortcode) {
        $msisdn     = $this->msisdn_sanitizer( $msisdn,false );
        $ws         = $this->_ws;
        $vLocation  = $this->_vLocation;
        $uri        = $this->_uri;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $timeStamp  = date('YmdHis');
        $spPassword = md5($spId . $spPwd . $timeStamp);
        $UserID     = $msisdn;
       
        $client = new SoapClient($ws, array(
            "location"  => $vLocation,
            "uri"       => $uri,
            "trace"     => 1,
            "exceptions" => 1
        ));
        //Body of the Soap Header. 
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp,
            'OA'            => $UserID,
            'FA'            => $UserID
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
      
        $client->__setSoapHeaders($header);

        $response = "";
        try {
            $response = $client->subscribeProduct(array(
                'subscribeProductReq'   => array(
                    'userID'                => array('ID' =>$msisdn,'type' => '0'),
                    'subInfo'               => array('productID' => $productID, 'channelID' => '7','operCode'=>'ng','isAutoExtend'=>0),
                    'extensionInfo'         => array('namedParameters' => array('key' => 'SubType', 'value' => '0') )
                )
            ));
            return $response->subscribeProductRsp->result;
        } catch (Exception $e) {
            return false;
        }      
    }

    public function unsubscribe($msisdn, $productID , $shortcode ) {
        $msisdn     = $this->msisdn_sanitizer( $msisdn,false );
        $ws         = $this->_ws;
        $vLocation  = $this->_vLocation;
        $uri        = $this->_uri;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $timeStamp  = date('YmdHis');
        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $spPassword = md5($spId . $spPwd . $timeStamp);
        $UserID     = $msisdn;

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));
        //Body of the Soap Header. 
        $headerbody = array('spId' => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);
       
        $response = "";
        try {
            $response = $client->unSubscribeProduct(array(
                'unSubscribeProductReq' => array(
                    'userID'        => array('ID' => $msisdn, 'type' => '0'),
                    'subInfo'       => array('productID' => $productID, 'channelID' => '1', 'operCode' => 'zh', 'isAutoExtend' => '0'),
                    'extensionInfo' => array('namedParameters' => array('key' => 'SubType', 'value' => '0') )
                )
            ));
			$des = "Description not available";
            if (isset($response->unSubscribeProductRsp->resultDescription))
                $des = $response->unSubscribeProductRsp->resultDescription;
            return $response->unSubscribeProductRsp->result;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendSms($msisdn, $message, $shortcode = '5033', $debug = false) {
        $msisdn = msisdn_sanitizer($msisdn,false);
        if(empty($shortcode))$shortcode = '5033';
        
    	$ws         = $this->_smsws;
        $vLocation  = $this->_smsLoc;
        $uri        = $this->_smsuri;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        //$serviceId = 234012000008174;

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));

        $timeStamp  = date('YmdHis');
        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $spPassword = md5($spId . $spPwd . $timeStamp); 
        $UserID     = 'tel:'.$msisdn;
       
        $headerbody = array(
        	'spId' 			=> $spId,
            'spPassword'    => $spPassword,
            'serviceId'     => $this->serviceid,
            'timeStamp'     => $timeStamp,
            'OA'            => $UserID,
        );

// var_dump($headerbody);
// var_dump( array(
//                         'addresses'     => $UserID,
//                         //'senderName'  => 'TwinPine',
//                         'senderName'    => $shortcode,
//                         'message'               => $message,
//                         'charging'              => '0000',
//                  ) );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //var_dump($headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);
        $response = "";
        try {
        	 $response = $client->sendSms(array(
        	 	'addresses' 	=> $UserID,
        	 	//'senderName' 	=> 'TwinPine',
        	 	'senderName' 	=> $shortcode,
        	 	'message' 		=> $message,
        	 	'charging' 		=> '0000',
        	 ));

          return $result =  $response->result; 
       // var_dump($response);
	} catch (Exception $e) { 
        return false;
            if($debug == true){
             //   echo "<pre>";print_r($e) ; echo "</pre>";
            }
        	//return false;
	//	var_dump($e);
		//echo "<pre>";print_r($e) ; echo "</pre>";
        }
    }

	function curl($url,$requestData = ''){
		$options = array(
			"url" 			=> $url,
			"data" 			=> "plain",
			"httpheader" 	=> array('Content-type: application/json'),
			//"post" 			=> "1",
			//"post_fields" 	=> json_encode($requestData),
			"return_transfer" => "1"
		);		
		$obj =& Terragon::load_class('curlclass');
		$obj->set_options($options);
		$resp = $obj->Execute();	
		if($obj->get_httpCode() != 200){
			return "";
		}return $resp;
	}

    public function chargeService( $msisdn,  $shortcode) {
    
        $msisdn = $this->msisdn_sanitizer( $msisdn,false );

        $ws         = $this->_cws;
        $vLocation  = $this->_cvLocation;
        $uri        = $this->_cvLocation;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));

        $timeStamp  = date('YmdHis');
        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $spPassword = md5($spId . $spPwd . $timeStamp);
         
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'serviceId'     => $this->serviceid,
            'timeStamp'     => $timeStamp,
            'oauth_token'   => '15103739075500223326',
        );

        //$headerbody = array();
        echo "<pre>";print_r($headerbody);echo "</pre>";
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);

        $response = "";
        try {

            $param = array(
                
                'endUserIdentifier' => $msisdn,
                'charge'  => array(
                    'description'   => 'charging information',
                    'currency'      => 'NGN',
                    'amount'        => 0.2,
                    'code'          =>'10778'
                ),
                'referenceCode' => $this->getTransactionId()
             );

            //  $param = array(
            //     'endUserIdentifier' => $msisdn,
            //     'charge'  => array(
            //         'description'   => 'charging information',
            //         'currency'      => 'NGN',
            //         'amount'        => 0.2,
            //         'code'          =>'10778',
            //     ),
            //     'referenceCode' => $this->getTransactionId()
            // );

            echo "<pre>";print_r($param);echo "</pre>";

            $response = $client->chargeAmount($param);
            $result =   $response->result; 
            //var_dump($response);
            echo "<pre>";print_r($response);echo "</pre>";
        } catch (Exception $e) {  
            echo "<pre>";print_r($e);echo "</pre>";
            die;
            return "";
        }
    }

    public function authouriseService( $msisdn,  $shortcode) {
    
        $msisdn = $this->msisdn_sanitizer( $msisdn,false );

        $ws         = $this->_aws;
        $vLocation  = $this->_avLocation;
        $uri        = $this->_avLocation;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));

        $timeStamp  = date('YmdHis');
        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $spPassword = md5($spId . $spPwd . $timeStamp);
        
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'serviceId'     => $this->serviceid,
            'timeStamp'     => $timeStamp,
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);

        $response = "";
        try {

            // <v1:endUserIdentifier>151103132</v1:endUserIdentifier> 
            // <v1:transactionId>1261651514515</v1:transactionId>
            // <v1:scope>99</v1:scope>
            // <v1:serviceId>35000001000001</v1:serviceId>
            // <v1:amount>121212</v1:amount>
            // <v1:currency>CYN</v1:currency> 
            // <v1:description>sub</v1:description>

            $scope = array(
                '17'=> 'Payment',
                '99'=> 'Subscription',
                '4' =>  'LBS',
                '79'=> 'On-demand',
            );
            
            //description => Service/Charging/Subscription
            $param = array(
                
                'endUserIdentifier' => $msisdn,
                'transactionId'     => $this->getTransactionId(),
                'scope'             => 79,
                'serviceId'         => $this->serviceid,
                'description'       => 'Payment service',
                'amount'            => 0.2,
                'currency'          => 'NGN',
                'frequency'          => 0,
                'notificationURL'    => 'http://go.twinpinenetwork.com:81/api/authorization',
                'tokenValidity'      => 1,

                'extensionInfo'        => array('item'              => array(
                    array('key'=>'totalAmount', 'value'=>20 ),
                    array('key'=>'productName', 'value'=>'MTNN-payment' ),
                    array('key'=>'channel', 'value'=>2 ),
                    array('key'=>'serviceInterval', 'value'=>1 ),
                    array('key'=>'serviceIntervalUnit', 'value'=>2 ),
                    array('key'=>'tokenType', 'value'=>0 )
                )),
             );

            echo "<pre>";print_r($param);echo "</pre>";

            $response = $client->authorization($param);
            $result =   $response->result; 
            //var_dump($response);
            echo "<pre>";print_r($response);echo "</pre>";
        } catch (Exception $e) {  
            echo "<pre>";print_r($e);echo "</pre>";
            die;
            return "";
        }
    }

    public function authouriseCharge( $msisdn,  $shortcode) {
    
        $msisdn = $this->msisdn_sanitizer( $msisdn,false );

        $ws         = $this->_aws;
        $vLocation  = $this->_avLocation;
        $uri        = $this->_avLocation;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));

        $timeStamp  = date('YmdHis');
        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $spPassword = md5($spId . $spPwd . $timeStamp);
        
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp,
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);
        $response = "";
        try {
            //description => Service/Charging/Subscription
            $param = array(
                'endUserIdentifier' => $msisdn,
                'operation'     => 1,
            );
            echo "<pre>";print_r($param);echo "</pre>";
            $response = $client->queryAuthorizationList($param);
            $result =   $response->result; 
            //var_dump($response);
            echo "<pre>";print_r($response);echo "</pre>";
            return $result;
        } catch (Exception $e) {  
            echo "<pre>";print_r($e);echo "</pre>";
            die;
            return false;
        }
    }
	
	function startSmsNotification ($msisdn, $shortcode , $endpoint ) {
		$msisdn = $this->msisdn_sanitizer($msisdn);
		
		$ws         = $this->_startSMSNotWS;
        $vLocation  = $this->_startSMSNotvLocation;
        $uri        = $this->_startSMSNoturi;
        $ns         = $this->_ns; //Namespace of the WS. 
        $details    = $this->_shortcode[$shortcode];

        $spId       = $details['spId'];
        $spPwd      = $details['spPwd'];
        $timeStamp  = date('YmdHis');
        $spPassword = md5($spId . $spPwd . $timeStamp);
        $UserID     = $msisdn;
       
        $client = new SoapClient($ws, array(
            "location"  => $vLocation,
            "uri"       => $uri,
            "trace"     => 1,
            "exceptions" => 1
        ));
		
		//Body of the Soap Header. 
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp
        );
		
		//Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
      
		$client->__setSoapHeaders($header);
		
		$interfaceName = 'notifySmsReception';
		$correlator    = '00001';
		
	
		$criteria = "demand";
		
		$response = "";
        try {
		
            $response = $client->startSmsNotification(array(
                'reference'   => array(
					'endpoint'       =>     $endpoint,
                    'interfaceName'  =>     $interfaceName,
                    'correlator'     =>     $correlator 
                ),
				'smsServiceActivationNumber' => $msisdn,
				'criteria' => $criteria
            ));

            var_dump($response);
            die;

			return $response->result;
        } catch (Exception $e) {

             echo "<pre>";print_r($e);echo "</pre>";
            die;
            return false;
        }      
	}

    function msisdn_sanitizer($msisdn,$plus = true){
        $msisdn = trim($msisdn);
        $msisdn = str_replace('+', '', $msisdn);
        if(preg_match('/^234/i',$msisdn) == true){
            $msisdn = '0'.substr($msisdn, 3);       
        }
        if(strlen($msisdn) == '11'){
            $msisdn = '+234'.substr($msisdn, 1); 
        }else{
            if(strpos($msisdn,'+') == false) 
                $msisdn = '+'.$msisdn;      
        }
        if($plus == false){
            $msisdn = str_replace('+', '', $msisdn);
        }
        return $msisdn;
    }

    function getTransactionId(){
        $string = "0110000";
        // sum of ip
        $node = 41 + 206 + 4 + 162;
        $string .= str_pad($node, 4, "0", STR_PAD_LEFT);
        $string .= date('ymdHis');
        $strings   = array();
        $i = 1;
        $maxview = 9999;
        for($i; $i<= ($maxview); $i++){
            $strings[] = $i;
        }
        $string .= str_pad(array_rand($strings), 4, "0", STR_PAD_LEFT);
        $i = 1;
        $maxview = 999;
        for($i; $i<= ($maxview); $i++){
            $strings[] = $i;
        }
        $string .= str_pad(array_rand($strings), 3, "0", STR_PAD_LEFT);
        return $string;
    }

    function __set($k,$v)
	{
		$this->$k  = $v;
	}
	
	function __get($k)
	{
		return $this->$k;
	}

	
}
