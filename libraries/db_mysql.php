<?php
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
*/
defined( 'TERRAGON' ) or die( 'Restricted access' );
class db_mysql
{
	/* public: connection parameters */
	var $Host     = "";
	var $Database = "";
	var $User     = "";
	var $Password = "";
	var $Dummy    = "";
	var $Persistent = false;
	var $Prefix 	= "";  
	var $table_load 	= array();	
	/* public: configuration parameters */
	var $Auto_Free     = 1;     ## Set to 1 for automatic mysql_free_result()
	var $Debug         = 0;     ## Set to 1 for debugging messages.
	var $debugrow = false;
	var $Halt_On_Error = ""; ## "yes" (halt with message), "no" (ignore errors quietly), "report" (ignore errror, but spit a warning)
	var $Halt_On_Noprefix = "yes"; ## "yes" (halt with message), "no" (ignore prefix)
	var $Seq_Table     = "db_sequence";	
	/* public: result array and current row number */
	var $Record   = array();
	var $Rowf = array();
	var $Row = array();
	/* public: current error number and error text */
	var $Errno    = 0;
	var $Error    = "";	
	/* public: this is an api revision, not a CVS revision. */
	var $type     = "mysql";
	var $revision = "2.0";	
	/* private: link ,query array , fetch array handles */
	var $Link_ID  = 0;
	var $Query_ID = array();
	var $Query_String = array();
	var $fetct_result = array();
	var $insert_id = ""; 
	var $dbcollat		= 'utf8_general_ci';
	var $char_set		= 'utf8';
	/* public: constructor */
	
	// Private variables
	var $_protect_identifiers	= TRUE;
	var $_reserved_identifiers	= array('*'); // Identifiers that should NOT be escaped
	var $swap_pre		= '';
	var	$_escape_char = '`';
	var $ar_aliased_tables		= array();
	var $fields			= array();
	var $keys			= array();
	var $primary_keys	= array();
	var $db_char_set	=	'';
	function DB_Sql($query = "") {	  
		if($query != "")     $this->query($query);
	}
	/* public: some trivial reporting */
	function link_id() {
		return $this->Link_ID;
	}	
	function query_id($block = 0) {
		return $this->Query_ID[$block];
	}  
	/* try connection */
	function try_connect($Host = "", $User = "", $Password = "") {
		/* Handle defaults */
		if ("" == $Host)      $Host     = $this->Host;
		if ("" == $User)      $User     = $this->User;
		if ("" == $Password)  $Password = $this->Password;	  
		if($this->Persistent)  $this->Link_ID = @mysql_pconnect($Host, $User, $Password);
		else	  $this->Link_ID = @mysql_connect($Host, $User, $Password);		
		$is_connect = $this->Link_ID ? true : false;		
		return $is_connect;
	}
	/* public: connection management */
	function connect($Database = "", $Host = "", $User = "", $Password = "") {
		/* Handle defaults */
		if ("" == $Database)	$Database = $this->Database;
		if ("" == $Host)		$Host     = $this->Host;
		if ("" == $User)		$User     = $this->User;
		if ("" == $Password)	$Password = $this->Password;
		/* establish connection, select database */
		if ( 0 == $this->Link_ID ){    	  
			if($this->Persistent)	$this->Link_ID=mysql_pconnect($Host, $User, $Password);
			else					$this->Link_ID=mysql_connect($Host, $User, $Password);			
			if (!$this->Link_ID) {
				$this->halt("connect($Host, $User, \$Password) failed.");
				return 0;
			}		
			if (!mysql_select_db($Database,$this->Link_ID)) {
				$this->halt("cannot use database ".$this->Database);				
				return 0;
			}//mysql_connect($Host,$this->Dummy,$Password); //dummy connection 
		}
		return $this->Link_ID;
	}
	/* test conection in case sever has gone away*/
	function test_connect($Database = "", $Host = "", $User = "", $Password = "") 
	{
		if ("" == $Database)	$Database = $this->Database;
		if ("" == $Host)		$Host     = $this->Host;
		if ("" == $User)		$User     = $this->User;
		if ("" == $Password)	$Password = $this->Password;
		/* establish connection, select database */
		if ( 0 == $this->Link_ID ){    	  
			if($this->Persistent)	$this->Link_ID=mysql_pconnect($Host, $User, $Password);
			else					$this->Link_ID=mysql_connect($Host, $User, $Password);			
			if (!$this->Link_ID) {
				$this->halt("connect($Host, $User, \$Password) failed.");
				return 0;
			}		
			if (!mysql_select_db($Database,$this->Link_ID)) {
				$this->halt("cannot use database ".$this->Database);				
				return 0;
			}//mysql_connect($Host,$this->Dummy,$Password); //dummy connection 
		}/*elseif (!mysql_ping($this->Link_ID)){	  
			if($this->Persistent)	$this->Link_ID=mysql_pconnect($Host, $User, $Password);
			else					$this->Link_ID=mysql_connect($Host, $User, $Password);			
			if (!$this->Link_ID) {
				$this->halt("connect($Host, $User, \$Password) failed.");
				return 0;
			}		
			if (!mysql_select_db($Database,$this->Link_ID)) {
				$this->halt("cannot use database ".$this->Database);				
				return 0;
			}mysql_connect($Host,$this->Dummy,$Password); //dummy connection 
		}*/
		return $this->Link_ID;
	}
	/* public: perform a query */
	function query($Query_String,$block = 0,$debug = false) { // set true to debug query
		/* No empty queries, please, since PHP4 chokes on them. */
		if ($Query_String == "")
		/* The empty query string is passed on from the constructor,
		* when calling the class without a query, e.g. in situations
		* like these: '$db = new DB_Sql_Subclass;'
		*/
		return 0;
		
		if (!$this->test_connect()) {
			return 0; /* we already complained in connect() about that. */
		};
		# New query, discard previous result.
		if(array_key_exists($block,$this->Query_ID)){
			$this->free($block);
		}
		/*
		* if query is not empty
		* it is passed to the $this->streilize_query()
		* to ensure that all query passed meet standard
		*/
		$this->Query_String[$block] = $Query_String = $this->streilize_query($Query_String);
		if ($this->Debug)	printf("Debug: query = %s<br>\n", $Query_String);		
		if($debug == true) echo $Query_String."<br>";
		$this->Query_ID[$block] = mysql_query($Query_String,$this->Link_ID);
		$this->Row[$block]   = 0;
		$this->Errno = mysql_errno();
		$this->Error = mysql_error();
		$this->insert_id = mysql_insert_id();
		if (!$this->Query_ID[$block]) {
			$this->halt($Query_String);
		}
		# Will return nada if it fails. That's fine.
		return $this->Query_ID[$block];
	}
	function push_query($Query_String,$block = 0,$debug = false) { // set true to debug query
		/* No empty queries, please, since PHP4 chokes on them. */
		if ($Query_String == "")
		/* The empty query string is passed on from the constructor,
		* when calling the class without a query, e.g. in situations
		* like these: '$db = new DB_Sql_Subclass;'
		*/
		return 0;	
		if (!$this->test_connect()) {
			return 0; /* we already complained in connect() about that. */
		};		
		# New query, discard previous result.
		if(array_key_exists($block,$this->Query_ID)){
			$this->free($block);
		}
		$this->Query_String[$block] = $Query_String;
		if ($this->Debug)	printf("Debug: query = %s<br>\n", $Query_String);		
		if($debug == true) printf("Debug: query = %s<br>\n", $Query_String);
		$this->Query_ID[$block] = mysql_query($Query_String,$this->Link_ID);
		$this->Row[$block]   = 0;
		$this->Errno = mysql_errno();
		$this->Error = mysql_error();
		$this->insert_id = mysql_insert_id();
		if (!$this->Query_ID[$block]) {
			$this->halt($Query_String);
		}
		# Will return nada if it fails. That's fine.
		return $this->Query_ID[$block];
	}
	/* public: discard the query result */
	function free($block = 0) {
		if (is_resource($this->Query_ID[$block])) mysql_free_result($this->Query_ID[$block]);
		$this->Query_ID[$block] = 0;
	}
	function fetch_result($block = 0,$qblock = 0) {
		$fieid_name = array();
		$i = 0;
		while ($i < $this->num_fields($qblock)) {
			$meta = mysql_fetch_field($this->Query_ID[$qblock], $i);
			if (!$meta)	continue;
				$fieid_name[] =  $meta->name;
			$i++;
		}
		$tem = array();
		if(array_key_exists($block,$this->fetct_result)){
			$this->fetct_result[$block] = 0;
		}
		if(count($fieid_name > 0)){
			for ($i=0; $i < $this->nf($qblock); $i++){
				$this->next_record($qblock);
				$result_info = array();
				foreach($fieid_name as $search){
					$result_info[$search] =   $this->filenane($search);
				}$tem[] = $result_info;
			}$this->fetct_result[$block] = $tem;
			$this->Rowf[$block]   = 0;
		}
		return $this->fetct_result[$block];		
	}
	function fetch_record($block = 0) {
		if(isset($this->fetct_result[$block]) && count($this->fetct_result[$block]) > 0){ // handle fetch record		
			$this->Record = $this->fetct_result[$block][$this->Rowf[$block]];
			$this->Rowf[$block]   += 1;
			$stat = is_array($this->Record);
			if (!$stat && $this->Auto_Free) {
				$this->fetct_result[$block] = array();
			}return $stat;			
		}			
	}
	function next_record($block = 0) {	 
		if (!$this->Query_ID[$block]) {
			$this->halt("next_record called with no query pending.");
			return 0;
		}
		$this->Record = mysql_fetch_array($this->Query_ID[$block]);
		$this->Row[$block]   += 1;
		$this->Errno  = mysql_errno();
		$this->Error  = mysql_error();		
		$stat = is_array($this->Record);
		if (!$stat && $this->Auto_Free) {
			$this->free($block);
		}
		return $stat;
	} 	
	/* public: evaluate the result (size, width) */
	function affected_rows() {
		return mysql_affected_rows($this->Link_ID);
	}
	function num_rows($block) {
		//echo $block .':';
		//var_dump($this->Query_ID[$block]);
		$row = mysql_num_rows($this->Query_ID[$block]);
		if($row == false && !is_resource($this->Query_ID[$block])){
			userErrorHandler('204', $this->Query_String[$block], $block, '', '');
			if($this->debugrow == true)	echo $this->Query_String[$block];
		}return $row;
	}	
	function num_fetched($block) {
		return count($this->fetct_result[$block]);
	}
	function count($block = 0) {
		return mysql_result($this->Query_ID[$block], 0, 0);
	}	
	function num_fields($block = 0) {
		return mysql_num_fields($this->Query_ID[$block]);
	}
	public function get_field($row = 0, $col = 0, $block = 0  ) {
		if(array_key_exists($row,$this->fetct_result)){			
			if(array_key_exists($col,$this->fetct_result[$row])){
				return $this->fetct_result[$row][$col];
			}			
		}
	}
	function print_fetch(){
		echo "<pre>";print_r($this->fetct_result);echo"</pre>";
	}
	function print_query(){
		echo "<pre>";print_r($this->Query_ID);echo"</pre>";
	}
	/* public: shorthand notation */
	function nf($block = 0) {
		return $this->num_rows($block);
	}	
	function np($block = 0) {
		return $this->num_fetched($block);
	}
	
	function f($Name) {
		if(isset($this->Record[$Name]))		return $this->Record[$Name];
		else 								return "";
	}
	
	function p($Name) {
		print $this->Record[$Name];
	}	
	function filenane($Name,$decode = false,$debug = false) {
		 
		if(array_key_exists($Name,$this->Record)){
			if(isset($this->Record[$Name]))	{
				//$value = str_replace("''","'",$this->Record[$Name]);
				$value = $this->Record[$Name];
				if($decode == true) $value = htmlspecialchars_decode($value,ENT_QUOTES);
				return stripslashes($value);
			}
			else	return "";			
		}else {
			if($debug == true)echo $Name." does not exist<br>";
			return "";
		}		
	}
	function tosql($value, $type,$quote = true,$encode = true)
	{
	  if($value == "" && $value != 0)	return "NULL";
	  else
		if($type == "Number") return floatval($value);
		else{
		  if(get_magic_quotes_gpc() == 0) {
		//	$value = str_replace("'","''",$value);
			$value = str_replace("\\","\\\\",$value);
		  } else{
		//	$value = str_replace("\\'","''",$value);
			$value = str_replace("\\\"","\"",$value);
		  }
		  if($encode == true) 	  $value = htmlspecialchars($value, ENT_QUOTES);		
		  if($quote == false)	return addslashes($value);
		  else		return "'" .addslashes($value) . "'";
		}
	}
	/* private: error handling */
	function halt($msg) {
		if (is_resource($this->Link_ID)) {
			$this->Error = mysql_error($this->Link_ID);
			$this->Errno = mysql_errno($this->Link_ID);
		}   
		if ($this->Halt_On_Error == "no")  return;	
		if ($this->Halt_On_Error == "report"){
			$this->haltmsg($msg);	
		}elseif ($this->Halt_On_Error == "yes"){
			$this->haltmsg($msg);
			echo Terragon::raise_warning('get');	
			die("Session halted.");
		}
	}
	function haltmsg($msg) {
		import('gcommon.includes.SqlFormatter');
		$msg =  SqlFormatter::format($msg);
		$e = sprintf("<b>Query error:</b> %s<br>\n", $msg);		
		$e .= sprintf("<b>MySQL Error</b>: %s (%s)<br>\n", $this->Errno,  $this->Error);
		Terragon::raise_warning ('set', array('msg' =>$e,'type'=>'alert'));
	}
	function streilize_query($Query_String)
	{
		$Query_String =  trim($Query_String);
		$Query_String = preg_replace('/\s\s+/', ' ',$Query_String);
		$BText = strpos($Query_String, "#__");	
		$endquery = strpos($Query_String, ";");	
		if($endquery === false){
			//die("End Query not specified by using ';' Query: ".$Query_String);
		}	
		if($BText === false){
			if($this->Halt_On_Noprefix == 'no')		 return $Query_String;
			 $this->halt("No table prefrix: : ".$Query_String);
			 return;
		}	
		$EText = strpos($Query_String," ", $BText);
		if($EText === false)
		{
			$string = strlen($Query_String);		
			$table = substr($Query_String, $BText + 3 - $string, $string - $BText + 3); 	
			$table = $this->replace_table ($table); 			
			$query = $this->preText($Query_String)." ".$table;		 	
			return $query;	
		}
		$table = substr($Query_String, $BText + 3, $EText - ($BText + 3));	
		$table = $this->replace_table ($table); 
		$preText = $this->preText($Query_String)." ".$table." ";	
		$Query_String = substr($Query_String,$EText+1); 
		if(strpos($Query_String, "#__") === false){
			$preText  .= $Query_String;		
			return $preText;
		}		
		if($Query_String != ""){	
			$preText  .= $this->streilize_query ($Query_String);
		}	
		return $preText;	  
	}
	function preText ($Query_String,$pos = 0)
	{	
		$Strp = strpos($Query_String, "#__");	
		return $preText = substr($Query_String,$pos,$Strp); 
	}
	function replace_table ($table)
	{
		if(empty($table)) return;
		$string = "#__".$table; // add string prefix to be replaced
		/*if(!in_array($table,$this->table_load))	{$prefix = "sipes_"; // change here for prefix settings ## important}else{}	*/
		$prefix = $this->Prefix;		
		$return = str_replace("#__" ,$prefix,$string);		
		return $return;
	}
	public function fixcond($cond='')
	{
		if(is_array($cond))
		{		    
		    $concate = ''; 
		     foreach($cond as $keyOp => $fOp)
		     {
		        if($keyOp == 'CONCATE' and count($cond)>1){						
					if(is_string($fOp))		$concate = $fOp;					
				}	
				if(is_array($fOp)){			
					if (array_key_exists('compare_type', $fOp) )	$c = $fOp['compare_type'];
					else											$c = '=';					
					foreach($fOp as $key => $value)
					{	
						if($key == 'compare_type') continue;
						if(trim($key)!='' and trim($value)!=''){
							if($c == 'like') $value = $value."%";	
							$whr[] = $key .' '.$c.' '.  $this->tosql($value,'Text') .'  ';
						}
					}
					if(strtolower($keyOp) == 'like') $keyOp = 'or';
					$w[] = "( ".implode(" $keyOp ",$whr)." )";
					unset($whr);									
				}		       
		     }
		   $cond = implode(" $concate ",$w);
		}
		$cond = preg_replace("/WHERE/i","",$cond);
		$cond = (trim($cond)!='') ? "WHERE ".$cond : "";
		return $cond;
	}
	function fixtable($table_param)
	{		
		$table = "";
		if(is_array($table_param)){					
			$table = $table_param['MAIN'];
			if ($this->Prefix != '')		$table = preg_replace("/{$this->Prefix}/","",$table);
			$table = preg_replace("/#__/","",$table);
			$table = "#__".$table;
			if (array_key_exists('TABLE', $table_param) && array_key_exists('ON', $table_param) ) {
				$i = 0;	$v = "";
				foreach($table_param['TABLE'] as $t){
					if ($this->Prefix != '')		$t = preg_replace("/{$this->Prefix}/","",$t);
					$t = preg_replace("/#__/","",$t);
					$c = " left join #__".$t;
					$param = $table_param['ON'][$i];
					foreach($param as $k => $v){
						$r = ' on '.$v .' = '.  $this->tosql($k,'Text',false) .'  ';
					}$h[] =	$v =  $c." ".$r;				
					$i++;
				}$v = implode(" ",$h);
				$table = $table." ".$v;
			}
		}elseif(is_string($table_param)){
			if ($this->Prefix != '')			$table = preg_replace("/{$this->Prefix}/","#__",$table_param);
			else $table = $table_param;
		}return $table;
	}
	public function insert($table,array $data,$ignore=false,$escapeencode = array(),$unquote = array(),$debug = false)
	{
		foreach($data as $key=>$value){
			$ky[] = $key;
			$quote = in_array($key,$unquote) ? false : true;
			$escape = in_array($key,$escapeencode) ? false : true;
			$val[] = $this->tosql(trim($value),'Text',$quote,$escape);
		}
		$table = $this->fixtable($table); 
		if($ignore == true){
			$ins = "INSERT IGNORE INTO $table (".implode(",",$ky).") VALUES (".implode(",",$val).")";
		}else{
			$ins  = "INSERT INTO $table (".implode(",",$ky).") VALUES (".implode(",",$val).")";
		}$this->query($ins,0,$debug);
	}
	public function update($table,array $data,$cond='')
	{
		///$uQ = array();
		foreach($data as $key=>$value){
			$uQ[] = $key.' = '. $this->tosql($value,'Text'); // TODO as per your requirement
		}
		$table = $this->fixtable($table); 
		$cond = $this->fixcond($cond);
		$uQ = implode(",",$uQ);
		$sql = "UPDATE $table SET $uQ $cond ";
		$this->query($sql);
		return $this->affected_rows();
	}
	public function get_Count($table,$cond='')
	{
		$cond = $this->fixcond($cond);
		$table = $this->fixtable($table);
		$sql = "SELECT COUNT(*) AS CNT FROM $table $cond";
		$this->query($sql);
		return  $this->count();
	}
	public function delete($table,$cond='')
	{
		$cond = $this->fixcond($cond);
		$table = $this->fixtable($table);
		$sql = "DELETE FROM $table $cond";
		$this->query($sql);
		return $this->affected_rows();
	}
	function _delete($table, $where = array(), $like = array(), $limit = FALSE)
	{
		$conditions = '';

		if (count($where) > 0 OR count($like) > 0)
		{
			$conditions = "\nWHERE ";
			$conditions .= implode("\n", $this->ar_where);

			if (count($where) > 0 && count($like) > 0)
			{
				$conditions .= " AND ";
			}
			$conditions .= implode("\n", $like);
		}

		$limit = ( ! $limit) ? '' : ' LIMIT '.$limit;

		return "DELETE FROM ".$table.$conditions.$limit;
	}
	/*
	e.g $sgl = $this->db->prepare('Select from #_table where id = %d and status = %s', array('10','good'));
	*/
	public function prepare($query, array $param)
	{
		$todb = array();
		foreach($param as $val){			
			$bool = is_numeric ( $val ) ? false : true;
			$todb[] = $this->tosql($val,'Text',$bool);
		}
		return vsprintf ( $query, $todb );
	}
	public function select($table,$field='*',$cond='',$start = null,$limit = null,$order_by = null,$direction =null,$group_by = null,$key = 0,$is_distinct = false,$degug=false)
	{
		$limit = $this->tosql((int)$limit,'Text',false);
		$start =  $this->tosql((int)$start,'Text',false);
		if($direction != null) $direction = $this->tosql($direction,'Text',false);
		$start = (isset($start) && $start>0) ? $start : '0';
		$limit = (isset($limit) && $limit > 0) ? sprintf("LIMIT %d,%d",$start,$limit) : "";
		$order_by = (isset($order_by) && trim($order_by)!='') ? "ORDER BY ".$this->tosql($order_by,'Text',false) : "";
		$group_by = (isset($group_by) && trim($group_by)!='') ? "GROUP BY ".$this->tosql($group_by,'Text',false) : "";
		$cond = $this->fixcond($cond);
		$table = $this->fixtable($table);   
		$select = (!$is_distinct) ? 'SELECT ' : 'SELECT DISTINCT ';    		
        $sql = "$select $field FROM $table $cond $group_by $order_by $direction $limit";		
		$this->query($sql,$key,$degug);
	}
	function table_names($block = 0) {
		$this->query("SHOW TABLES",$block);
		$i=0;
		while ($info=mysql_fetch_row($this->Query_ID[$block]))
		{
			$return[$i]["table_name"]= $info[0];
			$return[$i]["tablespace_name"]=$this->Database;
			$return[$i]["database"]=$this->Database;
			$i++;
		}return $return;
	}
	/* public: return table metadata */
	function metadata($table='',$full=false) {
		$count = 0;
		$id    = 0;
		$res   = array();
		/*
		* Due to compatibility problems with Table we changed the behavior
		* of metadata();
		* depending on $full, metadata returns the following values:
		*
		* - full is false (default):
		* $result[]:
		*   [0]["table"]  table name
		*   [0]["name"]   field name
		*   [0]["type"]   field type
		*   [0]["len"]    field length
		*   [0]["flags"]  field flags
		*
		* - full is true
		* $result[]:
		*   ["num_fields"] number of metadata records
		*   [0]["table"]  table name
		*   [0]["name"]   field name
		*   [0]["type"]   field type
		*   [0]["len"]    field length
		*   [0]["flags"]  field flags
		*   ["meta"][field name]  index of field named "field name"
		*   The last one is used, if you have a field name, but no index.
		*   Test:  if (isset($result['meta']['myfield'])) { ...
		*/
		
		// if no $table specified, assume that we are working with a query
		// result
		if ($table){
			$this->connect();
			$sql = $this->_list_columns($table);
			$id = $this->push_query($sql);
			//$id = mysql_list_fields($this->Database, $table, $this->Link_ID);
			if (!$id)$this->halt("Metadata query failed.");
		}else{
			$id = $this->Query_ID; 
			if (!$id) $this->halt("No query specified.");
		}
		$count = mysql_num_fields($id);				
		// made this IF due to performance (one if is faster than $count if's)
		if (!$full) {
			for ($i=0; $i<$count; $i++) {
				$res[$i]["table"] = mysql_field_table ($id, $i);
				$res[$i]["name"]  = mysql_field_name  ($id, $i);
				$res[$i]["type"]  = mysql_field_type  ($id, $i);
				$res[$i]["len"]   = mysql_field_len   ($id, $i);
				$res[$i]["flags"] = mysql_field_flags ($id, $i);
			}
		}else { // full
			$res["num_fields"]= $count;		
			for ($i=0; $i<$count; $i++) {
				$res[$i]["table"] = mysql_field_table ($id, $i);
				$res[$i]["name"]  = mysql_field_name  ($id, $i);
				$res[$i]["type"]  = mysql_field_type  ($id, $i);
				$res[$i]["len"]   = mysql_field_len   ($id, $i);
				$res[$i]["flags"] = mysql_field_flags ($id, $i);
				$res["meta"][$res[$i]["name"]] = $i;
			}
		}	
		// free the result only if we were called on a table
		if ($table) mysql_free_result($id);
		return $res;
	}
	function seek($pos = 0,$block = 0) {
		$status = mysql_data_seek($this->Query_ID[$block], $pos);
		if ($status)  $this->Row[$block] = $pos;
		else {
		  $this->halt("seek($pos) failed: result has ".$this->num_rows($block)." rows");		
		  /* half assed attempt to save the day, 
		   * but do not consider this documented or even
		   * desireable behaviour.
		   */
		  mysql_data_seek($this->Query_ID[$block], $this->num_rows($block));
		  $this->Row[$block] = $this->num_rows;
		  return 0;
		}return 1;
	}
 	 /* public: table locking */
	function lock($table, $mode="write") {
		$this->connect();	
		$query="lock tables ";
		if (is_array($table)) {
			while (list($key,$value)=each($table)) {
				if ($key=="read" && $key!=0) {
				$query.="$value read, ";
				}else {
					$query.="$value $mode, ";
				}
			}
			$query=substr($query,0,-2);
		}else {
			$query.="$table $mode";
		}
		$res = mysql_query($query, $this->Link_ID);
		if (!$res) {
			$this->halt("lock($table, $mode) failed.");
			return 0;
		}return $res;
	}  
	function unlock() {
		$this->connect();		
		$res = mysql_query("unlock tables");
		if (!$res) {
			$this->halt("unlock() failed.");
			return 0;
		}return $res;
	}
	/* public: sequence numbers */
	function nextid($seq_name) {   $this->connect();    
		if ($this->lock($this->Seq_Table)) {
			/* get sequence number (locked) and increment */
			$q  = sprintf("select nextid from %s where seq_name = '%s'",$this->Seq_Table,$seq_name);
			$id  = mysql_query($q, $this->Link_ID);
			$res = mysql_fetch_array($id);			
			/* No current value, make one */
			if (!is_array($res)) {
				$currentid = 0;
				$q = sprintf("insert into %s values('%s', %s)", $this->Seq_Table, $seq_name, $currentid);
				$id = mysql_query($q, $this->Link_ID);
			} else {
				$currentid = $res["nextid"];
			}
			$nextid = $currentid + 1;
			$q = sprintf("update %s set nextid = '%s' where seq_name = '%s'",$this->Seq_Table,$nextid,$seq_name);
			$id = mysql_query($q, $this->Link_ID);
			$this->unlock();
		} else {
			$this->halt("cannot lock ".$this->Seq_Table." - has it been created?");
			return 0;
		}return $nextid;
	}
	/**
	 * Create database
	 *
	 * @access	private
	 * @param	string	the database name
	 * @return	bool
	 */
	function _create_database($name)
	{
		return "CREATE DATABASE ".$name;
	}

	// --------------------------------------------------------------------

	/**
	 * Drop database
	 *
	 * @access	private
	 * @param	string	the database name
	 * @return	bool
	 */
	function _drop_database($name)
	{
		return "DROP DATABASE ".$name;
	}

	// --------------------------------------------------------------------

	/**
	 * Process Fields
	 *
	 * @access	private
	 * @param	mixed	the fields
	 * @return	string
	 */
	function _process_fields($fields)
	{
		$current_field_count = 0;
		$sql = '';

		foreach ($fields as $field=>$attributes)
		{
			// Numeric field names aren't allowed in databases, so if the key is
			// numeric, we know it was assigned by PHP and the developer manually
			// entered the field information, so we'll simply add it to the list
			if (is_numeric($field))
			{
				$sql .= "\n\t$attributes";
			}
			else
			{
				$attributes = array_change_key_case($attributes, CASE_UPPER);

				$sql .= "\n\t".$this->_protect_identifiers($field);

				if (array_key_exists('NAME', $attributes))
				{
					$sql .= ' '.$this->_protect_identifiers($attributes['NAME']).' ';
				}

				if (array_key_exists('TYPE', $attributes))
				{
					$sql .=  ' '.$attributes['TYPE'];

					if (array_key_exists('CONSTRAINT', $attributes))
					{
						switch ($attributes['TYPE'])
						{
							case 'decimal':
							case 'float':
							case 'numeric':
								$sql .= '('.implode(',', $attributes['CONSTRAINT']).')';
							break;

							case 'enum':
							case 'set':
								$sql .= '("'.implode('","', $attributes['CONSTRAINT']).'")';
							break;

							default:
								$sql .= '('.$attributes['CONSTRAINT'].')';
						}
					}
				}

				if (array_key_exists('UNSIGNED', $attributes) && $attributes['UNSIGNED'] === TRUE)
				{
					$sql .= ' UNSIGNED';
				}

				if (array_key_exists('DEFAULT', $attributes))
				{
					$sql .= ' DEFAULT \''.$attributes['DEFAULT'].'\'';
				}

				if (array_key_exists('NULL', $attributes) && $attributes['NULL'] === TRUE)
				{
					$sql .= ' NULL';
				}
				else
				{
					$sql .= ' NOT NULL';
				}

				if (array_key_exists('AUTO_INCREMENT', $attributes) && $attributes['AUTO_INCREMENT'] === TRUE)
				{
					$sql .= ' AUTO_INCREMENT';
				}
			}

			// don't add a comma on the end of the last field
			if (++$current_field_count < count($fields))
			{
				$sql .= ',';
			}
		}

		return $sql;
	}

	// --------------------------------------------------------------------

	/**
	 * Create Table
	 *
	 * @access	private
	 * @param	string	the table name
	 * @param	mixed	the fields
	 * @param	mixed	primary key(s)
	 * @param	mixed	key(s)
	 * @param	boolean	should 'IF NOT EXISTS' be added to the SQL
	 * @return	bool
	 */
	function _create_table($table, $fields, $primary_keys, $keys, $if_not_exists)
	{
		$sql = 'CREATE TABLE ';

		if ($if_not_exists === TRUE)
		{
			$sql .= 'IF NOT EXISTS ';
		}

		$sql .= $this->_escape_identifiers($table)." (";

		$sql .= $this->_process_fields($fields);

		if (count($primary_keys) > 0)
		{
			$key_name = $this->_protect_identifiers(implode('_', $primary_keys));
			$primary_keys = $this->_protect_identifiers($primary_keys);
			$sql .= ",\n\tPRIMARY KEY ".$key_name." (" . implode(', ', $primary_keys) . ")";
		}

		if (is_array($keys) && count($keys) > 0)
		{
			foreach ($keys as $key)
			{
				if (is_array($key))
				{
					$key_name = $this->_protect_identifiers(implode('_', $key));
					$key = $this->_protect_identifiers($key);
				}
				else
				{
					$key_name = $this->_protect_identifiers($key);
					$key = array($key_name);
				}

				$sql .= ",\n\tKEY {$key_name} (" . implode(', ', $key) . ")";
			}
		}

		$sql .= "\n) DEFAULT CHARACTER SET {$this->char_set} COLLATE {$this->dbcollat};";

		return $sql;
	}

	// --------------------------------------------------------------------

	/**
	 * Drop Table
	 *
	 * @access	private
	 * @return	string
	 */
	function _drop_table($table)
	{
		return "DROP TABLE IF EXISTS ".$this->_escape_identifiers($table);
	}

	// --------------------------------------------------------------------

	/**
	 * Alter table query
	 *
	 * Generates a platform-specific query so that a table can be altered
	 * Called by add_column(), drop_column(), and column_alter(),
	 *
	 * @access	private
	 * @param	string	the ALTER type (ADD, DROP, CHANGE)
	 * @param	string	the column name
	 * @param	array	fields
	 * @param	string	the field after which we should add the new field
	 * @return	object
	 */
	function _alter_table($alter_type, $table, $fields, $after_field = '')
	{
		$sql = 'ALTER TABLE '.$this->_protect_identifiers($table)." $alter_type ";

		// DROP has everything it needs now.
		if ($alter_type == 'DROP')
		{
			return $sql.$this->_protect_identifiers($fields);
		}

		$sql .= $this->_process_fields($fields);

		if ($after_field != '')
		{
			$sql .= ' AFTER ' . $this->_protect_identifiers($after_field);
		}

		return $sql;
	}

	function _truncate($table)
	{
		return "TRUNCATE ".$table;
	}
	function _list_tables($prefix_limit = FALSE)
	{
		
		$sql = "SHOW TABLES FROM ".$this->_escape_char.$this->Database.$this->_escape_char;

		if ($prefix_limit !== FALSE AND $this->Prefix != '')
		{
			$sql .= " LIKE '".$this->escape_str($this->Prefix,true)."%'";
		}

		return $sql;
	}
	function _list_columns($table = '',$column = '')
	{
		$sql = "SHOW COLUMNS FROM ".$this->_protect_identifiers($table, TRUE, NULL, FALSE);
		if($column != ''){
			$sql .= " LIKE '".$this->escape_str($column,true)."%'";
		}
		return $sql;
	}
	
	function _rename_table($table_name, $new_table_name)
	{
		$sql = 'ALTER TABLE '.$this->_protect_identifiers($table_name)." RENAME TO ".$this->_protect_identifiers($new_table_name);
		return $sql;
	}
	
	function escape_str($str, $like = FALSE)
	{
		if (is_array($str))
		{
			foreach ($str as $key => $val)
	   		{
				$str[$key] = $this->escape_str($val, $like);
	   		}

	   		return $str;
	   	}

		if (function_exists('mysql_real_escape_string') AND is_resource($this->Link_ID))
		{
			$str = mysql_real_escape_string($str, $this->Link_ID);
		}
		elseif (function_exists('mysql_escape_string'))
		{
			$str = mysql_escape_string($str);
		}
		else
		{
			$str = addslashes($str);
		}

		// escape LIKE condition wildcards
		if ($like === TRUE)
		{
			$str = str_replace(array('%', '_'), array('\\%', '\\_'), $str);
		}

		return $str;
	}
	
	function _escape_identifiers($item)
	{
		if ($this->_escape_char == '')
		{
			return $item;
		}

		foreach ($this->_reserved_identifiers as $id)
		{
			if (strpos($item, '.'.$id) !== FALSE)
			{
				$str = $this->_escape_char. str_replace('.', $this->_escape_char.'.', $item);

				// remove duplicates if the user already included the escape
				return preg_replace('/['.$this->_escape_char.']+/', $this->_escape_char, $str);
			}
		}

		if (strpos($item, '.') !== FALSE)
		{
			$str = $this->_escape_char.str_replace('.', $this->_escape_char.'.'.$this->_escape_char, $item).$this->_escape_char;
		}
		else
		{
			$str = $this->_escape_char.$item.$this->_escape_char;
		}

		// remove duplicates if the user already included the escape
		return preg_replace('/['.$this->_escape_char.']+/', $this->_escape_char, $str);
	}
	
	function _protect_identifiers($item, $prefix_single = FALSE, $protect_identifiers = NULL, $field_exists = TRUE)
	{
		if ( ! is_bool($protect_identifiers))
		{
			$protect_identifiers = $this->_protect_identifiers;
		}

		if (is_array($item))
		{
			$escaped_array = array();

			foreach ($item as $k => $v)
			{
				$escaped_array[$this->_protect_identifiers($k)] = $this->_protect_identifiers($v);
			}

			return $escaped_array;
		}

		// Convert tabs or multiple spaces into single spaces
		$item = preg_replace('/[\t ]+/', ' ', $item);

		// If the item has an alias declaration we remove it and set it aside.
		// Basically we remove everything to the right of the first space
		$alias = '';
		if (strpos($item, ' ') !== FALSE)
		{
			$alias = strstr($item, " ");
			$item = substr($item, 0, - strlen($alias));
		}

		// This is basically a bug fix for queries that use MAX, MIN, etc.
		// If a parenthesis is found we know that we do not need to
		// escape the data or add a prefix.  There's probably a more graceful
		// way to deal with this, but I'm not thinking of it -- Rick
		if (strpos($item, '(') !== FALSE)
		{
			return $item.$alias;
		}

		// Break the string apart if it contains periods, then insert the table prefix
		// in the correct location, assuming the period doesn't indicate that we're dealing
		// with an alias. While we're at it, we will escape the components
		if (strpos($item, '.') !== FALSE)
		{
			$parts	= explode('.', $item);

			// Does the first segment of the exploded item match
			// one of the aliases previously identified?  If so,
			// we have nothing more to do other than escape the item
			if (in_array($parts[0], $this->ar_aliased_tables))
			{
				if ($protect_identifiers === TRUE)
				{
					foreach ($parts as $key => $val)
					{
						if ( ! in_array($val, $this->_reserved_identifiers))
						{
							$parts[$key] = $this->_escape_identifiers($val);
						}
					}

					$item = implode('.', $parts);
				}
				return $item.$alias;
			}

			// Is there a table prefix defined in the config file?  If not, no need to do anything
			if ($this->Prefix != '')
			{
				// We now add the table prefix based on some logic.
				// Do we have 4 segments (hostname.database.table.column)?
				// If so, we add the table prefix to the column name in the 3rd segment.
				if (isset($parts[3]))
				{
					$i = 2;
				}
				// Do we have 3 segments (database.table.column)?
				// If so, we add the table prefix to the column name in 2nd position
				elseif (isset($parts[2]))
				{
					$i = 1;
				}
				// Do we have 2 segments (table.column)?
				// If so, we add the table prefix to the column name in 1st segment
				else
				{
					$i = 0;
				}

				// This flag is set when the supplied $item does not contain a field name.
				// This can happen when this function is being called from a JOIN.
				if ($field_exists == FALSE)
				{
					$i++;
				}

				// Verify table prefix and replace if necessary
				if ($this->swap_pre != '' && strncmp($parts[$i], $this->swap_pre, strlen($this->swap_pre)) === 0)
				{
					$parts[$i] = preg_replace("/^".$this->swap_pre."(\S+?)/", $this->Prefix."\\1", $parts[$i]);
				}

				// We only add the table prefix if it does not already exist
				if (substr($parts[$i], 0, strlen($this->Prefix)) != $this->Prefix)
				{
					$parts[$i] = $this->Prefix.$parts[$i];
				}

				// Put the parts back together
				$item = implode('.', $parts);
			}

			if ($protect_identifiers === TRUE)
			{
				$item = $this->_escape_identifiers($item);
			}

			return $item.$alias;
		}

		// Is there a table prefix?  If not, no need to insert it
		if ($this->Prefix != '')
		{
			// Verify table prefix and replace if necessary
			if ($this->swap_pre != '' && strncmp($item, $this->swap_pre, strlen($this->swap_pre)) === 0)
			{
				$item = preg_replace("/^".$this->swap_pre."(\S+?)/", $this->Prefix."\\1", $item);
			}

			// Do we prefix an item with no segments?
			if ($prefix_single == TRUE AND substr($item, 0, strlen($this->Prefix)) != $this->Prefix)
			{
				$item = $this->Prefix.$item;
			}
		}

		if ($protect_identifiers === TRUE AND ! in_array($item, $this->_reserved_identifiers))
		{
			$item = $this->_escape_identifiers($item);
		}

		return $item.$alias;
	}
	protected function _track_aliases($table)
	{
		if (is_array($table))
		{
			foreach ($table as $t)
			{
				$this->_track_aliases($t);
			}
			return;
		}

		// Does the string contain a comma?  If so, we need to separate
		// the string into discreet statements
		if (strpos($table, ',') !== FALSE)
		{
			return $this->_track_aliases(explode(',', $table));
		}

		// if a table alias is used we can recognize it by a space
		if (strpos($table, " ") !== FALSE)
		{
			// if the alias is written with the AS keyword, remove it
			$table = preg_replace('/ AS /i', ' ', $table);

			// Grab the alias
			$table = trim(strrchr($table, " "));

			// Store the alias, if it doesn't already exist
			if ( ! in_array($table, $this->ar_aliased_tables))
			{
				$this->ar_aliased_tables[] = $table;
			}
		}
	}

}
?>