<?php 
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
defined( 'TERRAGON' ) or die( 'Restricted access' );

require_once(__DIR__.'/predis/Autoloader.php');

class Predis {
	public $client;
	var $soft 		= false;
	var $conected 	= false; 
	var $scheme;	
	var $host;		
	var $port; 			
	var $database;

	function __construct(){
		
	}
	public function connect(){
		Predis\Autoloader::register();
		try {
			global $rediscli;
			$this->client = new Predis\Client(array(
				"scheme" 		=> $this->scheme,
				"host" 			=> $this->host,
				"port" 			=> $this->port,
				"database" 	=> $this->database,
				"password" 	=> "103REDISSERVER"
			));
			$this->conected = true;
		}
		catch (Exception $e) {
			if($this->soft == false){
				show_error("Unable to connect to Redis: {$e->getMessage()}", 500);
			}
		}
	}
}
