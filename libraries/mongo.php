<?php
defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Oluwasegun Matthew (07060514642)
* @email       oadetimehin@terragonltd.com
* @update      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261)
* @update      Satope Dayo
* @email       osatope@terragonltd.com (08069385406)
**/
class mongo {  
	var $mongoLib;
	function __construct(){
		parent::__construct();
		global $mongo;
		$this->mongoLib = $mongodb;   
	}
	/**
	*  --------------------------------------------------------------------------------
	*  LOAD
	*  --------------------------------------------------------------------------------
	*
	*  Loads all documents from the passed collection, limit or start can also be passed
	*
	*  @usage = $this->mongo_model->load('foo', $data = array());
	*/
	public function load($collection,$condition=array(),$start=null,$limit=null){
		if (!empty($condition)){
			$this->mongoLib->where($condition);
		}  
		$data = $this->mongoLib->get($collection,$limit,$start);   
		return $data;
	}
	//work on this
	public function filter_load($collection,$condition=array(),$start=null,$limit=null){
		if (!empty($condition)){
			$this->mongoLib->or_like_field($condition);
		}    
		$results = $this->mongoLib->get($collection,$limit,$start);
		return $results;  
	}
	/**
	*  --------------------------------------------------------------------------------
	*  LOAD WHERE GREATER THAN
	*  --------------------------------------------------------------------------------
	*
	*  Loads all documents from the passed collection where a field is greater than a value, limit or start can also be passed
	*
	*  @usage = $this->mongo_model->loadgt('foo','exp',10 );
	*/
	public function loadgt($collection,$field, $value,$start=null,$limit=null){
		$this->mongoLib->where_gt($field,$value);
		$data = $this->mongoLib->get($collection,$limit,$start);   
		return $data;
	}
	/**
	*  --------------------------------------------------------------------------------
	*  ADD
	*  --------------------------------------------------------------------------------
	*
	*  Add a new document to the passed collection
	*
	*  @usage = $this->mongo_model->load('foo', $data = array());
	*/
	public function add($collection,$data){  
		return $this->mongoLib->insert($collection, $data);
	}
	/**
	*  --------------------------------------------------------------------------------
	*  UPDATE
	*  --------------------------------------------------------------------------------
	*
	*  Update  document in the passed collection
	*
	*  @usage = $this->mongo_model->update('foo', $data = array(),$condition = array());
	*/ 
	public function update ($collection,$data,$condition){
		$this->mongoLib->where($condition);
		return $this->mongoLib->update_all($collection, $data);
	}
	public function delete($collection,$data){
		return $this->mongoLib->delete_all($collection, $data);
	}
	public function check($collection,$data){    
		return $this->mongoLib->check($collection, $data);
	}
	public function count($collection,$condition = array()){  
		if (!empty($condition)){
			$this->mongoLib->where($condition);
		}  
		return $this->mongoLib->count_all_results($collection);
	}
	public function or_count($collection,$condition = array()){  
		if (!empty($condition)){
			$this->mongoLib->or_like_field($condition);
		}  
		return $this->mongoLib->count_all_results($collection);
	}
}
?>