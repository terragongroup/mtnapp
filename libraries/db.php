<?php
defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class db {  
	var $db;
	function __construct($param = ""){
		global $db;
		//$this->db = $db;  
		$this->db	=& Terragon::dbconnect($param); 
	}
	
	public function load($table,$condition = '',$start=null,$limit=null){
		$this->db->select($table, '*', $condition, $start, $limit, '', '', null, 'get',false,false);	
		return $this->db->fetch_result('fetch','get');
	}

	public function count($table,$data = '*',$condition = ''){
		$this->db->select($table,'count('.$data.')',$condition, null, null, '', '', null, 0,false,false);	
		return $this->db->count();
	}
	
	public function add($table,$data){  
		return $this->db->insert($table, $data);
	}
	
	public function update ($table,$data,$condition){
		$update= $this->db->update($table, $data,$condition);
		return $update;
	}

	public function delete($table,$condition){
		return $this->db->delete($table, $condition);
	}
}
?>