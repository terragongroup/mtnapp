<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class MsisdnService{
	
	public $client;

	private $push 			= '';
	private $DATA			= array();

	private $msisdn			= "";
	private $productid		= "";


	var $redisactive = false;

	function __construct($msisdn = "",$productid = ""){
		$this->redis_connection();
		$this->init($msisdn,$productid);
	}

	protected function redis_connection(){
		global $redis;
       	$this->client = $redis->client;      
       	if(!is_object($this->client)){
       		echo "Couldn't connected to on msisdn layer<br>";
       	}
       $this->redisactive = true;
	}
	private function init($msisdn,$productid){
		$this->msisdn 		= msisdn_sanitizer($msisdn);
		$this->productid 	= $productid;
		$this->push = "service:".$this->msisdn.':'.$this->productid;
	}

	function PushService($status,$partner){
		$time 		= get_time_key();
		if($this->exist() == true){
			$data = $this->DATA;
			$data['status'] 	= $status;
			$data['partner'] 	= $partner;
			$data['time'] 		= $time;
			$data['senttime'] 	= time();
		}else{
			$data['status'] 	= $status;
			$data['partner'] 	= $partner;
			$data['time'] 		= $time;
			$data['senttime'] 	= time();
		}
		try {
      		$this->client->hmset($this->push,$data);
			$this->DATA = $this->client->hgetall($this->push); 
	    }catch (Exception $e) {
	    	$this->DATA = $data;
	    }
	}

	function subscribe($duration){
		if($this->exist() == true){
			$time_created 		= date("Y-m-d h:i:s");
		    $expiry_date 		= date("Ymd",strtotime ("+$duration days", strtotime($time_created)));
		   
			$data = $this->DATA;
			$data['subscription_time'] 	= $time_created;
			$data['expiry_date']  		= $expiry_date;
			$data['duration']  			= $duration;		
			/*
			$bill_cycle_start   = date("Ymd")."000000";
		    $bill_cycle_end     = date("Ymd",strtotime ("+$duration days", strtotime($bill_cycle_start)))."000000";
			$data['bill_cycle_start'] 	= $bill_cycle_start;
			$data['bill_cycle_end'] 	= $bill_cycle_end;
			*/
		
			try {
	      		$this->client->hmset($this->push,$data);
				$this->DATA = $this->client->hgetall($this->push); // update attempt array
		    }catch (Exception $e) {
		    	$this->DATA = $data;
		    }
		}
	}

	function unsubscribe(){
		if($this->exist() == true){
			$this->setStatus('inactive');
		}
	}

	function setStatus($status){
		if($this->exist() == true){
			try {
		      	$data = $this->DATA;
				$data['status'] = $status;
				//$this->client->hmset($this->push,$data);
				$this->client->hset($this->push,'status',$status);
				$this->DATA = $this->client->hgetall($this->push); 
		    }catch (Exception $e) {
		    	$this->DATA['status'] = $status;
		    }
		}
	}

	function setPartner($partner){
		if($this->exist() == true){
			try {
		      	$data = $this->DATA;
				$data['partner'] = $partner;
				//$this->client->hmset($this->push,$data);
				$this->client->hset($this->push,'partner',$partner);
				$this->DATA = $this->client->hgetall($this->push); 
		    }catch (Exception $e) {
		    	$this->DATA['partner'] = $status;
		    }
		}
	}

	function getStatus(){
		if($this->exist() == true){
			if(array_key_exists('status', $this->DATA)){
				return $this->DATA['status'];
			}
		}
		return "";
	}

	function getLasttimeupdated(){
		if($this->exist() == true){
			if(array_key_exists('time', $this->DATA)){
				return $this->DATA['time'];
			}
			return "";
		}
		return "";
	}

	function getLastSentTime(){
		if($this->exist() == true){
			if(array_key_exists('senttime', $this->DATA)){
				return $this->DATA['senttime'];
			}
			return "";
		}
		return "";
	}

	function getServices(){
		try {
	      	if(count($this->DATA) > 0){
				return $this->DATA;
			}
			$this->DATA = $this->client->hgetall($this->push);
			return $this->DATA;
	    }catch (Exception $e) {
	     	return $this->DATA;
	    }
	}

	function exist(){
		$this->getServices();
		return (count($this->DATA) > 0) ? true : false;
	}


}
