<?php 
defined( 'TERRAGON' ) or die( 'Restricted access' );
require_once __DIR__ . '/RabbitMQ/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
class RabbitMQWorker {
	public $channel;
	var $connected = false;
	VAR $REDISCACHE = array();
	function __construct(){
		$this->rabbit_connection();
	}
	protected function rabbit_connection(){
		$rabbitmq 	=& Terragon::rabbitmq(true); 
		if($rabbitmq->connected == true){
			$this->channel = $rabbitmq->channel;      	
	       	if(!is_object(	$this->channel)){
	       		echo "Couldn't connected to RabbitMQ Channel";
	       	}
	       	$this->connected = true;
		}

	}
	public function PushToQueue($queue,$info){
		if($this->connected == true){
			$queue_name = $queue;
			try{
				$this->channel->queue_declare($queue_name, false, true, false, false);
				$processed_data = json_encode($info);
				$sender_message = new AMQPMessage($processed_data,array('delivery_mode' => 2));
				$this->channel->basic_publish($sender_message,'',$queue_name);
				$producer_response = array('status'=>true,'data'=>'Data Sent');
			}catch(Exception $ex){
				$producer_response = array('status'=>false,'data'=>'Something went wrong.');
			}
		}
		return $producer_response;
	}
}
