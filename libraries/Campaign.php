<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class Campaign{

	private $DATA			= array();
	private $data			= "";
	private $campid			= "";
	private $dbtool;
	private $table;
	var $exist = false;
	VAR $cache = "";
	function __construct(){
		$this->table = "#__".Options::$_cpdetails;
	}
	
	function get($data = array()){
		global $db;
		$where = "";
		$count = count($data);
		if(count($data) > 0){

			$key = array_keys($data);
			
			$item = $data[$key[0]];
			
			if($item == $this->cache){
				return;
			}
				
			$this->cache = $item;
			
			$where = "where ";
			$c = 0;
			foreach($data as $k => $v){
				$c++;
				$where .= $k."=".$db->tosql($v,'Text');
				if($c != $count)
				$where .= "and ";
			}
		}
		
		$db->select($this->table, '*', $where, null, null, '', '', null, 'contentdata',false,false);	
			
		if($db->nf('contentdata') > 0)
		{	
			$this->exist = true;
			$data = $db->fetch_result('contentfetchdata','contentdata');
			foreach($data[0] as $k => $v) {
				$this->$k = $v;		
			}
		}
	}


	function get_subscribers($camp_id, $status = '', $start = 0, $limit = 10){

		$start 		= empty($start) ? 0: $start;
		$limit 		= empty($limit) ? 10: $limit;

		$elastic 	=& Terragon::elastic(); 
	
		$params = array(
			'index' => 'mtnsdp',
			'type' 	=> 'userdata'
		);	
		$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" =>  $camp_id  , "fields" => ["cds_campaign_id"], 'operator' => 'and' , "type"=> "cross_fields"];
		if(!empty($status)){
			$params['body']['query']['bool']['must'][]['multi_match'] =  ["query" =>  $status , "fields" => ["subscription_status"], 'operator' => 'and' , "type"=> "cross_fields"];
		}
		$params['body']['from'] = $start;
		$params['body']['size'] = $limit;
		$DATA = $elastic->search($params);
		$total = $DATA['hits']['total'];
		$data = $DATA['hits']['hits'];
		return array('total'=>$total,'data'=>$data);
	}

	function exist(){
		return $this->exist;
	}

	function cpupdate($msisdn,$type, $add_to_attempt_log = false){

		$product_id = $this->product_id;
		
		$tool 		=& Terragon::load_libraries('db');

		$time 		= $this->get_time_key();

		$msisdn = msisdn_sanitizer($msisdn,false);

		$add_to_attempt_log = ($add_to_attempt_log == false) ? 'no' : 'yes';

		$data = array(
	        'product_id'	=> $product_id,
	        'msisdn'		=> $msisdn, 
	        'type'			=> $type,
	        'timestamp'		=> $time
	    );

	     $rmq =& Terragon::load_libraries('RabbitMQWorker');

		$strings	= array('MTN_CP_UPDATE','MTN_CP_UPDATE_NEW');
		$key		= array_rand($strings);
		$queue_name =  $strings[$key];

		//$queue_name = "MTN_CP_UPDATE_NEW";
	
		if($rmq->connected == true){
			$data['add_to_attempt_log'] = $add_to_attempt_log;
			$rmq->PushToQueue($queue_name,$data);
		}else{
			$tool->add("#__".Options::$_cpupdate,$data);
		}
		/*
	    try{
			 $rmq =& Terragon::load_libraries('RabbitMQWorker');
	
			if($rmq->connected == true){
				$rmq->PushToQueue('MTN_CP_UPDATE',$data);
			}else{
				$tool->add("#__".Options::$_cpupdate,$data);
			}
		}catch(Exception $ex){
			$tool->add("#__".Options::$_cpupdate,$data);
		}
		*/
	}

	function get_time_key($m = '',$d = '',$y = ''){
		$d = ($d == '') ? date('d') : $d;
		$m = ($m == '') ? date('m') : $m;
		$y = ($y == '') ? date('Y') : $y;
		return mktime(0, 0, 0, $m, $d, $y);
	}

	function __set($k,$v)
	{
		$this->$k  = $v;
	}
	
	function __get($k)
	{
		return $this->$k;
	}

}
