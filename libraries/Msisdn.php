<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261)
**/
class Msisdn {
	protected $_ci;
	public $client;

	private $push 			= '';
	private $MsisdnDATA		= array();
	private $msisdn			= "";

	var $new_collection;
	var $renew_collection;	
	var $active_collection;
	var $inactive_collection;	
	var $pending_collection;

	var $status = array(
		'active' 	=> 'active',
		'pending' 	=> 'pending',
		'renew'		=> 'renew',
		'new' 		=> 'new',
		'inactive' 	=> 'inactive'
	); 

	var $collection = array(
		'active' 	=> '',
		'pending' 	=> '',
		'renew'		=> '',
		'new' 		=> '',
		'inactive' 	=> ''
	); 

	var $redisactive = false;

	function __construct($msisdn = ""){
		
		$this->_ci = &get_instance();
		
		$this->_ci->load->model('mongo_model','MDL');
		
		$this->collection = array(
			'active' 	=> Options::$_active_collection,
			'pending' 	=> Options::$_pending_collection,
			'renew'		=> Options::$_renew_collection,
			'new' 		=> Options::$_new_collection,
			'inactive' 	=> Options::$_inactive_collection
		); 

		$this->redis_connection();

		if(!empty($msisdn))
			$this->init($msisdn);
		
	}


	function &getMsisdn($msisdn)
	{
		static $instances;
		
		if (!isset ($instances)) 	$instances = array ();

		if (empty($instances[$msisdn])) {	
			$instances[$msisdn] = new Msisdn($msisdn);
		}
		return $instances[$msisdn];
	}

	protected function redis_connection(){
		global $redis;
       	$this->client = $redis;      
       	if(!is_object($this->client)){
       		echo "Couldn't connect to on msisdn layer<br>";
       	}
       $this->redisactive = true;
	}

	private function init($msisdn){
		$this->msisdn = $msisdn;
		$this->push = "msisdn:".$this->msisdn;
	}

	public function PushService($serviceid,$status){
		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}
		if($this->exist() == true){
			$data = $this->MsisdnDATA;
			if($data[$serviceid] != $status){
				$data[$serviceid] = $status;
				$this->client->hmset($this->push,$data);
				$this->MsisdnDATA = $this->client->hgetall($this->push); // update service array
			}
		}else{
			$data[$serviceid] = $status;
			$this->client->hmset($this->push,$data);
			$this->MsisdnDATA = $this->client->hgetall($this->push); // update service array
		}
	}

	function ServiceExist($serviceid){
		$this->getServices();
		if(array_key_exists($serviceid, $this->MsisdnDATA)){
			return true;
		}return false;
	}

	function getServices(){
		if(count($this->MsisdnDATA) > 0){
			return $this->MsisdnDATA;
		}
		$this->MsisdnDATA = $this->client->hgetall($this->push);
		return $this->MsisdnDATA;
	}

	function exist(){
		$this->getServices();
		return (count($this->MsisdnDATA) > 0) ? true : false;
	}

	function getServiceStatus($serviceid){
		if($this->exist() == true){
			if(array_key_exists($serviceid, $this->MsisdnDATA)){
				return $this->MsisdnDATA[$serviceid];
			}return '';
		}return false;
	}



	
	function SetActive($serviceid,$moredata = array()){

		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}	
		$param = array('service_duration'=>'','subscription_date'=>'','expiry_date'=>'','duration'=>'','route'=>'');
		foreach($param as $c=>$v){
			if(isset($moredata[$c])) $param[$c] = $moredata[$c];
		}

		$serviceid = (int)$serviceid;

		if($this->getServiceStatus($serviceid) == $this->status['active'])
			return;

		$CurrentStatus = $this->getServiceStatus($serviceid);

		$this->removedocument($CurrentStatus,$serviceid);

		$this->PushService($serviceid,$this->status['active']);

		$data = array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
		);
		$savedata = array_merge($data, $param);

		$this->_ci->MDL->add($this->collection['active'],$savedata);
		
		$data = array(
			'msisdn'=>$this->msisdn,
			'service'=>$serviceid,
			'user_status'=>$this->status['active']
		);
		$savedata = array_merge($data, $param);

		$this->_ci->rMQ->PushToQueue($this->_ci->config->item('rabbit_sub_queue_prefix'), $savedata );

	}
	
	function SetNew($serviceid){
		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}	
		$serviceid = (int)$serviceid;

		if($this->getServiceStatus($serviceid) == $this->status['new'])
			return;

		$CurrentStatus = $this->getServiceStatus($serviceid);

		$this->removedocument($CurrentStatus,$serviceid);

		$this->PushService($serviceid,$this->status['new']);
		
		$this->_ci->MDL->add($this->collection['new'],array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
			'status'	=> 'active'			//billing attempt is active
		));

		$this->_ci->rMQ->PushToQueue($this->_ci->config->item('rabbit_sub_queue_prefix'), 
			array(
				'msisdn'	=> $this->msisdn,
				'service'	=> $serviceid,
				'user_status'	=> $this->status['new']
			)
		);
	}
	function SetRenew($serviceid){
		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}	
		$serviceid = (int)$serviceid;

		if($this->getServiceStatus($serviceid) == $this->status['renew'])
			return;

		$CurrentStatus = $this->getServiceStatus($serviceid);

		$this->removedocument($CurrentStatus,$serviceid);

		$this->PushService($serviceid,$this->status['renew']);
		
		$this->_ci->MDL->add($this->collection['renew'],array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
			'status'	=> 'active'
		));

		// add rabbitmq layer

		$this->_ci->rMQ->PushToQueue($this->_ci->config->item('rabbit_sub_queue_prefix'), 
			array(
				'msisdn'	=> $this->msisdn,
				'service'	=> $serviceid,
				'user_status'	=> $this->status['renew']
			)
		);
	}
	function SetInactive($serviceid){
		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}	
		$serviceid = (int)$serviceid;

		if($this->getServiceStatus($serviceid) == $this->status['inactive'])
			return;

		$CurrentStatus = $this->getServiceStatus($serviceid);

		$this->removedocument($CurrentStatus,$serviceid);

		$this->PushService($serviceid,$this->status['inactive']);
	
		$this->_ci->MDL->add($this->collection['inactive'],array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
		));
		
		$this->_ci->rMQ->PushToQueue($this->_ci->config->item('rabbit_sub_queue_prefix'), 
			array(
				'msisdn'	=> $this->msisdn,
				'service'	=> $serviceid,
				'user_status'	=> $this->status['inactive']
			)
		);
	}
	function SetPending($serviceid){
		if($this->redisactive == false){
			// to do | handle redis failure
			return;
		}	
		$serviceid = (int)$serviceid;

		if($this->getServiceStatus($serviceid) == $this->status['pending'])
			return;

		$CurrentStatus = $this->getServiceStatus($serviceid);

		$this->removedocument($CurrentStatus,$serviceid);

		$this->PushService($serviceid,$this->status['pending']);
		
		$this->_ci->MDL->add($this->collection['pending'],array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
		));

		$this->_ci->rMQ->PushToQueue($this->_ci->config->item('rabbit_sub_queue_prefix'), 
			array(
				'msisdn'	=> $this->msisdn,
				'service'	=> $serviceid,
				'user_status'	=> $this->status['pending']
			)
		);
	}

	function clear()
	{
		if($this->exist() == true){
			foreach ($this->MsisdnDATA as $serviceid => $CurrentStatus) {
				$this->removedocument($CurrentStatus,$serviceid);
			}
			$this->client->hmset($this->push,array());
			$this->MsisdnDATA = $this->client->hgetall($this->push); // update service array
		}return false;
	}

	function delete(){ 
		if($this->exist() == true){
			foreach ($this->MsisdnDATA as $serviceid => $CurrentStatus) {
				$this->removedocument($CurrentStatus,$serviceid);
			}
			$this->client->del($this->push);
		}
	}

	function deleteService($serviceid){
		if($this->ServiceExist($serviceid) == true){
			$CurrentStatus = $this->getServiceStatus($serviceid);
			unset($this->MsisdnDATA[$serviceid]);
			$this->client->hmset($this->push,$this->MsisdnDATA);
			$this->MsisdnDATA = $this->client->hgetall($this->push); // update service array
			$this->removedocument($CurrentStatus,$serviceid);
		}
	}

	function removedocument($CurrentStatus,$serviceid){
		// delete from mongo
		$condition = array(
			'msisdn'	=> $this->msisdn,
			'service'	=> $serviceid,
		);
		switch($CurrentStatus){
			case $this->status['active'] : 
				$this->_ci->MDL->delete($this->collection['active'],$condition);
			break;
			case $this->status['inactive'] : 
				$this->_ci->MDL->delete($this->collection['inactive'],$condition);
			break;
			case $this->status['pending'] : 
				$this->_ci->MDL->delete($this->collection['pending'],$condition);
			break;
			case $this->status['new'] : 
				$this->_ci->MDL->delete($this->collection['new'],$condition);
			break;
			case $this->status['renew'] : 
				$this->_ci->MDL->delete($this->collection['renew'],$condition);
			break;

		}
	}
}
