 <?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class UserCampid{

	private $DATA			= array();
	private $data			= "";
	private $campid			= "";
	private $type			= "";
	private $push			= "";
	private $dbtool;
	private $table;
	public $client;
	public $eclient;
	var $redisactive 		= false;
	var $econnect 			= false;
	var $ekey				= "";
	function __construct($campid = "",$data = ""){
		$this->table = "#__".Options::$_userdata;
		$this->elastictype	= Options::$_e_userdata;
		$this->init($campid,$data);
	}
	protected function redisconnect(){
		global $redis;
       	$this->client = $redis->client;      
       	if(!is_object($this->client)){
       		echo "Couldn't connected to on msisdn layer<br>";
       	}
       $this->redisactive = true;
	}

	function elastic_connection(){
		$this->eclient =& Terragon::elasticSearch();   
       	if(!is_object($this->client)){
       		echo "Couldn't connected to on msisdn layer<br>";
       	}
       $this->econnect = true;
	}
	protected function dbconnect(){
		$this->dbtool =& Terragon::load_libraries('db');
	}
	private function init($campid,$data){
		$this->campid 		= $campid;
		$this->data 		= $data;
		$this->dbconnect();
		$this->redisconnect();
		$this->elastic_connection();
	}
	function get($type = 'user_id'){
		$this->type = $type;
		if(count($this->DATA) > 0){
			return $this->DATA;
		}
		global $db;
		if($type == 'msisdn'){
			$this->data = msisdn_sanitizer($this->data,false);
		}

		$this->redisdata();

		$this->addToElalastic();

		if(count($this->DATA) > 0){
			return;
		}

		$this->elasticdata($type);

		if(count($this->DATA) > 0){
			return;
		}

	//	var_dump($this->DATA);
	//	die;
		$this->DATA = $this->dbtool->load($this->table, " where cds_campaign_id =".$db->tosql($this->campid,'Text'). " and {$type} = ".$db->tosql($this->data,'Text'),0,1);
		if(count($this->DATA) > 0){
			foreach($this->DATA[0] as $k => $v) {
				$this->$k = $v;		
			}
			$this->msisdn = msisdn_sanitizer($this->msisdn,false);

			$this->DATA = array(
				'id' 							=> $this->id,
				'msisdn' 						=> $this->msisdn,
				'user_id' 						=> $this->user_id,
				'cds_campaign_id' 				=> $this->campid,
				'subscription_status' 			=> $this->subscription_status,
				'time_created' 					=> $this->time_created,
			);

			$this->addToElalastic();
			try {
				$this->client->hmset("track:".$this->campid.':'.$this->msisdn,$this->DATA);
				$this->client->hmset("track:".$this->campid.':'.$this->user_id,$this->DATA);
			}catch (Exception $e) {
				
			}
		}
		return $this->DATA;
	}

	function addRefId($refid){
		if(count($this->DATA) > 0){
			$data = $this->DATA;
			$data['refid'] = $refid;
			try {
				$this->client->hmset("track:".$this->campid.':'.$this->msisdn,$data);
				$this->client->hmset("track:".$this->campid.':'.$this->user_id,$data);
				$this->DATA = $this->client->hgetall("track:".$this->campid.':'.$this->msisdn); 
			}catch (Exception $e) {
				return false;
			}
			try {
				$this->eclient->add($this->elastictype, "track:".$this->campid.':'.$this->msisdn , $data );
			}catch (Exception $e) {
				return false;
			}
		}
	}

	function getRefId(){
		if(count($this->DATA) > 0){
			return array_key_exists('refid',$this->DATA) ? $this->DATA['refid']  : '';
		}
		return "";
	}

	function redisdata(){
		try {
			$this->push = "track:".$this->campid.':'.$this->data;
			$DATA = $this->client->hgetall($this->push); 
			if(count($DATA) == 0){
				return;
			}
			$this->DATA = $DATA;
			if(count($this->DATA) > 0){
				foreach($this->DATA as $k => $v) {
					$this->$k = $v;		
				}
				$this->msisdn = msisdn_sanitizer($this->msisdn,false);
			}
		}catch (Exception $e) {
			return false;
		}
	}
	function exist($type = 'user_id'){
		$this->get($type);
		return (count($this->DATA) > 0) ? true : false;
	}
	
	function gerarkeyuserid($length = 10,$upper = false) 
	{
		return uniqid("cc-u-", false); 
	}
	function add($status = 'inactive'){
		if($this->exist('msisdn') == true){
			return;
		}
		$userid = $this->gerarkeyuserid();
		$msisdn = msisdn_sanitizer($this->data,false);
		$data = array(
			'msisdn'				=> $msisdn,
			'user_id'				=> $userid,
			'cds_campaign_id'		=> $this->campid,
			'subscription_status' 	=> $status,
			'time_created'			=> date("Y-m-d H:i:s")
		);
		$this->dbtool->add($this->table,$data);
		try {
			$this->eclient->add($this->elastictype, "track:".$this->campid.':'.$msisdn , $data );
		}catch (Exception $e) {
			return false;
		}
		
		$this->get('msisdn');
		return $this->DATA;
	}	

	function active()
	{
		$this->setStatus('active');
		//$this->dbtool->update($this->table,array("subscription_status"=>'active'),"WHERE id = ".$this->id);
	}

	function inactive()
	{
		$this->setStatus('inactive');
		//$this->dbtool->update($this->table,array("subscription_status"=>'inactive'),"WHERE id = ".$this->id);
	}

	function setStatus($status){
		if($this->exist($this->type) == true){
			$data = $this->DATA;
			$data['subscription_status'] = $status;
			try {
				$this->client->hmset("track:".$this->campid.':'.$this->msisdn,$data);
				$this->client->hmset("track:".$this->campid.':'.$this->user_id,$data);
				$this->DATA = $this->client->hgetall("track:".$this->campid.':'.$this->msisdn); 
			}catch (Exception $e) {
				return false;
			}
			try {
				$this->eclient->add($this->elastictype, "track:".$this->campid.':'.$this->msisdn , $data );
			}catch (Exception $e) {
				return false;
			}
			$this->DATA = $data;
		}
	}

	function getStatus(){
		if($this->exist($this->type) == true){
			return $this->DATA['subscription_status'];
		}
		return "";
	}

   function bill($amount){

		// $strings	= array('0','1');
		// $key		= array_rand($strings);
		// return $strings[$key];

		$this->transactionID = $this->gerarkeyuserid();
		$pricecode = 'SUB'.$amount;
		
		$url = 'http://cms-mtnplayni.imimobile.net/davinci9999/davinci/BillingApps/tpbillapi/ChargeUser.aspx?'
		.'ph='.$this->msisdn
		.'&transid='.$this->transactionID
		.'&ctype=SUB&ua=USERAGENT'
		.'&pcode='.$pricecode
		.'&cid=3636292'
		.'&title=TITLE'
		.'&acturl=ACTURL&chnl=SMS&vendor=TWINEPINE';
		$report = curl($url);

		if(stripos($report,'0|Success') == 'true') {
			return '0';
		}else {
			return $report;
		}
	}

	function tmonibill($amount){
		$strings	= array('0','1');
		$key		= array_rand($strings);
		return $strings[$key];
	}

	function curl($url,$requestData = ''){
		$options = array(
			"url" 			=> $url,
			"data" 			=> "plain",
			"httpheader" 	=> array('Content-type: application/json'),
			//"post" 			=> "1",
			//"post_fields" 	=> json_encode($requestData),
			"return_transfer" => "1"
		);		
		$obj =& Terragon::load_class('curlclass');
		$obj->set_options($options);
		$resp = $obj->Execute();	
		if($obj->get_httpCode() != 200){
			return "";
		}return $resp;
	}

	public function addToElalastic(){
		//$this->redisdata();
		//echo "<pre>";print_r($this->DATA);echo "</pre>";
		try {
          	if(count($this->DATA) > 0){
				$this->ekey =	"track:".$this->DATA['cds_campaign_id'].':'.$this->DATA['msisdn'];
				$this->eclient->add($this->elastictype, $this->ekey , $this->DATA );
			}	
        }catch (Exception $e) {
          
        }
	}

	function elasticdata($type = 'user_id'){
		if($type == 'msisdn'){
			$this->data = msisdn_sanitizer($this->data,false);
		}
		try {
          	$DATA =  $this->eclient->query( $this->elastictype , $this->data );	
        }catch (Exception $e) {
          $DATA = array();
        }

        if(count($DATA) == 0){
        	return;
        }
		
		if(array_key_exists('status', $DATA ) && $DATA['status'] == 404){
			return;
		}
		$found = false;
		if (count($DATA) > 0) {
			if(array_key_exists('hits', $DATA)){
				$DATA = $DATA['hits'];
				if(array_key_exists('hits', $DATA)){
					$DATA = $DATA['hits'];
					if(count($DATA) > 0){
						foreach ($DATA as  $item) {

							$data = $item['_source'];

							if($type == 'user_id'){
								if($data['user_id'] == $this->data && $data['cds_campaign_id'] == $this->campid ){
									$DATA = $data;
									$found = true;
									break;
								}
							}else{
								if($data['msisdn'] == $this->data && $data['cds_campaign_id'] == $this->campid ){
									$DATA = $data;
									$found = true;
									break;
								}
							}
						}
					}
				}
			}
		}

		if($found == true)
		{
			$this->DATA = $DATA;

			foreach($this->DATA as $k => $v) {
				$this->$k = $v;		
			}
			try {
	          	$this->client->hmset("track:".$this->campid.':'.$this->msisdn,$this->DATA);
				$this->client->hmset("track:".$this->campid.':'.$this->user_id,$this->DATA);
	        }catch (Exception $e) {
	          $DATA = array();
	        }
		}
		return $this->DATA;
	}

	public function subscribe($productID , $shortcode , $serviceid) {
		$sdp  =& Terragon::getSDP($serviceid);
		return $sdp->subscribe($this->msisdn, $productID , $shortcode);  
    }

    public function unsubscribe($productID , $shortcode , $serviceid ) {
    	$sdp  =& Terragon::getSDP($serviceid);
		return $sdp->unsubscribe($this->msisdn, $productID , $shortcode);
    }

    public function sendSms($message, $serviceId) {
    	$sdp  =& Terragon::getSDP($serviceId);
		return $sdp->sendSms($this->msisdn, $message, '5033');
    }

    function __set($k,$v)
	{
		$this->$k  = $v;
	}
	
	function __get($k)
	{	
		return property_exists($this, $k) ? $this->$k : '';
	}

	
}
