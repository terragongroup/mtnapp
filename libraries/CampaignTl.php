<?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class CampaignTl{

	private $DATA			= array();
	private $data			= "";
	private $campid			= "";
	private $dbtool;
	private $service_table;
	private $billing_table;
	private $content_table;
	private $log_table;
	var $exist = false;
	VAR $cache = "";
	var $db;
	function __construct(){
		$this->service_table = "#__".Options::$_tlservice;
		$this->billing_table = "#__".Options::$_tlbilling;
		$this->content_table = "#__".Options::$_tlcontent;
		$this->log_table 	 = "#__".Options::$_tllogs;
		$this->db 			=& Terragon::dbconnect('TL'); 
	}
	
	function getServcie($data = array()){ 
		$where = "";
		$count = count($data);
		if(count($data) > 0){
			$key = array_keys($data);
			$item = $data[$key[0]];
			if($item == $this->cache){
				return;
			}
			$this->cache = $item;
			$where = "where ";
			$c = 0;
			foreach($data as $k => $v){
				$c++;
				$where .= $k."=".$this->db->tosql($v,'Text');
				if($c != $count)
				$where .= "and ";
			}
		}
		$this->db->select($this->service_table, '*', $where, null, null, '', '', null, 'contentdata',false,false);	
		if($this->db->nf('contentdata') > 0){	
			$this->exist = true;
			$data = $this->db->fetch_result('contentfetchdata','contentdata');
			return $data[0];
			// foreach($data[0] as $k => $v) {
			// 	$this->$k = $v;		
			// }
		}

		return false;
	}

	function exist(){
		return $this->exist;
	}

	function pushupdate($data){
		$time 		= $this->get_time_key();
		$data['timestamp'] = $time;
	    $rmq =& Terragon::load_libraries('RabbitMQWorker');
		$strings	= array('MTN_TL_UPDATE','MTN_TL_UPDATE_NEW');
		$key		= array_rand($strings);
		$queue_name =  $strings[$key];
		//$queue_name = "MTN_TL_UPDATE";
		if($rmq->connected == true){
			$rmq->PushToQueue($queue_name,$data);
		}else{
		}
	}

	public function msisdn_logs($msisdn,$date,$transacType,$desc,$productID){
		$param = array(
	        'id'	=> null,
	        'msisdn'  => $msisdn, 
	        'date'  => $date,
	        'transaction_type'   => $transacType,
	        'description'  	=> $desc,
	        'product_id'  	=> $productID
	    );
	    foreach($param as $key => $value){
	        $ky[] = $key;
	        $val[] =  '"'.trim(  $value ).'"';
	    }
	    $sql  = "INSERT INTO {$this->log_table} (".implode(",", $ky ).") VALUES (".implode(",", $val ).")";  
	    $this->db->query($sql);	
	}

	public function addBillInfo($transactionDate,$billType,$msisdn,$legacyProductCode){
		$param = array(
	        'id'	=> null,
	        'date'  => $transactionDate, 
	        'type'  => $billType,
	        'legacy_product_code'   => $legacyProductCode,
	        'msisdn'  	=> $msisdn
	    );
	    foreach($param as $key => $value){
	        $ky[] = $key;
	        $val[] =  '"'.trim(  $value ).'"';
	    }
		$sql  = "INSERT INTO {$this->billing_table} (".implode(",", $ky ).") VALUES (".implode(",", $val ).")";  
		$this->db->query($sql);	
	}

	function getContent($service_id){ 
		$this->db->select($this->content_table, 'content', " where legacy_product_code =".$this->db->tosql($service_id,'Text'). " and dispatch_status = 'sent' and dispatch_time like '%".$this->db->tosql(date("Y-m-d"),'Text',false)."%'"  , null, null, '', '', null, 'content',false,false);	
		if($this->db->nf('content') > 0){	
			$this->exist = true;
			$data = $this->db->fetch_result('contentlist','content');
			return $data[0]['content'];
		}return false;
	}

	function get_time_key($m = '',$d = '',$y = ''){
		$d = ($d == '') ? date('d') : $d;
		$m = ($m == '') ? date('m') : $m;
		$y = ($y == '') ? date('Y') : $y;
		return mktime(0, 0, 0, $m, $d, $y);
	}

	function __set($k,$v)
	{
		$this->$k  = $v;
	}
	
	function __get($k)
	{
		return $this->$k;
	}

}
