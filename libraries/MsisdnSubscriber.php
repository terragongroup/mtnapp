 <?php defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class MsisdnSubscriber{

	private $DATA			= array();
	private $msisdn			= "";
	private $productid		= "";
	private $service_code	= "";
	private $service_id 	= "";
	private $type			= "";
	private $push			= "";
	private $db;
	private $dbtool;
	private $table;
	private $service_tb;
	public $client;
	public $eclient;
	var $redisactive 		= false;
	var $econnect 			= false;
	var $ekey				= "";
	var $PARAM = array(
		'id' 							=> '',
		'msisdn' 						=> '',
		'Apply_Time' 					=> '',
		'Expire_Time' 					=> '',
		'Rent_status' 					=> '',
		'legacy_product_code' 			=> '',
		'delete_status' 				=> '0'
	);
	function __construct($productid = "",$msisdn = ""){
		$this->table  		= "#__".Options::$_tlsubscriber;
		$this->service_tb 	= "#__".Options::$_tlservice;
		$this->init($productid,$msisdn);
	}
	protected function redisconnect(){
		global $redis;
       	$this->client = $redis->client;      
       	if(!is_object($this->client)){
       		echo "Couldn't connected to on msisdn layer<br>";
       	}
       $this->redisactive = true;
	}

	protected function dbconnect(){
		$this->db	=& Terragon::dbconnect('TL'); 
		$this->dbtool =& Terragon::load_libraries('db','TL');
	}
	private function init($productid,$msisdn){
		$this->productid 	= $productid;
		$this->msisdn 		= $msisdn;
		$this->dbconnect();
		$this->getLegacyProductCode();
		$this->redisconnect();
		$this->rediskey = "TLSUBSCRIBER:".$this->msisdn.':'.$this->productid;
	}
	function get(){
		if(empty($this->service_code)){
			return;
		}
		if(count($this->DATA) > 0){
			return $this->DATA;
		}
		$this->msisdn = msisdn_sanitizer($this->msisdn,false);
		$this->redisdata();
		if(count($this->DATA) > 0){
			return;
		}
	//	die;
		$this->DATA = $this->dbtool->load($this->table, " where msisdn =".$this->db->tosql($this->msisdn,'Text'). " and legacy_product_code = ".$this->db->tosql($this->service_code,'Text'),0,1);
		if(count($this->DATA) > 0){
			foreach($this->DATA[0] as $k => $v) {
				$this->$k = $v;		
			}
			$this->msisdn = msisdn_sanitizer($this->msisdn,false);

			$this->DATA = array(
				'id' 							=> $this->id,
				'msisdn' 						=> $this->msisdn,
				'Apply_Time' 					=> $this->Apply_Time,
				'Expire_Time' 					=> $this->Expire_Time,
				'Rent_status' 					=> $this->Rent_status,
				'legacy_product_code' 			=> $this->legacy_product_code,
				'delete_status' 				=> $this->delete_status
			);
			$this->updateredis();
		}
		return $this->DATA;
	}
	function updateredis(){
		try {
			$this->client->hmset($this->rediskey,$this->DATA);
		}catch (Exception $e) {
			
		}
	}

	function redisdata(){
		try {
			$DATA = $this->client->hgetall($this->rediskey); 
			if(count($DATA) == 0){
				return;
			}
			$this->DATA = $DATA;
			if(count($this->DATA) > 0){
				foreach($this->DATA as $k => $v) {
					$this->$k = $v;		
				}
				$this->msisdn = msisdn_sanitizer($this->msisdn,false);
			}
		}catch (Exception $e) {
			return false;
		}
	}
	function exist(){
		$this->get();
		return (count($this->DATA) > 0) ? true : false;
	}

	function getLegacyProductCode(){
		$DATA = $this->dbtool->load($this->service_tb, " where product_id =".$this->db->tosql($this->productid,'Text'),0,1);
		if(count($DATA) > 0){
			$this->service_code = $DATA[0]['legacy_product_code'];
			$this->service_id 	= $DATA[0]['service_id'];
		}
	}
	
	function add($param){
		if($this->exist() == true){
			return;
		}
		$msisdn = msisdn_sanitizer($this->msisdn,false);
		$data = array();
		foreach ($param as $key => $value) {
			if(array_key_exists($key, $this->PARAM)){
				$data[$key] = $value;
			}
		}
		$data['msisdn'] = $msisdn;
		$data['legacy_product_code'] = $this->service_code;
		$this->dbtool->add($this->table,$data);
		$this->get();
		return $this->DATA;
	}	

	function renew($param){
		if($this->exist() == false){
			return;
		}
		$msisdn = msisdn_sanitizer($this->msisdn,false);
		foreach ($param as  $key => $value) {
			if(array_key_exists($key, $this->PARAM)){
				$this->DATA[$key] = $value;
			}
		}
		$this->DATA['id'] = $this->id;
		$this->DATA['msisdn'] = $msisdn;
		$this->DATA['legacy_product_code'] = $this->service_code;
		$this->dbtool->update($this->table,$this->DATA,"WHERE id = ".$this->id);
		$this->updateredis();
	}
	function unsub($param){
		if($this->exist() == false){
			$this->add($param);
		}
		$this->DATA['delete_status'] = 1;
		$this->dbtool->update($this->table,array("delete_status"=>'1'),"WHERE id = ".$this->id);
		$this->updateredis();
	}
	function getStatus(){
		if($this->exist($this->type) == true){
			return $this->DATA['delete_status'];
		}
		return "";
	}

	public function subscribe($productID)
	{

		//$strings	= array('00000000','22007330','22007238','22007201');
		//$key		= array_rand($strings);
		//return $strings[$key];


        $ws         = Options::$_ws;
        $vLocation  = Options::$_vLocation;
        $uri        = Options::$_uri;
       
        $timeStamp  = date('YmdHis');
        $spId       = Options::$_spId;
        $spPwd      = Options::$_spPwd;
      
        $spPassword = md5($spId . $spPwd . $timeStamp);
        $UserID     = $this->msisdn;
        $ns         = Options::$_ns; //Namespace of the WS. 

        $client = new SoapClient($ws, array(
            "location"  => $vLocation,
            "uri"       => $uri,
            "trace"     => 1,
            "exceptions" => 1
        ));
        //Body of the Soap Header. 
        $headerbody = array(
            'spId'          => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp,
            'OA'            => $UserID,
            'FA'            => $UserID
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
       
        $client->__setSoapHeaders($header);

        $response = "";

        try {
            $response = $client->subscribeProduct(array(
                'subscribeProductReq'   => array(
                    'userID'                => array('ID' =>$this->msisdn,'type' => '0'),
                    'subInfo'               => array('productID' => $productID, 'channelID' => '7'),
                    'extensionInfo'         => array('namedParameters' => array('key' => 'SubType', 'value' => '0') )
                )
            ));
            return $response->subscribeProductRsp->result;
        } catch (Exception $e) {
            return "";
        }
        
    }


    public function unsubscribe($productID) {

		//$strings	= array('00000000','22007219');
		//$key		= array_rand($strings);
		//return $strings[$key];

        $ws         = Options::$_ws;
        $vLocation  = Options::$_vLocation;
        $uri        = Options::$_uri;



        $timeStamp  = date('YmdHis');
        $spId       = Options::$_spId;
        $spPwd      = Options::$_spPwd;
       
        $spPassword = md5($spId . $spPwd . $timeStamp);
        $UserID = $this->msisdn;

        $ns 		= Options::$_ns; //Namespace of the WS. 

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));
        //Body of the Soap Header. 
        $headerbody = array('spId' => $spId,
            'spPassword'    => $spPassword,
            'timeStamp'     => $timeStamp
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);
       
        $response = "";
        try {
            $response = $client->unSubscribeProduct(array(
                'unSubscribeProductReq' => array(
                    'userID'        => array('ID' => $this->msisdn, 'type' => '0'),
                    'subInfo'       => array('productID' => $productID, 'channelID' => '1', 'operCode' => 'zh', 'isAutoExtend' => '0'),
                    'extensionInfo' => array('namedParameters' => array('key' => 'SubType', 'value' => '0') )
                )
            ));
			$des = "Description not available";
			if (isset($response->unSubscribeProductRsp->resultDescription))
				$des = $response->unSubscribeProductRsp->resultDescription;
        	
		return $response->unSubscribeProductRsp->result;
        } catch (Exception $e) {
            return "";
        }
    }

    public function sendSms($message, $serviceId) {
    	$ws         = Options::$_smsws;
        $vLocation  = Options::$_smsLoc;
        $uri        = Options::$_smsuri;

        //$serviceId = 234012000008174;

        $client = new SoapClient($ws, array(
            "location"      => $vLocation,
            "uri"           => $uri,
            "trace"         => 1,
            "exceptions"    => 1
        ));

        $timeStamp  = date('YmdHis');
        $spId       = Options::$_spId;
        $spPwd      = Options::$_spPwd;

        $spPassword = md5($spId . $spPwd . $timeStamp);
        
        $UserID = 'tel:'.$this->msisdn;
        $ns     = 'http://www.huawei.com.cn/schema/common/v2_1'; //Namespace of the WS. 
        
        $headerbody = array(
        	'spId' 			=> $spId,
            'spPassword'    => $spPassword,
            'serviceId'     => $serviceId,
            'timeStamp'     => $timeStamp,
            'OA'            => $UserID,
        );
        //Create Soap Header.        
        $header = new SOAPHeader($ns, 'RequestSOAPHeader', $headerbody);
        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header);
        $response = "";
        try {
        	 $response = $client->sendSms(array(
        	 	'addresses' 	=> $UserID,
        	 	//'senderName' 	=> 'TwinPine',
        	 	'senderName' 	=> '5033',
        	 	'message' 		=> $message,
        	 	'charging' 		=> '0000',
        	 	//'recieptRequest' => $message,
        	 	// 'encode' 		=> '3333',
        	 	// 'sourceport'	 => '80',
        	 	// 'destinationport' => '80',
        	 	// 'esmclass' 		=> 'ucf',
        	 	// 'data_coding' 	=> 'ww', 	
        	 ));
          return  $response->result; 
        } catch (Exception $e) {  
        	

        	echo "<pre>";print_r($e);echo "</pre>";

        	die;
          return "";
        }
    }

	function curl($url,$requestData = ''){
		$options = array(
			"url" 			=> $url,
			"data" 			=> "plain",
			"httpheader" 	=> array('Content-type: application/json'),
			//"post" 			=> "1",
			//"post_fields" 	=> json_encode($requestData),
			"return_transfer" => "1"
		);		
		$obj =& Terragon::load_class('curlclass');
		$obj->set_options($options);
		$resp = $obj->Execute();	
		if($obj->get_httpCode() != 200){
			return "";
		}return $resp;
	}
	
    function __set($k,$v)
	{
		$this->$k  = $v;
	}
	
	function __get($k)
	{
		return $this->$k;
	}

	
}
