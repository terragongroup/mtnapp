<?php
defined( 'TERRAGON' ) or die( 'Restricted access' );
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
class ApiResponse 
{
	
	var $mPros = array(
		'error' 		=> false,
		'error_code'	=> 0,
		'response' 		=> null,
		'message' 		=> ''
	);

	var $type = 'json';

	function errorResponse($message,$error_code=0,$status = 200){
		$this->mPros['error'] = true;
		$this->mPros['error_code'] = $error_code;
		$this->mPros['message'] = $message;
		$this->response($this->mPros,$status);
	}
	
	function successResponse($response){
		$this->mPros['error'] = false;
		//$this->mPros['message'] = $response;
		$this->mPros['response'] = $response;
		$this->response($this->mPros,200);
	}

	function response($data,$status)
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type');	
		switch($this->type)
		{
			case "xml" :
				 header('Content-Type: application/xml');	
				$xml_response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
				foreach($data as $k => $v){$xml_response .= "<".$k.">". $this->decode_lang($v)."</".$k.">";	}
				echo $xml_response;
				exit;
			break;
			case "json" :
			default : 		
				header('Content-Type: application/json');	
				echo json_encode($data);
				exit;		
			break;
		}
	}
}
?>
