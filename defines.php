<?php 
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
*/
define( 'TERRAGON', 1);
define( 'TERRAGON_BASE',		dirname(__FILE__));
define( 'DS',					DIRECTORY_SEPARATOR);
define( 'LIBRARIES',	 		TERRAGON_BASE.DS.'libraries' );
define( 'MODELS',	 			TERRAGON_BASE.DS.'models' );
?>