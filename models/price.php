<?php 
/**
* @author      Adesipe Oluwatosin
* @email       oadesipe@terragonltd.com (08037268261) 
**/
// Check to ensure this file is within the rest of the framework
defined( 'TERRAGON' ) or die( 'Restricted access' );

class model_price
{
	var $id			= null;
	var $db;
	var $table;
	var $exist = false;
	function __construct($identifier = 0)
	{		
		$this->table = "#__".Options::$_p_codes;
		if (!empty($identifier)) {
			$this->load((int)$identifier);
		}
	}	

	function exist(){
		return $this->exist;
	}

	function doupdate($param = array())
	{
		$this->db->update($this->table,$param," WHERE  id = ".$this->db->tosql($this->id,'Text'),array(),array(),false);
		foreach($param as $k => $v){
			$this->$k = $v;	
		}
	}

	function load($id)
	{
		global $db;
		$this->db =& $db;
		if(is_int($id) && isset($id))
		{
			$this->db->select($this->table, '*', " where id = ". $this->db->tosql($id,'Text'), null, null, '', '', null, 'price',true,false);	
			if($this->db->nf('price') > 0)
			{	
				$this->exist = true;
				$this->db->fetch_result('pricefetch','price');
				$data = $this->db->get_field('pricefetch');
				foreach($data as $k => $v) {
					$this->$k = $v;		
				}
			}			
		}		
		return false;
	}
	function __set($k,$v)
	{
		$this->$k  = $v;
	}
	function __get($k)
	{
		return $this->$k;
	}
}
?>